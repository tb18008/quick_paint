<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\ReportService;
use App\Services\UserImageService;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application front page.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function front_page(Request $request)
    {
        $user_image_service = new UserImageService();

        $newest_user_images = $user_image_service
            ->get_newest()
            ->paginate(config('pagination.split_items_per_page'),null , null,'newest');
        $favored_user_images = $user_image_service
            ->get_favored()
            ->paginate(config('pagination.split_items_per_page'),null , null,'favored');

        if($request->filled('active')) $active = $request->get('active');
        else $active = 'newest';

        return view('front_page',compact(['newest_user_images','favored_user_images', 'active']));
    }


    /**
     * Show the user's dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function dashboard(Request $request)
    {
        $user = $request->user();

        $user_image_service = new UserImageService();
        $report_service = new ReportService();

        if($user->onlyStandard()){

            $my_user_images = $user_image_service
                ->get_by_user($user->id)
                ->paginate(config('pagination.split_items_per_page'),['*'],'my_user_images');
            $favorites = $user_image_service
                ->get_favorites()
                ->paginate(config('pagination.split_items_per_page'),null , null,'favorites');

            if($request->filled('active')) $active = $request->get('active');
            else $active = 'my_user_images';

            return view('user.dashboard',compact(['my_user_images','favorites','active']));
        }
        elseif ($user->isStandard() && $user->isModerator()){

            $my_user_images = $user_image_service
                ->get_by_user($user->id)
                ->paginate(config('pagination.split_items_per_page'),['*'],'my_user_images');
            $favorites = $user_image_service
                ->get_favorites()
                ->paginate(config('pagination.split_items_per_page'),null , null,'favorites');

            $user_image_service->get_soft_deleted();
            $reports = $report_service
                ->get_all()
                ->paginate(config('pagination.split_items_per_page'),null , null,'reports');

            if($request->filled('active')) $active = $request->get('active');
            else $active = 'my_user_images';

            return view('user.dashboard',compact(['my_user_images','favorites','reports','active']));
        }
        elseif ($user->isModerator()){

            $favorites = $user_image_service
                ->get_favorites()
                ->paginate(config('pagination.split_items_per_page'),null , null,'favorites');

            $user_image_service->get_soft_deleted();
            $reports = $report_service
                ->get_all()
                ->paginate(config('pagination.split_items_per_page'),null , null,'reports');

            if($request->filled('active')) $active = $request->get('active');
            else $active = 'favorites';

            return view('user.dashboard',compact(['favorites','reports','active']));
        }
        elseif ($user->isAdmin()){

            $user_image_service->get_soft_deleted();
            $hidden_user_images = $user_image_service
                ->collect()
                ->paginate(config('pagination.split_items_per_page'),null , null,'hidden_user_images');
            $hidden_reports = $report_service
                ->get_soft_deleted()
                ->paginate(config('pagination.split_items_per_page'),null , null,'hidden_reports');
            $users = User::with('roles')
                ->orderBy('name')
                ->get()
                ->paginate(config('pagination.split_items_per_page'),null , null, 'users');

            if($request->filled('active')) $active = $request->get('active');
            else $active = 'hidden_user_images';

            return view('user.dashboard',compact(['hidden_user_images','hidden_reports','users','active']));
        }

        return redirect('/');
    }
}
