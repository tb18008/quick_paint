<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use App\Models\UserImage;
use App\Services\UserImageService;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{

    /**
     * Get user's favorite user images
     *
     * @return \Illuminate\View\View
     */
    public function user_favorites(){

        $user_image_service = new UserImageService();
        $user_images = $user_image_service->get_favorites()->paginate(config('pagination.index_items_per_page'));

        return view('user_image.index', compact('user_images'));
    }

    /**
     * Attach user image to user's favorites
     *
     * @param Request $request
     * @param $user_image_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function attach(Request $request, $user_image_id)
    {
        $user = $request->user();

        if ($user->can('attach_detach', Favorite::class)) { //User is authorized to attach or detach favorites

            $user_image = UserImage::find($user_image_id);

            if($user_image){    //User image found

                if(!$user->favorites()->where('user_image_id',$user_image->id)->first()){ //User image isn't included in favorites yet
                    $user->favorites()->attach($user_image);
                    return response()->json('success',200);
                }

                return response()->json("User image already a favorite",400);
            }

            return response()->json("User image not found",404);
        }

        return response()->json("Only standard or moderator users can have favorites",403);

    }

    /**
     * Detach user image from user's favorites
     *
     * @param Request $request
     * @param $user_image_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detach(Request $request, $user_image_id)
    {
        $user = $request->user();

        if ($user->can('attach_detach', Favorite::class)) { //User is authorized to attach or detach favorites

            $user_image = UserImage::find($user_image_id);

            if($user_image){    //User image found

                if($user->favorites()->where('user_image_id',$user_image->id)->first()){ //User image is included in favorites
                    $user->favorites()->detach($user_image);
                    return response()->json('success',200);
                }

                return response()->json("User image isn't a favorite",400);
            }

            return response()->json("User image not found",404);
        }

        return response()->json("Only standard or moderator users can have favorites",403);

    }
}
