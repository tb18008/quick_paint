<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserImageDestroyRequest;
use App\Http\Requests\UserImageStoreRequest;
use App\Http\Requests\UserImageUpdateRequest;
use App\Models\User;
use App\Models\UserImage;
use App\Services\TagService;
use App\Services\UserImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|string
     */
    public function index(Request $request)
    {

        $user_image_service = new UserImageService();

        if($request->filled('only_trashed'))
        {
            $this->authorize('viewAny', UserImage::class);

            $user_image_service->get_soft_deleted();
        }

        if($request->filled('search_terms')){   //Search input filled with search terms

            $user_image_service->search($request->get('search_terms'));

        }
        elseif($request->filled('tags') || $request->filled('user') || $request->filled('name')){
            //Request contains a query of users, tags or names
            $user_image_service->get_by_query_string($request);
        }

        if($request->ajax()){   //Request is asynchronous and response will be search_results view

            if($request->filled('page')){
                $user_images = $user_image_service->collect()->paginate(config('pagination.browse_items_per_page'),null,$request->get('page'));
            }
            else{
                $user_images = $user_image_service->collect()->paginate(config('pagination.browse_items_per_page'));
            }

            return view("user_image.search_results",compact('user_images'));
        }
        //Response synchronous and response will be an index view

        $user_images = $user_image_service->collect()->paginate(config('pagination.index_items_per_page'));
        $user_images->withPath('?'.$request->getQueryString());


        return view('user_image.index', compact('user_images'));
    }

    /**
     * Retrieve models to narrow down user images.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function browse()
    {
        $tag_service = new TagService();
        $tags = $tag_service->cloud();

        $users = User::whereHas('user_images')->get();

        return view('user_image.browse', compact(['tags','users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', UserImage::class);

        $user = Auth::user();
        return view('user_image.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserImageStoreRequest $request)
    {
        $user_image_service = new UserImageService();
        $new_user_image = $user_image_service->store($request);

        if($new_user_image != null) return redirect('/user_images/'.$new_user_image->id)
            ->with('message',
                ['success'=>__(
                    'messages.stored',
                    [
                        'object_type'=>'user image',
                        'object_name'=> $new_user_image->name,
                    ])
                ]
            );

        return redirect()->back()
            ->with('message',
                ['fail'=>'Failed to store user image']
            );

    }

    /**
     * Display the specified resource.
     *
     * @param UserImage $user_image
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(UserImage $user_image)
    {
        return view('user_image.show',compact('user_image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param UserImage $user_image
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory
     */
    public function edit(UserImage $user_image)
    {
        $this->authorize('update', $user_image);

        $user_image->load('tags');
        return view('user_image.edit',compact('user_image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserImageUpdateRequest $request
     * @param UserImage $user_image
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function update(UserImageUpdateRequest $request, UserImage $user_image)
    {

        $user_image_service = new UserImageService();
        $new_user_image = $user_image_service->update($request, $user_image);

        if($new_user_image != null) return redirect('/user_images/'.$new_user_image->id)
            ->with('message',
                ['success'=>__(
                    'messages.updated',
                    [
                        'object_type'=>'user image',
                        'object_name'=> $new_user_image->name,
                    ])
                ]
            );

        return redirect()->back()
            ->with('message',
                ['fail'=>__(
                    'messages.updated',
                    [
                        'object_type'=>'user image',
                        'object_name'=> $new_user_image->name,
                    ])
                ]
            );
    }

    /**
     * Remove the specified resource from storage or mark resource as soft deleted.
     *
     * @param UserImageDestroyRequest $request
     * @param UserImage $user_image
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(UserImageDestroyRequest $request, UserImage $user_image)
    {
        if($request->filled('forced') && $request->get('forced') == true) {
            $forced = true;
            $message_type = 'destroyed';
        }
        else {
            $forced = false;
            $message_type = 'hidden';
        }

        $user_image_name = $user_image->name;

        //Calling UserImageService
        $user_image_service = new UserImageService();
        $user_image_service->destroy($user_image, $forced);

        //Response
        return redirect('user_images')->with('message',['warning'=>__(
            'messages.'.$message_type,
            [
                'object_type'=>'user image',
                'object_name'=>$user_image_name,
            ]
        )]);
    }

    /**
     * Restores a soft deleted resource.
     *
     * @param $user_image_id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore($user_image_id)
    {
        $this->authorize('restore',[UserImage::class, $user_image_id]);

        $user_image_service = new UserImageService();
        $user_image = $user_image_service->restore($user_image_id);

        if($user_image != null) return redirect()->back()->with('message',['success'=>__(
            'messages.restored',
            [
                'object_type'=>'user image',
                'object_name'=>$user_image->name,
            ]
        )]);
        else return redirect()->back()->with('message',['fail'=>__(
            'messages.not_found',
            [
                'object_type'=>'user image',
                'object_id'=>$user_image_id,
            ]
        )]);


    }
}
