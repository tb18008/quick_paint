<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|string
     */
    public function index(Request $request)
    {
        $tag_service = new \App\Services\TagService();
        $tags = $tag_service->cloud()->paginate(config('pagination.cloud_items_per_page'));
        return view('tag.browse',compact('tags'));
    }
}
