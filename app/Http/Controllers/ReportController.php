<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportDestroyRequest;
use App\Http\Requests\ReportStoreRequest;
use App\Models\Report;
use App\Models\ReportType;
use App\Models\UserImage;
use App\Services\ReportService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $report_service = new ReportService();
        $reports = collect([]);

        if($request->filled('hidden') && $user->can('viewAny', Report::class)){
            $reports = $report_service->get_soft_deleted();
        }
        else{
            if($user->isModerator() || $user->isAdmin()){
                $reports = $report_service->get_all();
            }
            elseif($user->isStandard()){
                $reports = $report_service->get_by_user($request->user());
            }
        }
        $reports = $reports->paginate(config('pagination.index_items_per_page'));
        return view('report.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->authorize('create', Report::class);

        //User is authorized to request a report create form

        $request->validate([
            'user_image_id' => 'required|numeric|exists:users,id',
        ]);

        //Provided user image id is valid

        $user_image = UserImage::with('user')->find($request->get('user_image_id'));
        $report_types = ReportType::all();

        return view('report.create', compact(['report_types','user_image']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReportStoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(ReportStoreRequest $request)
    {
        $new_report = Report::create($request->validated());

        if(isset($new_report)) return redirect('reports/'.$new_report->id)
            ->with('message',['success'=>__('messages.report_submit')]);

        return redirect('user_images/'.$request->get('user_image_id'))
            ->with('message', ['fail'=>'Failed to create report']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Report $report)
    {
        $this->authorize('view',$report);
        return view('report.show', compact('report'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ReportDestroyRequest $request
     * @param $report_id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(ReportDestroyRequest $request, $report_id)
    {
        $message_type = '';

        //Authorization
        if($request->input('forced') == true)   {   //Force delete
            $this->authorize('forceDelete',[Report::class, $report_id]);
            $message_type = 'destroyed';

            $report = Report::withTrashed()->find($report_id);
            $report->forceDelete();
        }
        else {  //Soft delete
            $this->authorize('delete',[Report::class, $report_id]);
            $message_type = 'hidden';

            $report = Report::find($report_id);
            $report->delete();
        }

        //Response
        return redirect('/reports')->with('message',['warning'=>__(
            'messages.'.$message_type,
            [
                'object_type'=>'report',
                'object_id'=>$report_id,
            ]
        )]);

    }

    /**
     * Restores a soft deleted resource.
     *
     * @param $report_id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore($report_id)
    {
        $this->authorize('restore',[Report::class, $report_id]);

        $report = Report::withTrashed()->find($report_id);
        if($report->restore()) redirect('/reports')->with('message',['success'=>__(
            'messages.restored',
            [
                'object_type'=>'report',
                'object_name'=>$report->subject,
            ]
        )]);
        return redirect('/reports')->with('message',['fail'=>__(
            'messages.not_found',
            [
                'object_type'=>'report',
                'object_id'=>$report_id,
            ]
        )]);
    }
}
