<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $user->load('roles');
        return view('user.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {

        $this->authorize('delete', $user);

        //Delete relations
        $user->reports()->delete();

        //Delete user images
        $user_image_service = new UserImageService();
        $user_image_service->delete_all_for_user($user);

        $user_name = $user->name;
        $user_id = $user->id;

        $user->delete();

        if(Auth::user()->id == $user_id){

            Auth::logout();

            return redirect('/login')
                ->with('message',['warning'=>__(
                    'messages.destroyed',
                    [
                        'object_type'=>'user',
                        'object_name'=> $user_name,
                    ]
                )]);
        }


        return redirect('/home')->with('message',['warning'=>__(
            'messages.destroyed',
            [
                'object_type'=>'user',
                'object_name'=>$user_name,
            ]
        )]);
    }
}
