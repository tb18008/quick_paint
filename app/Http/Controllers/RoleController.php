<?php

namespace App\Http\Controllers;

use App\Http\Requests\ElevateRoleRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ElevateRoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ElevateRoleRequest $request)
    {
        $role_update_success = NULL;

        $update_type = '';
        $new_role = '';

        $user = User::find($request->get('user_id'));

        if($request->filled('promote')) {   //Promotion to Moderator
            $role_update_success = $user->promoteUser();

            $update_type = 'promoted';
            $new_role = 'moderator';
        }
        elseif ($request->filled('demote')){    //Demotion to Standard user
            $role_update_success = $user->demoteUser();

            $update_type = 'demoted';
            $new_role = 'standard user';
        }

        if($role_update_success) return redirect()->back()
            ->with('message',
                ['success'=>__(
                    'messages.role_update',
                    [
                        'user_name'=> $user->getAttribute('name'),
                        'update_type'=> $update_type,
                        'new_role'=> $new_role,
                    ])
                ]
            );

        return redirect()->back()
            ->with('message',
                ['fail'=>'Failed to change user role']
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}
