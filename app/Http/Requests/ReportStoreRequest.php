<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ReportStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isStandard() || $this->user()->isModerator();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reporter_id' => 'required|in:'.Auth::id(),
            'report_type_id' => 'numeric|required|exists:report_types,id',
            'user_image_id' => 'numeric|required|exists:user_images,id',
            'report_text' => 'string|required',
            'subject' => 'string|required',
        ];
    }
}
