<?php

namespace App\Http\Requests;

use App\Models\UserImage;
use Illuminate\Foundation\Http\FormRequest;

class UserImageUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param $user_image
     * @return bool
     */
    public function authorize()
    {
        $user_image = $this->route()->parameters()['user_image'];
        return $this->user()->id == $user_image->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:64|nullable',
            'tag_string' => 'string|max:128|nullable',
            'image_data' => 'required|base64_png:16,16,512,512',
        ];
    }
}
