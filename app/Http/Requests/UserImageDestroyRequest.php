<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserImageDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user_image = $this->route()->parameters()['user_image'];

        if($this->filled('forced') && $this->get('forced') == true){    //Force delete

            //Only admin or owner can force delete specified user image
            return $this->user()->id == $user_image->user->id || $this->user()->isAdmin();

        }
        else{   //Soft delete

            //Only moderator who doesn't own the user image can soft delete specified user image
            return $this->user()->isModerator() && ($this->user()->id != $user_image->user->id);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forced' => 'nullable|boolean'
        ];
    }
}
