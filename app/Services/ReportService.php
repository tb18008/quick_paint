<?php

namespace App\Services;

use App\Models\Report;
use App\Models\User;

class ReportService
{

    /**
     * ImageService constructor.
     */
    public function __construct()
    {

    }

    public function get_all(){
        return Report::with(['reporter','user_image'])->get();
    }

    public function get_by_user(User $user){
        return $user->reports()->with(['reporter','user_image'])->orderBy('created_at')->get();
    }

    public function get_soft_deleted(){
        return Report::onlyTrashed()->with(['reporter','user_image'])->get();
    }

}
