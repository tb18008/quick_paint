<?php

namespace App\Services;

use App\Models\Tag;
use App\Models\User;
use App\Models\UserImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image as Image;

class UserImageService
{
    private $image;
    public $file_name;
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    public $user_images;

    /**
     * ImageService constructor.
     */
    public function __construct()
    {
        $this->user_images = UserImage::query();
    }

    public function resize($width = 100, $height = 100){

        //Image resizing
        return Image::make($this->image)
            ->resize($width, $height, function($constraint) {
               $constraint->aspectRatio();
            });

    }

    public function store(Request $request){

        $this->image = Image::make($request->get('image_data'));

        $this->file_name = 'image'.date_timestamp_get(date_create()).'.png';

        //Saving original
        $this->image->save(storage_path('app/public/user_images/'.$this->file_name));

        //Saving thumbnail
        $this->image = $this->resize();
        $this->image->save(storage_path('app/public/thumbnails/'.$this->file_name));

        //Attaching user image to user
        $user_image = new UserImage(
            [
                'url' => $this->file_name,
                'thumbnail_url'=>$this->file_name,
                'name' => $request->input('name'),
            ]);
        $user = Auth::user();
        $user->user_images()->save($user_image);

        //Attaching tags to user image
        $this->assignTags($request->input('tag_string'), $user_image);

        $user_image->save();

        return $user_image;
    }

    public function update(Request $request, UserImage $user_image){
        $updated_image = Image::make($request->get('image_data'));

        $this->image = $updated_image;
        $this->file_name = 'image'.date_timestamp_get(date_create()).'.png';

        //Deleting previous image and thumbnail
        $user_image_path = storage_path('app/public/user_images/'.$user_image->url);
        $thumbnail_path = storage_path('app/public/thumbnails/'.$user_image->thumbnail_url);
        if (file_exists($user_image_path)) unlink($user_image_path);
        if (file_exists($thumbnail_path)) unlink($thumbnail_path);

        //Saving original and thumbnail
        $this->image->save(storage_path('app/public/user_images/'.$this->file_name));
        $this->image = $this->resize();
        $this->image->save(storage_path('app/public/thumbnails/'.$this->file_name));

        //Detaching tags from user image
        $user_image->tags()->detach();

        //Attaching tags to user image
        $this->assignTags($request->input('tag_string'), $user_image);

        $user_image->update([
            'url' => $this->file_name,
            'thumbnail_url'=>$this->file_name,
            'name' => $request->input('name'),
        ]);

        return $user_image;
    }

    private function assignTags($user_image_tags, UserImage $user_image){
        //Existing tags   (needs App\Models\Tag objects with their ids)
        $existing_tags = Tag::all();

        //New tags
        $user_image_tags = collect(explode(',',$user_image_tags))    //Turn string to array of tag names
            ->map(function ($tag) {
                //Lower case all characters
                $tag = strtolower($tag);
                //Trim whitespaces at ends
                $tag = trim($tag);
                //Make alphanumeric (removes all other characters)
                $tag = preg_replace("/[^a-z0-9_\s-]/", "", $tag);
                //Clean up multiple dashes or whitespaces
                $tag = preg_replace("/[\s-]+/", " ", $tag);
                //Convert whitespaces and dashes to underscore
                $tag = preg_replace("/[\s-]/", "_", $tag);
                return trim($tag);
            })
            ->unique()
            ->filter(function ($value) {    //Filter out empty string tags
                return $value != '';
            });

        $new_tag_names = $user_image_tags->diff($existing_tags->pluck('name'));

        //Assignable tags   (needs App\Models\Tag objects with their ids)
        $assignable_tags = $existing_tags->filter(function($existing_tag) use ($user_image_tags){
            return $user_image_tags->contains($existing_tag->name);
        });

        //Create and assign new tags
        $timestamp = Carbon::now();
        $new_tags = $new_tag_names->map(function($new_tag_name) use ($timestamp){
            return [
                'name' => $new_tag_name,
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
            ];
        });
        foreach ($new_tags as $new_tag){
            $created_tag = Tag::create($new_tag);
            $user_image->tags()->attach($created_tag->id, ['created_at' => $timestamp, 'updated_at' => $timestamp]);
        }

        //Assign already existing tags
        foreach ($assignable_tags as $assignable_tag){
            $user_image->tags()->attach($assignable_tag->id, ['created_at' => $timestamp, 'updated_at' => $timestamp]);
        }
    }

    public function destroy(UserImage $user_image, $forced){
        if($forced){

            //Deleting image and thumbnail files
            $user_image_path = storage_path('app/public/user_images/'.$user_image->url);
            $thumbnail_path = storage_path('app/public/thumbnails/'.$user_image->thumbnail_url);
            if (file_exists($user_image_path)) unlink($user_image_path);
            if (file_exists($thumbnail_path)) unlink($thumbnail_path);

            return $user_image->forceDelete();
        }
        else return $user_image->delete();   //Soft delete
    }

    public function delete_all_for_user(User $user){

        //Iterate through each user's user image (even soft deleted ones)
        $user->user_images()->withTrashed()->get()->each(function ($user_image) {
            $user_image->tags()->detach();  //Detach tags related to user image

            //Delete image files associated to user image
            $user_image_file_path = storage_path('app/public/user_images/'.$user_image->url);
            $thumbnail_file_path = storage_path('app/public/thumbnails/'.$user_image->thumbnail_url);

            if(file_exists($user_image_file_path)) unlink($user_image_file_path);
            if(file_exists($thumbnail_file_path)) unlink($thumbnail_file_path);
        });

        //Remove entries of user's user images from table
        $user->user_images()->withTrashed()->forceDelete();

    }

    public function restore($user_image_id){

        $user_image = UserImage::withTrashed()->find($user_image_id);
        if($user_image->restore()) return $user_image;
        return false;

    }

    public function search($search_terms){
        //Search term string to array, removing duplicates
        $search_terms = collect(explode(' ',$search_terms))->unique();

        //Set service's user images to include only the user images that are found with the provided search terms
        $this->user_images = $this->user_images
            ->whereIn(
                'id',
                UserImage::search($search_terms)->withTrashed()->get()->pluck('id')
            );
    }

    public function get_by_query_string (Request $request){
        $user_images = collect();
        if($request->filled('tags')){
            $user_images = $this->get_by_tags($request->get('tags'))->get();
        }
        if($request->filled('user')){
            $user_images_by_users = $this->get_by_user($request->get('user'))->get();
            if($user_images->isEmpty()){
                $user_images = $user_images_by_users;
            }
            else{
                $user_images = $user_images->intersect($user_images_by_users);
            }
        }
        if($request->filled('name')){
            $user_images_by_names = $this->get_by_name($request->get('name'))->get();
            if($user_images->isEmpty()){
                $user_images = $user_images_by_names;
            }
            else{
                $user_images = $user_images->intersect($user_images_by_names);
            }
        }

        $user_image_ids = $user_images->pluck('id');

        return $this->user_images->whereIn('id',$user_image_ids);
    }

    public function get_by_user($user_id){
        return $this->user_images->where('user_id',$user_id);
    }

    public function get_by_tags($tag_ids){

        //Check that all ids in array have a model associated with them
        $tags = Tag::find($tag_ids);
        if($tags->count() < count($tag_ids)) return UserImage::where('id',null);

        //Get user image ids for first tag and remove the first tag from collection
        $user_image_ids = $tags->first()->user_images()->get()->pluck('id');
        $tags->shift();

        //Intersect collected user image ids with the results of subsequent tags
        foreach ($tags as $tag){
            $user_image_ids = $user_image_ids->intersect($tag->user_images()->get()->pluck('id'));
        }


        //Return user images by collected ids. All user images have the tags with specified tag ids
        return UserImage::whereIn('id',$user_image_ids);
    }

    public function get_newest(){
        return UserImage::orderByRaw('MAX(created_at,updated_at) DESC')->get();    //Orders by created_at and updated_at whichever is the highest
    }

    public function get_favored(){
        return UserImage::withCount('favorites')->orderBy('favorites_count','desc')->get();
    }

    public function get_by_name($name){
        return $this->user_images->where('name',$name);
    }

    public function get_favorites(){
        $user = Auth::user();

        return $user->favorites()->orderBy('created_at')->get();
    }

    public function get_soft_deleted(){
        $this->user_images = $this->user_images->onlyTrashed();
    }

    /*
     * @return \Illuminate\Support\Collection
     */
    public function collect(){
        return $this->user_images->get();
    }
}
