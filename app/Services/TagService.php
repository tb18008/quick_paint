<?php

namespace App\Services;

use App\Models\Tag;

class TagService
{

    public function cloud(){

        //Get list of tags
        return Tag::has('user_images')->get();

    }
}
