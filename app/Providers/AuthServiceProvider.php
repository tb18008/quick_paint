<?php

namespace App\Providers;

use App\Models\Favorite;
use App\Models\Report;
use App\Models\User;
use App\Models\UserImage;
use App\Policies\FavoritePolicy;
use App\Policies\ReportPolicy;
use App\Policies\UserImagePolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         UserImage::class => UserImagePolicy::class,
         Report::class => ReportPolicy::class,
         User::class => UserPolicy::class,
         Favorite::class => FavoritePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
    }
}
