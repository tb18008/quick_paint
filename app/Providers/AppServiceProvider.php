<?php

namespace App\Providers;

use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::defaultStringLength(191);

        /**
         * Paginate a standard Laravel Collection.
         *
         * @param int $perPage
         * @param int $total
         * @param int $page
         * @param string $pageName
         * @return array
         */
        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);

            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });

        //Validator for image data
        Validator::extend('base64_png', function ($attribute, $value, $params, $validator) {
            try {
                //Attempt to make image from base64 decoded png data
                $image = ImageManagerStatic::make($value);

                if(sizeof($params) == 4){
                    //Validate image width and height
                    if($image->width() < $params[0] || $image->width() > $params[2]) return false;
                    if($image->height() < $params[1] || $image->height() > $params[3]) return false;
                }

                //Image data is valid
                return true;
            } catch (\Exception $e) {
                return false;
            }
        });
    }
}
