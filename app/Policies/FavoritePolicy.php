<?php

namespace App\Policies;

use App\Models\Favorite;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class FavoritePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can attach or detach favorites
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function attach_detach(User $user)
    {
        return $user->isStandard() || $user->isModerator() //Admins can't have favorites
        ? Response::allow()
        : Response::deny("Only standard or moderator users can have favorites");
    }
}
