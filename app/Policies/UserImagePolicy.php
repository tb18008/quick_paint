<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserImage;
use Illuminate\Auth\Access\HandlesAuthorization;
use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;

class UserImagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isModerator() || $user->isAdmin(); //Only admins or moderators can view models (trashed included)
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User|null $user
     * @param UserImage|null $user_image
     * @return mixed
     */
    public function view(?User $user, ?UserImage $user_image)
    {
        if($user_image->trashed()) return optional($user)->isModerator() || optional($user)->isAdmin();
        else return true; //Non-deleted user images can be viewed by any user
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isStandard();   //Standard users can create user images
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param UserImage $user_image
     * @return mixed
     */
    public function update(User $user, UserImage $user_image)
    {
        return $user->id == $user_image->user->id;    //Only owners can update user images
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param UserImage $user_image
     * @return mixed
     */
    public function delete(User $user, UserImage $user_image)
    {
        return $user->isModerator() && ($user->id != $user_image->user->id);   //A moderator can soft delete images except their own
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param $user_image_id
     * @return mixed
     */
    public function restore(User $user, $user_image_id)
    {
        $user_image = UserImage::withTrashed()->find($user_image_id);
        if($user_image != null) {
            //Admins can restore deleted user images if the user image id is valid
            return $user->isAdmin();
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param $user_image_id
     * @return mixed
     */
    public function forceDelete(User $user, $user_image_id)
    {
        $user_image = UserImage::withTrashed()->find($user_image_id);
        return $user->id == $user_image->user->id || $user->isAdmin();    //Only admin or owners can force delete user images
    }
}
