<?php

namespace App\Policies;

use App\Models\Report;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin(); //Only admins can view soft deleted models
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Report  $report
     * @return mixed
     */
    public function view(User $user, Report $report)
    {
        return $user->id == $report->reporter_id || $user->isModerator() || $user->isAdmin();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isStandard() || $user->isModerator();    //Only a standard user or moderator can create a report
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param $report_id
     * @return mixed
     */
    public function delete(User $user, $report_id)
    {
        $report = Report::find($report_id);
        return $user->isModerator() && !($user->id == $report->reporter_id);   //A moderator can soft delete reports except their own
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function restore(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param $report_id
     * @return mixed
     */
    public function forceDelete(User $user, $report_id)
    {
        $report = Report::withTrashed()->find($report_id);
        if($report->trashed()) return $user->isAdmin(); //Only admin can force delete hidden reports
        return $user->id == $report->reporter_id || $user->isAdmin();    //Only owners or admins can force delete reports
    }
}
