<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['reporter_id','user_image_id','report_text','report_type_id', 'subject'];

    public function reporter()
    {
        return $this->belongsTo('App\Models\User','reporter_id');
    }

    public function user_image()
    {
        return $this->belongsTo('App\Models\UserImage');
    }
}
