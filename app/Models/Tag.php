<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['weight'];

    public function user_images()
    {
        return $this->belongsToMany('App\Models\UserImage', 'tag_user_image');
    }

    /**
     * Get the weight flag for the tag.
     *
     * @return bool
     */
    public function getWeightAttribute()
    {
        return $this->user_images()->count();
    }

    use HasFactory;
}
