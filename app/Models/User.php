<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['weight'];

    /**
     * Get the weight flag for the user.
     *
     * @return bool
     */
    public function getWeightAttribute()
    {
        return $this->user_images()->count();
    }

    public function user_images()
    {
        return $this->hasMany('App\Models\UserImage');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report', 'reporter_id');
    }

    /**
     * The user images that the user has as favorites.
     */
    public function favorites()
    {
        return $this->belongsToMany('App\Models\UserImage','favorites')->using('App\Models\Favorite');
    }

    //Role functions

    public function onlyStandard(){

        //User's only role is 'Standard' user
        $user_roles = $this->roles()->get();
        return $user_roles->count() == 1 && $user_roles->first()->name == 'Standard';
    }

    public function isStandard(){

        //User is a Standard user
        return $this->roles()->where('name', 'Standard')->first();
    }

    public function isModerator(){

        //User is a Moderator
        return $this->roles()->where('name', 'Moderator')->first();
    }

    public function isAdmin(){

        //User is an Admin
        return $this->roles()->where('name', 'Admin')->first();
    }

    public function promoteUser(){
        if($this->onlyStandard()){
            $this->roles()->attach(Role::where('name','Moderator')->first());
            return true;
        }
        return false; //Couldn't promote user
    }

    public function demoteUser(){
        if($this->isStandard() && $this->isModerator()){
            return $this->roles()->detach(Role::where('name','Moderator')->first());
        }
        return false; //Couldn't demote user
    }
}
