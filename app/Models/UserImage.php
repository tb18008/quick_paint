<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class UserImage extends Model
{
    use SoftDeletes, Searchable;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['name'] = $this->name;
        $array['user'] = $this->user['name'];   //User's name
        $array['tags'] = $this->tags()->get()->pluck('name');   //Tag names

        return $array;
    }

    protected $fillable = ['url', 'image', 'thumbnail_url', 'name', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'tag_user_image');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report');
    }

    /**
     * The users that have this user image as a favorite.
     */
    public function favorites()
    {
        return $this->belongsToMany('App\Models\User','favorites')->using('App\Models\Favorite');
    }

    use HasFactory;
}
