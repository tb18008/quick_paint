<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserImage;
use App\Services\UserImageService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Intervention\Image\Facades\Image as Image;
use Tests\Services\ResponseDataService;
use Tests\Services\UserFactoryService;
use Tests\TestCase;

class UserImagesTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;
    private $user_factory;
    private $user_image_factory;

    public function setUp() : void
    {
        parent::setUp();

        //Models required for creating user images
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
        $this->user_factory = new UserFactoryService();
    }

    /**
    +-----------+-------------------------------------+------------------------------------------------+
    | Method    | URI                                 | Test                                           |
    +-----------+-------------------------------------+------------------------------------------------+
    | POST      | user_images                         | user_images_store_test                         |
    | GET|HEAD  | user_images                         | user_images_index_test                         |
    | GET|HEAD  | user_images/create                  | user_images_create_test                        |
    | GET|HEAD  | user_images/{user_image_id}/restore | user_images_restore_test                       |
    | PUT|PATCH | user_images/{user_image}            | user_images_update_test                        |
    | DELETE    | user_images/{user_image}            | user_images_destroy_test                       |
    | GET|HEAD  | user_images/{user_image}            | user_images_show_test                          |
    | GET|HEAD  | user_images/{user_image}/edit       | user_images_edit_test                          |
    +-----------+-------------------------------------+------------------------------------------------+
     */


    /**
     * Tests that the 'user_images' get route without a query returns the
     * same user image index view to guests and authenticated users.
     *
     * @test
     * @group unit_route
     * @group resource_index
     * @group acting_as_user
     * @return void
     */
    public function user_images_index_test()
    {
        //Getting user images index as a guest
        $response = $this->get('user_images');    //Send the GET request

        $this->assertGuest();

        //Request is authorized and user receives the view
        $response->assertOk();
        $response->assertViewIs('user_image.index');

        //View doesn't contain any user images
        $this->assertEmpty($this->response_data_service->get_response_data($response, 'user_images'));

        //Seed database with user images
        $this->artisan('db:seed --class=TagSeeder');
        $this->artisan('db:seed --class=UserImageSeeder');

        $response = $this->get('user_images');    //Send the GET request

        //View contains user images
        $guest_user_images = $this->response_data_service->get_response_data($response, 'user_images');

        $this->assertNotEmpty($guest_user_images);

        //Get user images index as authenticated user
        $response = $this->actingAs(User::factory()->create())->get('user_images');    //Send the GET request

        //Guest users and authenticated users get the same user images
        $this->assertEquals($guest_user_images, $this->response_data_service->get_response_data($response, 'user_images'));

    }

    /**
     * Tests that the 'user_images/create' get route returns a user images create
     * form only if user is authenticated and has a standard user role
     *
     * @test
     * @group unit_route
     * @group resource_create
     * @group acting_as_user
     * @return void
     */
    public function user_images_create_test()
    {
        //Getting user image create form as a guest
        $response = $this->get('user_images/create');    //Send the GET request

        $this->assertGuest();

        //User is redirected to login page to authenticate
        $response->assertRedirect('login');

        //Create users of each role and request the user image create form as each user
        $users = $this->user_factory->get_users_with_roles();

        foreach ($users as $user){

            $response = $this->actingAs($user)->get('user_images/create');

            if($user->isStandard()){    //Standard users are authorized to access route
                //Authorized users get the right status code and user image create form
                $response->assertOk();
                $response->assertViewIs('user_image.create');
            }
            else $response->assertForbidden(); //Other users are forbidden
        }
    }

    /**
     * Tests that posting data to route 'user_images':
     *  - requires authenticated user;
     *  - requires user to have the role 'standard' to be authorized;
     *  - request with valid data calls controller to create a new user image
     * with matching user image name, user_id, tags and image data.
     *
     * @test
     * @group unit_route
     * @group resource_store
     * @group acting_as_user
     * @return void
     */
    public function user_images_store_test()
    {

        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to post a user image as a guest
        $response = $this->post('user_images');    //Send the request

        $this->assertGuest();

        //User is redirected to login page because route uses authentication middleware
        $response->assertRedirect('login');


        //POST DATA AUTHORIZATION AND VALIDATION

        //Send request from a specific location
        //to assert that user is redirected back to the same location
        $from_location_url = '/user_image/create';

        //Getting user image post response while authenticated
        $users = $this->user_factory->get_users_with_roles();

        foreach ($users as $user){

            $response = $this->actingAs($user)->from($from_location_url)->post('user_images');    //Send the POST request
            $this->assertAuthenticated();

            if($user->isStandard()){

                //Response contains errors about the missing required fields
                $response->assertSessionHasErrors(['image_data']);
                $response->assertRedirect($from_location_url);  //Standard users get redirected back
            }
            else{

                //Assert that users with other roles are forbidden to post user image data
                $response->assertForbidden();
            }
        }


        //POST USER IMAGE DATA IF USER IS NOT FORBIDDEN

        //Test acting as the user that is authorized to post a user image
        $user = $users['standard'];

        //Data sent in request and checked after user image is stored
        $user_image_name = 'User image name';
        $tag_names = collect(['tag','tag_1','tag_2','tag_3','tag_4','tag_5', 'tag_6']);
        $image_data = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAACWSURBVHic7dBBEcAwAMOwdfw5pzD0qIXA57Nt38N+HaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gHYBVToEfBvPJk0AAAAASUVORK5CYII=';

        //Set sent image to variable to compare with stored image
        $image_sent = Image::make($image_data);

        $response = $this
            ->actingAs($user)
            ->from($from_location_url)
            ->post('user_images',[
                'name' => $user_image_name,
                /* Tag string separated by commas with spaces at the start and end of tag:
                 * - tag without whitespace characters, dashes or underscores;
                 * - tag with capital letters;
                 * - tag with underscore;
                 * - tag with whitespace character;
                 * - tag with multiple whitespace characters;
                 * - tag with dash;
                 * - tag with multiple dashes.
                 */
                'tag_string' => ' tag , TAG_1 , tag_2, tag 3 , tag  4 , tag-5 , tag--6 ',
                //64x64 px png, valid image data
                'image_data' => $image_data
            ]);

        $response->assertSessionHasNoErrors();  //User image data is valid
        $response->assertRedirect('user_images/1'); //Redirect to show view of created user image

        $user_images = UserImage::all();

        //There is only one user image stored
        $this->assertCount(1, $user_images);

        $user_image = $user_images->first();

        //Assert that user image in database has the same name as data sent in request
        $this->assertEquals($user_image_name, $user_image->name);

        //Assert that user image in database has the same user id as the post request sender
        $this->assertEquals($user_image->user_id, $user->id);

        //Assert that user image in database has the same tag names as data sent in request
        $this->assertEquals($tag_names, $user_image->tags()->get()->pluck('name'));

        //Assert that stored user image png is the same as the png for image data sent in request
        $image_in_storage = Image::make(storage_path('app/public/user_images/'.$user_image->url));
        $this->assertTrue($this->image_pixels_equal($image_sent,$image_in_storage));

        //Delete the created user image (along with deleting the new file)
        $user_image_service = new UserImageService();
        $user_image_service->destroy($user_image,true);
    }

    /**
     * Compare if the two images are same height width and
     * one image's pixel at X and Y has the same color at
     * the other image's pixel at X and Y
     *
     * @param $image_a
     * @param $image_b
     * @return bool
     */
    private function image_pixels_equal($image_a, $image_b){

        //Compare widths and heights
        if($image_a->height() != $image_b->height()) return false;
        if($image_a->width() != $image_b->width()) return false;

        //Iterate through each pixel in both images
        for($i=0;$i<$image_a->height();$i++){
            for($j=0;$j<$image_a->width();$j++){
                if($image_a->pickColor($i, $j) != $image_b->pickColor($i, $j)) return false;
            }
        }

        //Pixels match
        return true;
    }

    /**
     * Tests that posting data to route 'user_images':
     *  - requires authenticated user;
     *  - requires user to have the role 'standard' to be authorized;
     *  - request with valid data calls controller to create a new user image
     * with matching user image name, user_id, tags and image data.
     *
     * @test
     * @group unit_route
     * @group resource_update
     * @group acting_as_user
     * @return void
     */
    public function user_images_edit_test()
    {
        //Create a user image with an owner
        $user_image_owner = User::factory()->create();
        $user_image = UserImage::factory()->create(['user_id'=>$user_image_owner->id]);


        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to get the user image edit view
        $response = $this->get('user_images/'.$user_image->id.'/edit');    //Send the request

        $this->assertGuest();

        //User is redirected to login page because route uses authentication middleware
        $response->assertRedirect('login');


        //AUTHENTICATED USER AUTHORIZATION AND VIEW DATA COMPARISON

        //Getting user image edit form while authenticated
        $users = $this->user_factory->get_users_with_roles();
        //Add a user that owns the user image
        $users['owner'] = $user_image_owner;

        //Try to get a status code 200 response from route with each user
        foreach ($users as $user){

            $response = $this->actingAs($user)->get('user_images/'.$user_image->id.'/edit');
            $this->assertAuthenticated();

            if($user == $user_image_owner){
                //Only user image owners get status code 'OK' and the user image edit view
                $response->assertOk();
                $response->assertViewIs('user_image.edit');

                //View has a user image with tags attached to it
                $user_image_in_response = $this->response_data_service->get_response_data($response,'user_image');
                $this->assertCount(1, $user_image_in_response);

                //Assert that each of the created user image tags are in the response user image's tags
                $tags_in_response = $user_image_in_response->first()->tags()->get()->pluck('id');
                foreach ($user_image->tags()->get() as $tag) $this->assertContains($tag->id,$tags_in_response);

                //There are no extra tags
                $this->assertSameSize($user_image->tags()->get(), $tags_in_response);
            }
            else{
                //Assert that users that don't own the user image can't get the edit view
                $response->assertForbidden();
            }
        }
    }

    /**
     * Tests that posting data to route 'user_images' with the user image as a parameter:
     *  - requires authenticated user;
     *  - requires user to be the owner of user image in parameter;
     *  - request with valid data calls controller to update the existing user image
     * with matching user image name, user_id, tags and image data.
     *
     * @test
     * @group unit_route
     * @group resource_update
     * @group acting_as_user
     * @return void
     */
    public function user_images_update_test()
    {
        //Create a user image with an owner
        $user_image_owner = User::factory()->create();
        $user_image = UserImage::factory()->create(['user_id'=>$user_image_owner->id]);


        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to patch a user image as a guest
        $response = $this->patch('user_images/'.$user_image->id);    //Send the request

        $this->assertGuest();

        //User is redirected to login page because route uses authentication middleware
        $response->assertRedirect('login');


        //AUTHENTICATED USER AUTHORIZATION

        //Send request from a specific location
        //to assert that user is redirected back to the same location
        $from_location_url = '/user_image/edit';

        //Getting user image patch response while authenticated
        $users = $this->user_factory->get_users_with_roles();
        //Add a user that owns the user image
        $users['owner'] = $user_image_owner;

        //Try to get a status code 200 response from route with each user
        foreach ($users as $user){

            $response = $this->actingAs($user)->from($from_location_url)->patch('user_images/'.$user_image->id);
            $this->assertAuthenticated();

            if($user == $user_image_owner){

                //Response contains errors about the missing required fields
                $response->assertSessionHasErrors(['image_data']);
                $response->assertRedirect($from_location_url);
            }
            else{
                //Assert that users that don't own the user image can't update the user image
                $response->assertForbidden();
            }
        }


        //POST USER IMAGE DATA IF USER IS NOT FORBIDDEN

        $user_images = UserImage::all();

        //There is only one user image stored before update
        $this->assertCount(1, $user_images);

        //Test acting as the user that is authorized to post a user image
        $user = $users['owner'];

        //Data sent in request and checked after user image is updated
        $user_image_name = 'User image name';
        $tag_names = collect(['tag','tag_1','tag_2','tag_3','tag_4','tag_5', 'tag_6']);
        $image_data = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAACWSURBVHic7dBBEcAwAMOwdfw5pzD0qIXA57Nt38N+HaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gNYAHaA1QAdoDdABWgN0gHYBVToEfBvPJk0AAAAASUVORK5CYII=';

        //Set sent image to variable to compare with updated image
        $image_sent = Image::make($image_data);

        $this->withoutExceptionHandling();

        $response = $this
            ->actingAs($user)
            ->from($from_location_url)
            ->patch('user_images/'.$user_image->id,[
                'name' => $user_image_name,
                /* Tag string separated by commas with spaces at the start and end of tag:
                 * - tag without whitespace characters, dashes or underscores;
                 * - tag with capital letters;
                 * - tag with underscore;
                 * - tag with whitespace character;
                 * - tag with multiple whitespace characters;
                 * - tag with dash;
                 * - tag with multiple dashes.
                 */
                'tag_string' => ' tag , TAG_1 , tag_2, tag 3 , tag  4 , tag-5 , tag--6 ',
                //64x64 px png, valid image data
                'image_data' => $image_data
            ]);

        $response->assertSessionHasNoErrors();  //User image data is valid
        $response->assertRedirect('user_images/'.$user_image->id); //Redirect to show view of updated user image

        $user_images = UserImage::all();

        //There is only one user image after update
        $this->assertCount(1, $user_images);

        $user_image = $user_images->first();

        //Assert that user image in database has the same name as data sent in request
        $this->assertEquals($user_image_name, $user_image->name);

        //Assert that user image in database has the same user id as the post request sender
        $this->assertEquals($user_image->user_id, $user->id);

        //Assert that user image in database has the same tag names as data sent in request
        $this->assertEquals($tag_names, $user_image->tags()->get()->pluck('name'));

        //Assert that updated user image png is the same as the png for image data sent in request
        $image_in_storage = Image::make(storage_path('app/public/user_images/'.$user_image->url));
        $this->assertTrue($this->image_pixels_equal($image_sent,$image_in_storage));

        //Delete the created user image (along with deleting the new file)
        $user_image_service = new UserImageService();
        $user_image_service->destroy($user_image,true);
    }

    /**
     * Tests that 'user_images' delete route:
     *  - requires authenticated user;
     *  - soft deletes user image specified in parameter if user is authorized;
     *  - force deletes user image specified in parameter if user is authorized
     *  and input field's 'forced' value is true.
     *
     * @test
     * @group unit_route
     * @group resource_destroy
     * @group acting_as_user
     * @return void
     */
    public function user_images_destroy_test()
    {
        //Create a user image to destroy with an owner
        $user_image_owner = User::factory()->create();
        $user_image = UserImage::factory()->create(['user_id'=>$user_image_owner->id]);
        $this->assertDatabaseCount('user_images',1);


        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to delete a user image as a guest
        $response = $this->delete('user_images/'.$user_image->id);    //Send the request

        $this->assertGuest();

        //User is redirected to login page because route uses authentication middleware
        $response->assertRedirect('login');


        //AUTHENTICATED USER AUTHORIZATION

        //Set users to variable including the owner of user image
        $users = $this->user_factory->get_users_with_roles();
        //Add a user that owns the user image
        $users['owner'] = $user_image_owner;

        //Send request from a specific location
        //to assert that user is redirected back to the same location
        $from_location_url = '/user_image/'.$user_image->id;

        //Soft delete user image without request input 'forced'

        foreach ($users as $user){

            $response = $this->actingAs($user)
                ->from($from_location_url)
                ->delete('user_images/'.$user_image->id,[
                    //Input missing specifying a soft delete
                ]);

            if($user->isModerator() && $user != $user_image_owner){
                //If user is moderator and doesn't own the user image

                //User is returned to user image index
                $response->assertRedirect('user_images');

                //There is one user image that is soft deleted and it's id matches the
                //user image send in request parameter
                $soft_deleted_user_images = UserImage::onlyTrashed()->get();
                $this->assertCount(1,$soft_deleted_user_images);
                $soft_deleted_user_image = $soft_deleted_user_images->first();
                $this->assertEquals($user_image->id,$soft_deleted_user_image->id);

                $soft_deleted_user_image->restore(); //Restore user image for further tests
            }
            else{
                //Assert that users other users are forbidden to soft delete the user image
                $response->assertForbidden();

                //Assert that no user images have been soft deleted
                $soft_deleted_user_images = UserImage::onlyTrashed()->get();
                $this->assertCount(0,$soft_deleted_user_images);
            }
        }

        //Soft delete user image with request input 'forced'

        $response = $this->actingAs($users['moderator'])
            ->from($from_location_url)
            ->delete('user_images/'.$user_image->id,[
                'forced' => false //Input specifying a soft delete
            ]);

        //User is returned to user image index
        $response->assertRedirect('user_images');

        //There is one user image that is soft deleted and it's id matches the
        //user image send in request parameter
        $soft_deleted_user_images = UserImage::onlyTrashed()->get();
        $this->assertCount(1,$soft_deleted_user_images);
        $soft_deleted_user_image = $soft_deleted_user_images->first();
        $this->assertEquals($user_image->id,$soft_deleted_user_image->id);

        $soft_deleted_user_image->restore(); //Restore user image for further tests


        //Force delete user image

        //Assert that all users need a required field to be filled in request
        foreach ($users as $user){

            $response = $this->actingAs($user)
                ->from($from_location_url)
                ->delete('user_images/'.$user_image->id,[
                    'forced' => true    //Input specifying a force delete
                ]);
            if($user == $user_image_owner || $user->isAdmin()){
                //If user is admin or the user image owner

                //User is returned to user image index
                $response->assertRedirect('user_images');

                //There are no user images, not even among soft deleted user images
                $all_user_images = UserImage::withTrashed()->get();
                $this->assertCount(0,$all_user_images);

                //Create new user image for further tests
                $user_image = UserImage::factory()->create(['user_id'=>$user_image_owner->id]);

            }
            else{
                //Assert that users other users are forbidden to force delete the user image
                $response->assertForbidden();

                //Assert that the user image hasn't been soft deleted or force deleted
                $this->assertDatabaseCount('user_images',1);
            }
        }
    }

    /**
     * Tests that 'user_images' restore route:
     *  - requires authenticated user;
     *  - forbids users that don't have the admin role;
     *  - restores soft deleted user image.
     *
     * @test
     * @group unit_route
     * @group resource_restore
     * @group acting_as_user
     * @return void
     */
    public function user_images_restore_test()
    {
        //Create a user image to delete and attempt to restore
        $user_image = UserImage::factory()->create();
        $user_image->delete();
        $user_images = UserImage::all();
        $soft_deleted_user_images = UserImage::onlyTrashed()->get();

        //Assert that in the beginning there are no user images except the soft deleted one
        $this->assertCount(0,$user_images);
        $this->assertCount(1,$soft_deleted_user_images);


        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to restore a user image as a guest
        $response = $this->get('user_images/'.$user_image->id.'/restore');    //Send the request

        $this->assertGuest();

        //User is redirected to login page because route uses authentication middleware
        $response->assertRedirect('login');


        //AUTHENTICATED USER AUTHORIZATION

        //Set users of each role to variable
        $users = $this->user_factory->get_users_with_roles();

        //Send request from a specific location
        //to assert that user is redirected back to the same location
        $from_location_url = '/';

        //Assert that users go through authorization to access controller method
        foreach ($users as $user){

            $response = $this->actingAs($user)
                ->from($from_location_url)
                ->get('user_images/'.$user_image->id.'/restore');

            if($user->isAdmin()){   //If user is admin

                //User is returned back to url where request was sent from
                $response->assertRedirect($from_location_url);

                //The only user image is not soft deleted
                $user_images = UserImage::all();
                $soft_deleted_user_images = UserImage::onlyTrashed()->get();
                $this->assertCount(1,$user_images);
                $this->assertCount(0,$soft_deleted_user_images);

                $user_image->delete(); //Soft delete user image to not affect further tests
            }
            else{
                //Assert that users other users are forbidden to restore the user image
                $response->assertForbidden();

                //Assert that no user images have been restored
                $soft_deleted_user_images = UserImage::onlyTrashed()->get();
                $this->assertCount(1,$soft_deleted_user_images);
            }
        }
    }



}
