<?php

namespace Tests\Feature;

use App\Models\Tag;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\Services\ResponseDataService;
use Tests\Services\UserFactoryService;
use Tests\Services\UserImageFactoryService;
use Tests\TestCase;

class UserImagesQueryTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;
    private $user_factory;
    private $user_image_factory;

    public function setUp() : void
    {
        parent::setUp();

        //Models required for creating user images
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
        $this->user_factory = new UserFactoryService();
        $this->user_image_factory = new UserImageFactoryService();
    }


    /**
     * Tests that specifying a user image's name in query string:
     *  - returns one or multiple user images with a matching name;
     *  - doesn't return a user image with a similar name;
     *  - returns nothing if there are no user images with a matching name.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function query_by_user_image_name_test(){

        //GET USER IMAGES BY NAME

        //Create user image that would be in response if a query was just as fuzzy as a search
        $this->user_image_factory->get_named_user_image('B');        //Would show up for search_term=A B
        $this->user_image_factory->get_named_user_image('C D');      //Would show up for search_term=C

        //Set query string and expected user image models in response
        $user_images_for_query = [
            'name=A' => [$this->user_image_factory->get_named_user_image('A')],      //Only one with matching name
            'name=A B' => [$this->user_image_factory->get_named_user_image('A B')],  //Only one with matching name, no user images with similar names
            'name=C' => [],  //No user images where name is exactly 'C'
            'name=D' => [
                $this->user_image_factory->get_named_user_image('D'),    //Different user images
                $this->user_image_factory->get_named_user_image('D')     //that have the same name
            ],
        ];

        //Send get request with queries and compare response with expected user images
        $this->assert_expected_user_images($user_images_for_query);
    }

    /**
     * Tests that specifying a user image's tag ids in query string:
     *  - returns one or multiple user images with a matching name;
     *  - doesn't return a user image with a similar name;
     *  - returns nothing if there are no user images with a matching name.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function query_by_user_image_tags_test(){

        //GET USER IMAGES BY TAGS

        //Create user image that has one of the queried tag ids but not all

        Tag::factory()->create(['id'=>7]);

        $first_user_image_with_tag_id_2 = $this->user_image_factory->get_user_image_with_tags([2]);

        $second_user_image_with_tag_id_2 = UserImage::factory()->create();
        $second_user_image_with_tag_id_2->tags()->detach();
        $second_user_image_with_tag_id_2->tags()->attach($first_user_image_with_tag_id_2->tags()->first());

        //Set query string and expected user image models in response
        $user_images_for_query = [
            'tags[]=0' => [],   //No user images for tag that doesn't have related user images
            'tags[]=1' => [$this->user_image_factory->get_user_image_with_tags([1])],   //Only one with one matching tag id
            'tags[]=2' => [
                $first_user_image_with_tag_id_2,    //Multiple user images with one matching tag id
                $second_user_image_with_tag_id_2
            ],
            'tags[]=3&tags[]=4' => [$this->user_image_factory->get_user_image_with_tags([3,4])], //One with multiple matching tag ids
            'tags[]=5&tags[]=6' => [
                //A user image must have all the tags specified, otherwise it gets filtered out
                //$this->user_image_factory->get_user_image_with_tags([5]),
            ],
            'tags[]=' => [],    //Since query parameter is an array, pushing null to array still counts as filled request parameter
        ];

        //Send get request with queries and compare response with expected user images
        $this->assert_expected_user_images($user_images_for_query);
    }

    /**
     * Tests that specifying a user by id in query string:
     *  - returns all the user images the specified user has;
     *  - returns no user images if user has none;
     *  - returns no user images if user doesn't exist.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function query_by_user_image_user_test(){

        DB::table('users')->truncate(); //Delete all seeded users to create them by ids

        //GET USER IMAGES BY USER

        //Set query string and expected user image models in response
        $user_images_for_query = [
            'user=1' => $this->user_image_factory->get_user_images_for_user(1, 1),  //Only one user image for a user that has one user image
            'user=2' => $this->user_image_factory->get_user_images_for_user(2, 0),  //No user images for user that doesn't have user images
            'user=3' => [], //No user images for user that doesn't exist
            'user=4' => $this->user_image_factory->get_user_images_for_user(4, 4)   //All user images for user that has multiple user images
        ];

        //Send get request with queries and compare response with expected user images
        $this->assert_expected_user_images($user_images_for_query);
    }

    /**
     * Tests that the 'reports' get route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - responds with the same index if there are no reports in database;
     *  - responds with a different depending on role of the user if there are reports in database;
     *  - responds with a different if user owns some amount of reports.
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function query_only_trashed_user_images_test(){

        //Set an array with user for every type of role
        $users = $this->user_factory->get_users_with_roles();

        foreach ($users as $user){

            //Request only trashed user images as each of the users
            $response = $this->actingAs($user)->get('user_images?only_trashed=true');

            if($user->isModerator() || $user->isAdmin()){
                //Only admins or moderators are authorized to access user images that have been soft deleted
                $response->assertOk();
            }
            else{
                $response->assertForbidden();   //Other users are forbidden because of their role
            }
        }

        //Create user images (undeleted, soft deleted and force deleted) for testing user images contained in response
        $undeleted_user_image = UserImage::factory()->create();
        $soft_user_image = UserImage::factory()->create();
        $soft_user_image->delete();
        $force_deleted_user_image = UserImage::factory()->create();
        $force_deleted_user_image->forceDelete();

        //Use only the authorized users in following assertions
        $authorized_users = [
            'moderator' => $users['moderator'],
            'admin' => $users['admin'],
        ];

        foreach ($authorized_users as $authorized_user){
            $this->actingAs($authorized_user);

            //Set query string and expected user image models in response
            $user_images_for_query = [
                '' => [$undeleted_user_image],  //Query without specifying only trashed user images contains the one undeleted user image
                'only_trashed=' => [$undeleted_user_image],  //If the value is missing the response isn't filled
                //Query string value doesn't have to be a boolean value for it to return the soft deleted user images
                'only_trashed=false' => [$soft_user_image],
                'only_trashed=true' => [$soft_user_image],
            ];

            //Send get request with queries and compare response with expected user images
            $this->assert_expected_user_images($user_images_for_query);
        }
    }

    /**
     * Tests how user image query works with all three types of query and trashed images query if:
     *  - user image is not deleted in any way;
     *  - user image is soft deleted;
     *  - user image is force deleted.
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function query_user_images_test(){

        //Delete all seeded users to create users by id
        DB::table('users')->truncate();



        //REQUEST WITH ALL THREE QUERY FIELDS (WITHOUT TRASHED)


        //User image is not deleted in any way

        //Fields used in query string
        $user_image_attributes = [
            'user_id' => 1,
            'name' => 'User image name',
            'first_tag_id' => 1,
            'second_tag_id' => 2
        ];

        //User image for only user_id field, which should not show up in response because all other fields don't match
        User::factory()->create(['id'=>$user_image_attributes['user_id']]);

        //User image for user_id, name and tag fields
        $expected_user_image = UserImage::factory()->create([
            'user_id'=>$user_image_attributes['user_id'],
            'name'=>$user_image_attributes['name'],
            'deleted_at'=>null,
        ]);
        $expected_user_image->tags()->attach(Tag::factory()->create(['id'=>$user_image_attributes['first_tag_id']]));
        $expected_user_image->tags()->attach(Tag::factory()->create(['id'=>$user_image_attributes['second_tag_id']]));

        $query =
            'user='.$user_image_attributes['user_id'].'&'.
            'name='.$user_image_attributes['name'].'&'.
            'tags[]='.$user_image_attributes['first_tag_id'].'&'.
            'tags[]='.$user_image_attributes['second_tag_id'];

        $response = $this->get('user_images?'.$query);  //Send request

        //Retrieve actual user images in response
        $actual_user_images = $this->response_data_service->get_response_data($response, 'user_images');
        //Response has a user image with the expected user image id
        $this->assertContains($expected_user_image->id,collect($actual_user_images)->pluck('id'));
        //Expected user image is the only one in the response
        $this->assertCount(1, $actual_user_images);



        //REQUEST WITH ALL THREE QUERY FIELDS (WITH TRASHED)


        //User image is soft deleted

        $expected_user_image->delete(); //Soft delete the expected user image to test only_trashed query

        //Set an array with user for every type of role
        $users = $this->user_factory->get_users_with_roles();

        foreach ($users as $user){

            //Query without the only_trashed field to assert that user image has been soft deleted
            $query =
                'user='.$user_image_attributes['user_id'].'&'.
                'name='.$user_image_attributes['name'].'&'.
                'tags[]='.$user_image_attributes['first_tag_id'].'&'.
                'tags[]='.$user_image_attributes['second_tag_id'];

            $response = $this->actingAs($user)->get('user_images?'.$query);

            //The user image is no longer in the response with the same query because it is soft deleted
            $this->assertCount(0,
                $this->response_data_service->get_response_data($response, 'user_images')
            );

            //Query with the only_trashed field to assert that user image has been soft deleted but not force deleted
            $query = 'user='.$user_image_attributes['user_id'].'&'.
                'name='.$user_image_attributes['name'].'&'.
                'tags[]='.$user_image_attributes['first_tag_id'].'&'.
                'tags[]='.$user_image_attributes['second_tag_id'].'&'.
                'only_trashed=true';

            $response = $this->actingAs($user)->get('user_images?'.$query);

            if($user->isModerator() || $user->isAdmin()){   //Only moderators or admins get the user images

                $response->assertOk();

                //Retrieve actual user images in response
                $actual_user_images = $this->response_data_service->get_response_data($response, 'user_images');

                //The soft deleted user image id is among the actual user images in the response
                $this->assertContains($expected_user_image->id, collect($actual_user_images)->pluck('id'));

                //Expected user image is the only one in the response
                $this->assertCount(1, $actual_user_images);
            }
            else $response->assertForbidden();  //Other users are unauthorized
        }



        //User image is force deleted

        $expected_user_image->forceDelete();    //Force delete user image to assert that it will no longer be in the same queries

        foreach ($users as $user){

            //Query without the only_trashed field to assert that user image has not been restored
            $query =
                'user='.$user_image_attributes['user_id'].'&'.
                'name='.$user_image_attributes['name'].'&'.
                'tags[]='.$user_image_attributes['first_tag_id'].'&'.
                'tags[]='.$user_image_attributes['second_tag_id'];

            $response = $this->actingAs($user)->get('user_images?'.$query);

            //There are no user images in response
            $this->assertCount(0,
                $this->response_data_service->get_response_data($response, 'user_images')
            );

            //Query with the only_trashed field again and assert that user image can't be queried
            $query = 'user='.$user_image_attributes['user_id'].'&'.
                'name='.$user_image_attributes['name'].'&'.
                'tags[]='.$user_image_attributes['first_tag_id'].'&'.
                'tags[]='.$user_image_attributes['second_tag_id'].'&'.
                'only_trashed=true';

            $response = $this->actingAs($user)->get('user_images?'.$query);

            if($user->isModerator() || $user->isAdmin()){
                $response->assertOk();

                //There are still no user images in the response
                $this->assertCount(0,
                    $this->response_data_service->get_response_data($response, 'user_images')
                );
            }
            else $response->assertForbidden();
        }
    }


    /**
     * Tests that the 'reports' get route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - responds with the same index if there are no reports in database;
     *  - responds with a different depending on role of the user if there are reports in database;
     *  - responds with a different if user owns some amount of reports.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function user_images_request_ajax_test(){


        //AJAX CALL WITHOUT QUERY

        //Send user image index with AJAX call
        $response = $this->get('user_images',['HTTP_X-Requested-With' => 'XMLHttpRequest']);
        //Request was valid and the response is a search results view not user images index
        $response->assertOk();
        $response->assertViewIs('user_image.search_results');

        //Create a user image to be retrieved from response
        $expected_user_image = UserImage::factory()->create();

        $response = $this->get('user_images',['HTTP_X-Requested-With' => 'XMLHttpRequest']);

        $response_user_images = $this->response_data_service->get_response_data($response,'user_images');

        //Response has the expected user image and no other user image
        $this->assertContains($expected_user_image->id,collect($response_user_images)->pluck('id'));
        $this->assertCount(1,$response_user_images);

        $expected_user_image->forceDelete();    //Delete user image to not affect further assertions


        //AJAX CALL WITH QUERY

        DB::table('users')->truncate(); //Delete all seeded users to create users by id

        //Fields used in query string
        $user_image_attributes = [
            'user_id' => 1,
            'name' => 'User image name',
            'first_tag_id' => 1,
            'second_tag_id' => 2
        ];

        //User image for only user_id field, which should not show up in response because all other fields don't match
        User::factory()->create(['id'=>$user_image_attributes['user_id']]);

        //User image for user_id, name and tag fields
        $expected_user_image = UserImage::factory()->create([
            'user_id'=>$user_image_attributes['user_id'],
            'name'=>$user_image_attributes['name'],
            'deleted_at'=>null,
        ]);
        $expected_user_image->tags()->attach(Tag::factory()->create(['id'=>$user_image_attributes['first_tag_id']]));
        $expected_user_image->tags()->attach(Tag::factory()->create(['id'=>$user_image_attributes['second_tag_id']]));

        $query =
            'user='.$user_image_attributes['user_id'].'&'.
            'name='.$user_image_attributes['name'].'&'.
            'tags[]='.$user_image_attributes['first_tag_id'].'&'.
            'tags[]='.$user_image_attributes['second_tag_id'];

        $response = $this->get('user_images?'.$query,['HTTP_X-Requested-With' => 'XMLHttpRequest']);

        $response->assertViewIs('user_image.search_results');   //Response view is not user image index view

        //Retrieve actual user images in response
        $actual_user_images = $this->response_data_service->get_response_data($response, 'user_images');

        //Response has a user image with the expected user image id
        $this->assertContains($expected_user_image->id,collect($actual_user_images)->pluck('id'));

        //Expected user image is the only one in the response
        $this->assertCount(1, $actual_user_images);
    }


    /**
     * Sends a 'user_images' get request for each query in array
     * where key-value pair's key is a query and value is an array of expected user images.
     * Compares the expected user images with actual user images in response
     * by user image id existence and sizes of arrays.
     *
     * @param array $user_images_for_query
     * @param bool $dump
     * @return void
     */
    private function assert_expected_user_images(array $user_images_for_query, bool $dump = false){

        // Iterate through the pairs (Query => Array of expected user images)
        foreach ($user_images_for_query as $query => $expected_user_images) {

            //Get response for given query
            $response = $this->get('user_images?'.$query);

            //Retrieve actual user images in response
            $actual_user_images = $this->response_data_service->get_response_data($response, 'user_images');

            if($dump) {
                dump([
                    'query' => $query,
                    'expected user images' => collect($expected_user_images)->pluck('id'),
                    'actual user images' => collect($actual_user_images)->pluck('id')
                ]);
            }

            $user_images[$query] = $actual_user_images;

            //Assert that actual user images have expected user images
            foreach ($expected_user_images as $expected_user_image)
                $this->assertTrue(
                    collect($actual_user_images)->pluck('id')
                        ->contains($expected_user_image->id)
                );
            //All expected user images are in actual user images

            //Expected user image count matches actual user image count
            $this->assertSameSize($expected_user_images, $actual_user_images);

            //There are no additional user images (search actual results match expected results)
        }

    }
}
