<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FavoritesTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();

        $this->artisan('db:seed');
    }

    /**
    +-----------+-------------------------------------+---------------------------------+
    | Method    | URI                                 | Test                            |
    +-----------+-------------------------------------+---------------------------------+
    | GET|HEAD  | favorites                           | favorites_get_route_test        |
    | GET|HEAD  | favorites_attach/{user_image_id}    | favorites_attach_get_route_test |
    | GET|HEAD  | favorites_detach/{user_image_id}    | favorites_detach_get_route_test |
    +-----------+-------------------------------------+---------------------------------+
     */


    /**
     * Tests that the 'favorites' route returns a user image index view.
     * If the user has favorites then the view contains data of them.
     *
     * @test
     * @group unit_route
     * @group acting_as_user
     * @return void
     */
    public function favorites_get_route_test()
    {
        //Test that route goes through authentication middleware and can't be accessed by guests
        $this->assertGuest();
        $response = $this->get('favorites');
        $response->assertRedirect('login');


        //Authenticate as user without favorites and request favorites index
        $response = $this->actingAs(User::factory()->create())->get('favorites');
        $response->assertViewIs('user_image.index');  //Check if returned view is the expected result

        //Response data contains no user images because user has no favorites
        $this->assertEmpty($this->getResponseData($response, 'user_images'));


        //Authenticate as user with 1 favorite user image
        $user = User::factory()->create();
        $user->favorites()->attach(UserImage::factory()->create());

        $response = $this->actingAs($user)->get('favorites'); //Request favorites for user with user images as favorites
        $response->assertViewIs('user_image.index');  //Check if returned view is the expected result

        //Response data contains at least one user image because user has favorites
        $this->assertNotEmpty($this->getResponseData($response, 'user_images'));

    }

    private function getResponseData($response, $key){

        $content = $response->getOriginalContent();

        $content = $content->getData();

        return $content[$key]->all();

    }

    /**
     * Tests that the 'favorites_attach' route:
     *  - requires an authenticated user;
     *  - forbids users that don't have standard or moderator role;
     *  - returns status code 'Not Found' if user image can't be found
     *  - returns status code 'OK' if user image was added to user's favorites
     *  - returns status code 'Bad Request' if user image already was in user's favorites
     *
     * @test
     * @group unit_route
     * @group acting_as_user
     * @return void
     */
    public function favorites_attach_get_route_test()
    {
        $users = $this->get_user_of_each_role();

        //Test that route goes through authentication middleware and can't be accessed by guests
        $this->assertGuest();
        $response = $this->get('favorites_attach/0');
        $response->assertRedirect('login');

        foreach ($users as $user){
            $this->actingAs($user); //Authenticate as user without favorites
            $this->assertAuthenticated();

            if($user->isStandard() || $user->isModerator()){
                $response = $this->get('favorites_attach/0');
                $response->assertNotFound();  //Controller returns status code 'Not Found' if the service couldn't find by given user_image_id

                $user_image_id = UserImage::factory()->create()->id;

                $response = $this->get('favorites_attach/'.$user_image_id);
                $response->assertOk();  //Controller returns status code 'OK' if the found user image isn't already favored by the user

                $response = $this->get('favorites_attach/'.$user_image_id);
                $response->assertStatus(400);  //Controller returns status code 'Bad Request' if the found user image is already favored by the user
            }
            else{
                //Request favorite attach with valid user image id
                $response = $this->get('favorites_attach/'.UserImage::factory()->create()->id);
                $response->assertForbidden();  //Policy forbids user from accessing role if user does not have standard or moderator role
            }

        }

    }

    /**
     * Tests that the 'favorites_detach' route:
     *  - requires an authenticated user;
     *  - forbids users that don't have standard or moderator role;
     *  - returns status code 'Not Found' if user image can't be found
     *  - returns status code 'OK' if user image was removed to user's favorites
     *  - returns status code 'Bad Request' if user image wasn't already in user's favorites
     *
     * @test
     * @group unit_route
     * @group acting_as_user
     * @return void
     */
    public function favorites_detach_get_route_test()
    {
        $users = $this->get_user_of_each_role();

        //Test that route goes through authentication middleware and can't be accessed by guests
        $this->assertGuest();
        $response = $this->get('favorites_detach/0');
        $response->assertRedirect('login');

        foreach ($users as $user){
            $this->actingAs($user); //Authenticate as user without favorites
            $this->assertAuthenticated();

            if($user->isStandard() || $user->isModerator()){
                $response = $this->get('favorites_detach/0');
                $response->assertNotFound();  //Controller returns status code 'Not Found' if the service couldn't find by given user_image_id

                $user_image_id = UserImage::factory()->create()->id;

                $response = $this->get('favorites_detach/'.$user_image_id);
                $response->assertStatus(400);  //Controller returns status code 'Bad Request' if the found user image is not favored by the user

                $user->favorites()->attach($user_image_id);

                $response = $this->get('favorites_detach/'.$user_image_id);
                $response->assertOk();  //Controller returns status code 'OK' if the found user image is already favored by the user
            }
            else{
                //Request favorite attach with valid user image id
                $response = $this->get('favorites_detach/'.UserImage::factory()->create()->id);
                $response->assertForbidden();  //Policy forbids user from accessing role if user does not have standard or moderator role
            }

        }

    }

    /**
     * Creates users for:
     *  - standard
     *  - moderator
     *  - admin
     * roles for role dependant authorization tests.
     * Includes the creation of a user without a role.
     *
     * @return array
     */

    private function get_user_of_each_role(){
        $standard_role = Role::where('name','Standard')->first();
        $moderator_role = Role::where('name','Moderator')->first();
        $admin_role = Role::where('name','Admin')->first();

        $roleless_user = User::factory()->create();

        $standard_user = User::factory()->create();
        $standard_user->roles()->attach($standard_role);

        $moderator_user = User::factory()->create();
        $moderator_user->roles()->attach($moderator_role);

        $admin_user = User::factory()->create();
        $admin_user->roles()->attach($admin_role);

        //Array of users to iterate through
        return [
            $roleless_user,
            $standard_user,
            $moderator_user,
            $admin_user
        ];
    }

}
