<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RolesTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();

        $this->artisan('db:seed');
    }

    /**
    +-----------+-------------------------------------+---------------------------------+
    | Method    | URI                                 | Test                            |
    +-----------+-------------------------------------+---------------------------------+
    | PATCH     | role_elevate                        | role_elevate_patch_route_test   |
    +-----------+-------------------------------------+---------------------------------+
     */

    /**
     * Tests an Admin user's role elevation feature using the 'role_elevate' route
     *
     * @test
     * @group unit_route
     * @group acting_as_user
     * @return void
     */
    public function role_elevate_patch_route_test()
    {
        //Set variables with roles and users with the roles attached

        $standard_role = Role::where('name','Standard')->first();
        $moderator_role = Role::where('name','Moderator')->first();
        $admin_role = Role::where('name','Admin')->first();

        $roleless_user = User::factory()->create();

        $standard_user = User::factory()->create();
        $standard_user->roles()->attach($standard_role);

        $moderator_user = User::factory()->create();
        $moderator_user->roles()->attach($moderator_role);

        $admin_user = User::factory()->create();
        $admin_user->roles()->attach($admin_role);

        //Array of users to iterate through
        $users = [
            $roleless_user,
            $standard_user,
            $moderator_user,
            $admin_user
        ];

        //Test that route goes through authentication middleware and can't be accessed by guests
        $response = $this->patch('role_elevate',array_merge($this->validPromoteRoleData($standard_user),['user_id' => 0]));
        $response->assertRedirect('login');

        //Test that only admin is authorized to elevate roles

        foreach ($users as $user){
            $this->actingAs($user); //Authenticate as each of the roles

            $response = $this->patch('role_elevate',array_merge($this->validPromoteRoleData($standard_user),['user_id' => 0]));

            //Assertions
            if($user == $admin_user) $response->assertSessionHasErrors('user_id');//Admin is authorized but data is invalid
            else $response->assertForbidden();    //Only admin is authorized to elevate roles
        }

        //Set a new admin user that elevates each of the users without affecting it's own roles
        $acting_admin = User::factory()->create();
        $acting_admin->roles()->attach($admin_role);

        //Test user promotion from standard to standard and moderator

        $this->actingAs($acting_admin);

        foreach ($users as $user){

            //Get response with PATCH method using valid data
            $response = $this->patch('role_elevate',$this->validPromoteRoleData($user));

            //Assertions
            if($user == $standard_user){
                //Current iteration user had 'Standard' role
                $this->assertTrue(
                    $user->isStandard() && $user->isModerator() //Now user has both 'Standard' and 'Moderator' roles
                );
            }
            else {
                //Current iteration user didn't have 'Standard' role
                $this->assertTrue(
                    !($user->isModerator() && $user->isStandard()) //User doesn't have 'Standard' and 'Moderator' roles
                );
            }
        }

        //Test user demotion from standard and moderator to just standard

        $standard_user = User::factory()->create();
        $standard_user->roles()->attach($standard_role);
        $standard_and_moderator = User::factory()->create();
        $standard_and_moderator->roles()->attach($standard_role);
        $standard_and_moderator->roles()->attach($moderator_role);
        $moderator_user = User::factory()->create();
        $moderator_user->roles()->attach($moderator_role);

        $users = [
            $roleless_user,
            $standard_user,
            $standard_and_moderator,
            $moderator_user,
            $admin_user
        ];

        foreach ($users as $user){

            //Get response with PATCH method using valid data
            $response = $this->patch('role_elevate',$this->validDemoteRoleData($user));

            //Assertions
            if($user == $standard_and_moderator){
                //Current iteration user had 'Standard' and 'Moderation' role
                $this->assertTrue(
                    $user->isStandard() && !$user->isModerator() //Now user has 'Standard' but not 'Moderator' rolee
                );
            }
        }

        //Test that authenticated admin can't elevate their own role

        //Promote
        $response = $this->patch('role_elevate',$this->validPromoteRoleData($acting_admin));
        $this->assertEquals($acting_admin->roles()->get()->pluck('name'), collect($admin_role->name));

        //Demote
        $response = $this->patch('role_elevate',$this->validDemoteRoleData($acting_admin));
        $this->assertEquals($acting_admin->roles()->get()->pluck('name'), collect($admin_role->name));

        //Assert that other user roles haven't changed (standard user only has standard role and so on)
        $this->assertEquals($standard_user->roles()->get()->pluck('name'), collect($standard_role->name));
        $this->assertEquals($moderator_user->roles()->get()->pluck('name'), collect($moderator_role->name));
        $this->assertEquals($admin_user->roles()->get()->pluck('name'), collect($admin_role->name));

    }

    private function validPromoteRoleData($user){
        return [
            'user_id' => $user->id,
            'promote' => 1
        ];
    }

    private function validDemoteRoleData($user){
        return [
            'user_id' => $user->id,
            'demote' => 1
        ];
    }
}
