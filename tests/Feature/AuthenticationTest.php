<?php

namespace Tests\Feature;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    /**
    +-----------+-------------------------------------+------------------------------------------------------------------------+
    | Method    | URI                                 | Test                                                                   |
    +-----------+-------------------------------------+------------------------------------------------------------------------+
    | POST      | login                               | login_post_route_test                                                  |
    | GET|HEAD  | login                               | login_get_route_test                                                   |
    | POST      | logout                              | logout_post_route_test                                                 |
    | POST      | password/confirm                    | password_confirm_post_route_test                                       |
    | GET|HEAD  | password/confirm                    | password_confirm_get_route_test                                        |
    | POST      | password/email                      | password_reset_test                                                    |
    | POST      | password/reset                      | password_reset_test                                                    |
    | GET|HEAD  | password/reset                      | password_reset_test                                                    |
    | GET|HEAD  | password/reset/{token}              | password_reset_test                                                    |
    | GET|HEAD  | register                            | register_get_route_test                                                |
    | POST      | register                            | register_post_route_test                                               |
    +-----------+-------------------------------------+------------------------------------------------------------------------+
     */


    /**
     * Tests that the 'login' route returns a login form when user is not authenticated.
     * Tests that an authenticated user gets redirected when accessing 'login' get route.
     *
     * @test
     * @group unit_route
     * @group acting_as_guest
     * @return void
     */
    public function login_get_route_test()
    {
        //Getting login form while not logged in
        $response = $this->get('login');    //Send the GET request

        $response->assertOk();   //HTTP Status code 200, request was successful
        $response->assertViewIs('auth.login');  //Check if returned view is the expected result

        //While authenticated, try to get the login form
        $response = $this->actingAs(User::factory()->create())->get('/login');

        $response->assertRedirect(url(RouteServiceProvider::HOME)); //User gets redirected to home page
    }

    /**
     * Tests that post route 'login' returns error (error key 'email') if
     * the request data doesn't match User table entries. If the user data
     * is valid then controller redirects user to home page.
     *
     * @test
     * @group unit_route
     * @group acting_as_guest
     * @return void
     */
    public function login_post_route_test()
    {

        //Test if login route responds with an error if there are no users in database
        $response = $this->post('login',[
            'email'=> 'user@gmail.com',
            'password'=> 'password'
        ]);

        $response->assertSessionHasErrors('email'); //An error appears because there are no users with these credentials


        //Create a new user for authentication testing
        $user = User::factory()->create([
            'email'=> 'user@gmail.com',
            //The correct password will be 'password' (as User factory dictates)
        ]);


        //Test if login route responds with an error if the user password is incorrect
        $response = $this->post('/login',[
            'email'=> $user->email,
            'password'=> 'password0',
        ]);

        $response->assertSessionHasErrors('email'); //An error appears because the password was incorrect


        //Test that user is authenticated if request data is valid
        $response = $this->post('/login',[
            'email'=> $user->email,
            'password'=> 'password'
        ]);

        $response->assertSessionHasNoErrors(); //No errors appear in the session because the credentials were correct
        $response->assertRedirect(url(RouteServiceProvider::HOME)); //User gets redirected to home because LoginController specifies that
        $this->assertAuthenticated();   //Assert that user is actually authenticated

        //Create a new user for authentication testing
        $different_users = User::factory()->create([
            'email'=> 'different.user@gmail.com',
            //The correct password will be 'password' (as User factory dictates)
        ]);

        //Send authentication data while already logged in
        $response = $this->actingAs($different_users)->post('/login',[
            'email'=> $user->email,
            'password'=> 'password'
        ]);

        $response->assertRedirect(url(RouteServiceProvider::HOME)); //User gets redirected to home because LoginController specifies that
        $this->assertAuthenticatedAs($different_users); //Authenticated user doesn't change by providing another user's login data
    }

    /**
     * Tests how 'logout' post route responds when a guest and an authenticated user accesses the route
     *
     * @test
     * @group unit_route
     * @group acting_as_user
     * @return void
     */
    public function logout_post_route_test()
    {
        //Test that an unauthenticated user gets redirected to login form when attempting to logout
        $response = $this->from('browse')->post('logout'); //Log out while visiting a route available for guests

        $response->assertRedirect('/'); //AuthenticatesUsers trait logout method redirects user to '/'

        //Test that authenticated user is redirected to front page and is a guest user after posting to 'logout' route
        $this->actingAs(User::factory()->create());
        $this->assertAuthenticated();   //Assert that user was authenticated before logging out

        $response = $this->from('browse')->post('logout'); //Logout while visiting a route available for guests

        $response->assertRedirect('/'); //User gets redirected to front page and not back to origin location ('/browse')
        $this->assertGuest();   //Assert that user is no longer authenticated
    }

    /**
     * Tests that the password confirm route is available only for authenticated users
     * and successfully returns a view 'auth.passwords.confirm'
     *
     * @test
     * @group unit_route
     * @group acting_as_guest
     * @return void
     */
    public function password_confirm_get_route_test()
    {
        //Getting password confirm form while not logged in
        $response = $this->get('password/confirm');    //Send the GET request

        $response->assertRedirect('login'); //Route uses authentication middleware

        //Authenticate with new user and request password confirm form
        $response = $this->actingAs(User::factory()->create())->get('password/confirm');

        $response->assertOk();   //HTTP Status code 200, request was successful
        $response->assertViewIs('auth.passwords.confirm');  //Check if returned view is the expected result

    }

    /**
     * Tests that posting to password confirm redirects to 'login' route if guest
     * attempts to access route.
     * If user is authenticated and user sends password that doesn't match
     * the user's password then response contains an error. Otherwise user is redirected.
     *
     * @test
     * @group unit_route
     * @group acting_as_guest
     * @return void
     */
    public function password_confirm_post_route_test()
    {
        //Test posting password confirm form while not logged in
        $response = $this->post('password/confirm',[
            'password' => 'password0'
        ]);    //Send the GET request

        $response->assertRedirect('login'); //Route uses authentication middleware


        //Create a new user for authentication testing
        $user = User::factory()->create([
            'email'=> 'user@gmail.com',
            //The correct password will be 'password' (as User factory dictates)
        ]);


        //Test password confirmation with invalid password
        $response = $this->actingAs($user)->post('password/confirm',[
            'password' => 'password0'
        ]);

        $response->assertSessionHasErrors('password'); //An error appears because the password not filled


        //Test password confirmation with valid password
        $response = $this->actingAs($user)->post('password/confirm',[
            'password' => 'password'
        ]);

        $response->assertRedirect('/');   //If intended location to redirect to is not specified by middleware, redirects to '/'
        $response->assertSessionHasNoErrors(); //No errors appear because the password was correct

    }

    /**
     * Tests that:
     *  - the password email form can be accessed via 'password/reset' GET route;
     *  - the sent password email form gets is validated;
     *  - user is notified with a message containing password reset token;
     *  - the password reset form can be accessed via 'password/reset/{token}' GET route;
     *  - the sent password reset form gets is validated and user's password is updated;
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function password_reset_test()
    {
        //Create a user that needs their password reset
        $user = User::factory()->create();

        //During test allow ResetPassword notification
        $this->expectsNotification($user, ResetPassword::class);



        //GET PASSWORD EMAIL FORM

        $response = $this->get('password/reset');   //Get route response

        $response->assertOk();   //HTTP Status code 200, request was successful
        $response->assertViewIs('auth.passwords.email');  //Email form received


        //SEND PASSWORD RESET LINK TO EMAIL

        //Set from location to assert that user is redirected back
        $from_location_url = 'password/reset';

        //Test that post route requires email information
        $response = $this->from($from_location_url)->post('password/email');
        //Session has an error because email was not specified
        $response->assertRedirect($from_location_url);
        $response->assertSessionHasErrors('email');


        //Get route response sending email information that doesn't match any records
        $response = $this->from($from_location_url)->post('password/email',['email'=>$user->email.'a']);
        //Session has an error because email was not found in 'users' table
        $response->assertRedirect($from_location_url);
        $response->assertSessionHasErrors('email');

        //Assert that there are no entries in 'password_resets' table before a valid post request
        $this->assertDatabaseCount(0, 'password_resets');


        //Post user email information that matches created user
        $response = $this->from($from_location_url)->post('password/email',['email'=>$user->email]);

        $response->assertRedirect($from_location_url);  //Assert that user gets redirected (user checks email for password reset link)
        $response->assertSessionHasNoErrors();  //Session has no errors because email was found in 'users' table
        $response->assertSessionHas('status','We have emailed your password reset link!');  //Session has a message


        //Assert that there is a new entry in 'password_resets' table after a valid post request
        $this->assertDatabaseCount(1, 'password_resets');



        //USE TOKEN TO GET PASSWORD RESET FORM

        //Get token from dispatched notification without actually sending a mail message
        $token = $this->dispatchedNotifications[0]['instance']->token;

        //Get password reset form
        $response = $this->get('password/reset/'.$token);
        $response->assertOk();   //Token in GET route parameter was valid
        $response->assertViewIs('auth.passwords.reset');    //Password reset form received


        //SEND PASSWORD RESET FORM

        //Send POST request without required data
        $response = $this->post('password/reset');

        //Assert that response redirects user back to the password reset form
        //with errors because the required fields weren't filled
        $response->assertRedirect('password/reset');
        $response->assertSessionHasErrors(['token','email','password']);

        //Send POST request with all fields filled except hidden token field
        $response = $this->post('password/reset',[
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        //Assert that response redirects user back to the password reset form
        //with errors because the hidden token field was missing
        $response->assertRedirect('password/reset');
        $response->assertSessionHasErrors(['token']);

        //Send POST request with all fields filled but user's email invalid
        $response = $this->post('password/reset',[
            'token' => $token,
            'email' => $user->email.'a',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        //Assert that response redirects user back to the password reset form
        //with errors because a user with specified email could not be found
        $response->assertRedirect('password/reset');
        $response->assertSessionHasErrors(['email']);

        //Set variables with old credentials before password is changed
        $old_email = $user->email;
        $old_password = $user->password; //password

        //Reset password with valid data and token field
        $response = $this->post('password/reset',[
            'token' => $token,
            'email' => $user->email,
            'password' => 'password1',  //New user password
            'password_confirmation' => 'password1',
        ]);

        //Assert that user is redirected without errors
        $response->assertRedirect('/home');
        $response->assertSessionHasNoErrors();

        $user = User::first();  //Retrieve the one and only user

        //Assert that found user has the same email as the user requesting password reset
        $this->assertEquals($user->email,$old_email);
        //Assert that the password of the user has changed after posting to 'password/reset' route
        $this->assertNotEquals($old_password, $user->password);

    }

    /**
     * Tests that the 'register' route returns a register form when user is not authenticated.
     * Tests that an authenticated user gets redirected when accessing 'register' get route.
     *
     * @test
     * @group unit_route
     * @group acting_as_guest
     * @return void
     */
    public function register_get_route_test()
    {
        //Getting register form while not logged in
        $response = $this->get('register');    //Send the GET request

        $this->assertGuest();

        $response->assertOk();   //HTTP Status code 200, request was successful
        $response->assertViewIs('auth.register');  //Check if response contains register form

        //Getting register form while logged in
        $response = $this->actingAs(User::factory()->create())->get('register');    //Send the GET request

        $this->assertAuthenticated();

        $response->assertRedirect('home');   //User is redirected because they are already logged in
    }

    /**
     * Tests that post route 'register' returns errors if the required fields are invalid.
     * If the user data is valid then a new user is created and controller redirects user to home page.
     * Tests that authenticated users get redirected to 'home' in their 'register' post route response
     *
     * @test
     * @group unit_route
     * @group acting_as_guest
     * @return void
     */
    public function register_post_route_test()
    {

        //Sending register form from 'register' without required fields
        $response = $this->from('register')->post('register');    //Send the POST request

        //Response redirects user back to fill out 'register' form correctly
        $response->assertRedirect('register');
        //Errors about required fields are missing
        $response->assertSessionHasErrors(['email','name','password']);


        //Assert that there are no users in 'users' table
        $this->assertDatabaseCount('users', 0);

        $this->artisan('db:seed --class=RoleSeeder');   //RegisterController attaches standard role to newly registered users

        //Sending register form from 'register' with required fields
        $response = $this->post('register',[
            'name' => 'Test User',
            'email' => 'test@test.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertSessionHasNoErrors();  //All registration form fields valid
        $response->assertRedirect(url(RouteServiceProvider::HOME)); //Redirects user to home page
        //Assert that after posting register form there is a user in 'users' table
        $this->assertDatabaseCount('users', 1);


        //Sending register form while already authenticated
        $user = User::factory()->create();
        $this->assertDatabaseCount('users', 2);

        $response = $this->from('/')->actingAs($user)->post('register');    //Send the POST request

        $response->assertSessionHasNoErrors();  //Middleware 'guest' redirects user before form field validation
        $response->assertRedirect('home'); //User gets redirected to their home page

        $this->assertDatabaseCount('users', 2);

    }

}
