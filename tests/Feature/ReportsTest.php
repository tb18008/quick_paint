<?php

namespace Tests\Feature;

use App\Models\Report;
use App\Models\ReportType;
use App\Models\Role;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReportsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();

        $this->artisan('db:seed --class=RoleSeeder');   //RegisterController attaches standard role to newly registered users
    }

    /**
    +-----------+-------------------------------------+-------------------------+
    | Method    | URI                                 | Test                    |
    +-----------+-------------------------------------+-------------------------+
    | POST      | reports                             | reports_store_test      |
    | GET|HEAD  | reports                             | reports_index_test      |
    | GET|HEAD  | reports/create                      | reports_create_test     |
    | GET|HEAD  | reports/{report_id}/restore         | reports_restore_test    |
    | GET|HEAD  | reports/{report}                    | reports_show_test       |
    | DELETE    | reports/{report}                    | reports_destroy_test    |
    +-----------+-------------------------------------+-------------------------+
     */


    /**
     * Tests that the 'reports' get route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - responds with the same index if there are no reports in database;
     *  - responds with a different depending on role of the user if there are reports in database;
     *  - responds with a different if user owns some amount of reports.
     *
     * @test
     * @group unit_route
     * @group resource_index
     * @group acting_as_user
     * @return void
     */
    public function reports_index_test()
    {

        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Getting report index as a guest
        $response = $this->get('reports');    //Send the GET request

        $this->assertGuest();

        $response->assertRedirect('login');   //User is redirected to login page before accessing route


        //Getting report index while authenticated
        $response = $this->actingAs(User::factory()->create())->get('reports');    //Send the GET request

        $this->assertAuthenticated();

        $response->assertOk();   //User is not redirected
        $response->assertViewIs('report.index');  //Check if returned view is the expected result



        //ROUTE RESPONSE DATA DEPENDING ON ROLE


        //If there are no reports in database, the response won't contain any reports
        $this->assertEmpty(Report::all());

        //Iterate through every user role
        $users = $this->get_user_of_each_role();
        foreach ($users as $user){

            $response = $this->actingAs($user)->get('reports');    //Send the GET request
            //Get response data with the key 'reports' and assert that it is empty
            $this->assertEmpty($this->getResponseData($response, 'reports'));

            //Request hidden reports like an admin would
            $response = $this->actingAs($user)->get('reports?hidden=true');
            $this->assertEmpty($this->getResponseData($response, 'reports'));   //Also has no report data
        }


        //If there are reports in database only moderators and admins will see them
        $this->artisan('db:seed --class=ReportTypeSeeder');   //Required for report factory (report_type_id)
        $this->artisan('db:seed --class=UserSeeder');   //Required for report factory (reporter_id)
        $this->artisan('db:seed --class=TagSeeder');   //Required for user image seeder (user image has many tags)
        $this->artisan('db:seed --class=UserImageSeeder');   //Required for report factory (user_image_id)

        Report::factory()->count(7)->create();  //Regular reports
        Report::factory()->count(3)->create(['deleted_at'=>time()]);    //Soft deleted reports

        $this->assertCount(7,Report::all());
        $this->assertCount(3,Report::onlyTrashed()->get());

        //Create new users after seeding database so that the users have no report relations
        $users = $this->get_user_of_each_role();

        //Iterate through every user role
        foreach ($users as $user){

            //Request non-deleted reports index with the 'reports' URL
            $response = $this->actingAs($user)->get('reports');    //Send the GET request

            if($user == $users['roleless'] || $user->isStandard()){ //Roleless or standard

                //No reports because this user doesn't own any of the reports and doesn't have the role privileges
                $this->assertEmpty($this->getResponseData($response, 'reports'));
            }
            elseif($user->isModerator() || $user->isAdmin()){   //Moderator or admin

                //Response data contains only the reports that are not soft deleted
                $this->assertEquals(7,$this->getResponseDataTotalItemCount($response, 'reports'));
            }


            //Request hidden reports index by specifying so in query string
            $response = $this->actingAs($user)->get('reports?hidden=true');

            //If there are soft deleted reports in database only admins will see them
            if($user->isAdmin()){

                //Admin can see the 3 soft deleted reports
                $this->assertEquals(3,$this->getResponseDataTotalItemCount($response, 'reports'));
            }
            else{

                //Other users will see none or 7 reports
                $this->assertNotEquals(3,$this->getResponseDataTotalItemCount($response, 'reports'));
            }

        }



        //REPORT OWNERSHIP


        //Iterate through every user role who own some amount of reports
        foreach ($users as $user){

            //Add N reports where the user owns the report and N is user's id
            Report::factory()->count($user->id)->create(['reporter_id'=> $user->id]);

            $response = $this->actingAs($user)->get('reports');    //Send the GET request

            if($user == $users['roleless']){

                //No reports because this user doesn't have the role privilege of having a report
                $this->assertEmpty($this->getResponseData($response, 'reports'));
            }
            elseif($user->isStandard()){ //Standard

                //Report count = user's id
                $this->assertEquals($user->id,$this->getResponseDataTotalItemCount($response, 'reports'));
            }
            elseif($user->isModerator() || $user->isAdmin()){   //Moderator or admin

                //Shows all the reports that are not soft deleted (owned reports included for moderators)
                $this->assertEquals(Report::all()->count(),$this->getResponseDataTotalItemCount($response, 'reports'));
            }

        }
    }

    /**
     * Tests that the 'reports/create' get route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - is forbidden to users that don't have the standard or moderator role;
     *  - requires a valid user_image_id in query string;
     *  - returns a reports create form with necessary data if user is authorized
     * and provided user_image_id is valid.
     *
     * @test
     * @group unit_route
     * @group resource_create
     * @group acting_as_user
     * @return void
     */
    public function reports_create_test()
    {

        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to get report create form as a guest
        $response = $this->get('reports/create');    //Send the GET request

        $this->assertGuest();

        $response->assertRedirect('login');   //User is redirected to login page before accessing route


        //Getting report create response while authenticated
        $users = $this->get_user_of_each_role();

        foreach ($users as $user){

            $response = $this->actingAs($user)->get('reports/create');    //Send the GET request
            $this->assertAuthenticated();

            if($user->isStandard() || $user->isModerator()){
                //Assert that a standard or moderator user is allowed to create reports
                $this->assertTrue($response->status() != 403);  //Users get redirected or OK
            }
            else{
                //Assert that users with other roles are forbidden to create reports
                $response->assertForbidden();
            }
        }



        //REPORT CREATE FORM IF USER IS NOT FORBIDDEN

        //Set variables with user types that are allowed to use route
        $users = [
            'standard' => $users['standard'],
            'moderator' => $users['moderator'],
        ];

        //Send request from a specific location
        $from_location_url = '/';

        //User image to specify in request query string
        $request_user_image = UserImage::factory()->create();

        $this->artisan('db:seed --class=ReportTypeSeeder');   //Required for report create form

        foreach ($users as $user){

            //Requesting report create form attempt without specifying user image
            $response = $this->actingAs($user)->from($from_location_url)->get('reports/create');    //Send the GET request

            //User is returned back from where user sent the request
            $response->assertRedirect($from_location_url);
            //User also is presented with an error about the provided user image id
            $response->assertSessionHasErrors(['user_image_id']);


            //Another attempt at requesting report create form
            //including an invalid user image id in query string
            $response = $this
                ->actingAs($user)
                ->from($from_location_url)
                ->get('reports/create?user_image_id=0');    //Send the GET request

            //User is returned back from where user sent the request
            $response->assertRedirect($from_location_url);
            //User also is presented with an error about the provided user image id
            $response->assertSessionHasErrors(['user_image_id']);


            //Last attempt at requesting report create form
            //including a valid user image id in query string
            $response = $this
                ->actingAs($user)
                ->from($from_location_url)
                ->get('reports/create?user_image_id='.$request_user_image->id);    //Send the GET request

            $response->assertOk();  //GET request was valid
            $response->assertSessionHasNoErrors();
            $response->assertViewIs('report.create'); //User is presented a report create form

            //Response data contains all the report types for user to choose from
            $this->assertCount(ReportType::all()->count(), $this->getResponseData($response, 'report_types'));

            $response_user_image = $this->getResponseData($response, 'user_image');

            //Response data contains only one and only user image and it's id matches the one sent in request
            $this->assertEquals($request_user_image->id, $response_user_image->first()->id);
            $this->assertCount(1,$response_user_image);

        }

    }

    /**
     * Tests that the 'reports' post route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - is forbidden to users that don't have the standard or moderator role;
     *  - requires valid ids and report information fields;
     *  - validates that reporter is currently authenticated user.
     *
     * @test
     * @group unit_route
     * @group resource_store
     * @group acting_as_user
     * @return void
     */
    public function reports_store_test()
    {

        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to post a report as a guest
        $response = $this->post('reports');    //Send the request

        $this->assertGuest();

        $response->assertRedirect('login');   //User is redirected to login page before accessing route


        //Getting report store response while authenticated
        $users = $this->get_user_of_each_role();

        foreach ($users as $user){

            $response = $this->actingAs($user)->post('reports');    //Send the POST request
            $this->assertAuthenticated();

            if($user->isStandard() || $user->isModerator()){
                //Assert that a standard or moderator user is allowed to store reports
                $this->assertTrue($response->status() != 403);  //Users get redirected or OK
            }
            else{
                //Assert that users with other roles are forbidden to store reports
                $response->assertForbidden();
            }
        }



        //POST A REPORT IF USER IS NOT FORBIDDEN

        //Set variables with user types that are allowed to use route
        $users = [
            'standard' => $users['standard'],
            'moderator' => $users['moderator'],
        ];

        //Send request from a specific location
        //to assert that user is redirected back to the same location
        $from_location_url = '/reports/create';

        $this->artisan('db:seed --class=ReportTypeSeeder');   //Required input field ('report_type_id')

        foreach ($users as $user){

            //Attempt to store a post without providing any of the required fields
            $response = $this->actingAs($user)->from($from_location_url)->post('reports');    //Send the GET request

            //User is returned back from where user sent the request
            //with errors for all the missing fields
            $response->assertRedirect($from_location_url);
            $response->assertSessionHasErrors([
                'reporter_id',
                'report_type_id',
                'user_image_id',
                'subject',
                'report_text'
            ]);

            //Another attempt at storing report with valid data except,
            //form hidden reporter id contains some other user
            $response = $this
                ->actingAs($user)
                ->from($from_location_url)
                ->post('reports',[
                    'report_type_id' => ReportType::all()->random()->id,
                    'reporter_id' => User::factory()->create()->id,
                    'user_image_id' => UserImage::factory()->create()->id,
                    'subject' => 'Report with invalid reporter_id',
                    'report_text' => 'Report with invalid reporter_id',
                ]);    //Send the GET request

            //A user can't post a report claiming the reporter is another user
            $response->assertSessionHasErrors(['reporter_id']);
            $response->assertRedirect($from_location_url);

            //The table remains empty
            $this->assertDatabaseCount('reports',0);

            //Last attempt at storing a new report with valid data
            $response = $this
                ->actingAs($user)
                ->from($from_location_url)
                ->post('reports',[
                    'report_type_id' => ReportType::all()->random()->id,
                    'reporter_id' => $user->id, //Reporter has to be currently authenticated user
                    'user_image_id' => UserImage::factory()->create()->id,
                    'subject' => 'Report with valid reporter_id',
                    'report_text' => 'Report with valid reporter_id',

                    //Field that doesn't affect the result
                    //because model specifies which attributes are used in mass assignment
                    'unnecessary_field' => 'unnecessary value',
                ]);

            $response->assertSessionHasNoErrors(); //All fields were valid

            //The reports table now has an entry
            $this->assertDatabaseCount('reports', 1);

            //Request was valid and user is shown the one and only report
            $new_report = Report::first();
            $response->assertRedirect('reports/'.$new_report->id);

            $new_report->forceDelete(); //Delete newly created report to not affect further testing
        }

    }

    /**
     * Tests that the 'reports/{report}' get route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - is forbidden to users that don't have the moderator, admin role or don't own the report;
     *  - responds with 'report.show' view and requested report model;
     *  - returns a 'Not found' status code if requested report couldn't be found.
     *
     * @test
     * @group unit_route
     * @group resource_show
     * @group acting_as_user
     * @return void
     */
    public function reports_show_test()
    {

        //Seed everything since reports are seeded last
        $this->artisan('migrate:fresh --seed');


        //ROUTE USES AUTHENTICATION MIDDLEWARE

        $report = Report::factory()->create();

        //Trying to get report show view as a guest
        $response = $this->get('reports/'.$report->id);    //Send the GET request

        $this->assertGuest();

        $response->assertRedirect('login');   //User is redirected to login page before accessing route


        //ROUTE RESPONSE DATA DEPENDING ON ROLE AND OWNERSHIP


        $users = $this->get_user_of_each_role();

        //Create a user that owns created report
        $report_owner = User::factory()->create();
        $report->reporter_id = $report_owner->id;
        $report->save();

        $users['report_owner'] = $report_owner; //Add report owner to array of users

        foreach ($users as $user){

            $response = $this->actingAs($user)->get('reports/'.$report->id);    //Send the GET request
            $this->assertAuthenticated();

            if($user->isModerator() || $user->isAdmin() || $user == $report_owner){

                //Assert that a moderator, an admin or the owner of the report
                //are presented with a view that contains the requested report model
                $response->assertOk();
                $response->assertViewIs('report.show');
                $response->assertViewHas('report',$report);
            }
            else{

                //Assert that other users are forbidden to view selected report
                $response->assertForbidden();
            }
        }

        //Assert that passing an invalid report id responds with 'Not found' status code
        $response = $this->actingAs(User::factory()->create())->get('reports/0');
        $response->assertNotFound();

    }

    /**
     * Tests that the 'reports/{report}' delete route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - requires a 'forced' data field to pass validation;
     *  - will force delete a report if the user is the reporter or user's role is admin;
     *  - will soft delete a report if the user's role is moderator but the user is not the reporter.
     *
     * @test
     * @group unit_route
     * @group resource_destroy
     * @group acting_as_user
     * @return void
     */
    public function reports_destroy_test()
    {

        //Seed everything since reports are seeded last
        $this->artisan('migrate:fresh --seed');


        //ROUTE USES AUTHENTICATION MIDDLEWARE

        $report = Report::factory()->create();

        //Trying to delete a report as a guest
        $response = $this->delete('reports/'.$report->id);

        $this->assertGuest();

        $response->assertRedirect('login');   //User is redirected to login page before accessing route


        //AUTHENTICATED BUT REQUIRED FIELDS MISSING

        //Send request from a specific location to compare that redirected back if request isn't valid
        $from_location_url = '/';

        //Attempt to request a report's deletion without the required data fields
        $response = $this->actingAs(User::factory()->create())->from($from_location_url)->delete('reports/'.$report->id);
        $this->assertAuthenticated();

        //User is redirected back to their previous location with an error that the forced field is required
        $response->assertRedirect($from_location_url);
        $response->assertSessionHasErrors(['forced']);

        $this->assertNotNull(Report::find($report->id));    //Report is still in reports table


        //ROUTE RESPONSE DATA DEPENDING ON ROLE AND OWNERSHIP

        $users = $this->get_user_of_each_role();

        //Create a user that owns created report
        $report_owner = User::factory()->create();
        $report->reporter_id = $report_owner->id;
        $report->save();

        $users['report_owner'] = $report_owner; //Add report owner to array of users

        foreach ($users as $user){


            //FORCE DELETE

            //DELETE request, data contains key-value pair requesting a forced delete
            $response = $this->actingAs($user)->delete('reports/'.$report->id,['forced' => true]);

            if($user == $users['report_owner'] || $user->isAdmin()){    //Only admins and owners can force delete the report

                $response->assertRedirect('reports');   //Deletion successful, redirects to reports index

                $this->assertNull(Report::find($report->id));   //Report with that id is no longer there
                $this->assertNull(Report::onlyTrashed()->find($report->id));   //Not even among trashed reports

                //Create new report and set it's owner for further testing
                $report = Report::factory()->create();
                $report->reporter_id = $report_owner->id;
                $report->save();
            }
            else{

                //Other users can't force delete the report because of their role
                $response->assertForbidden();
            }

            //SOFT DELETE

            //DELETE request, data contains key-value pair requesting a non-forced delete (soft delete)
            $response = $this->actingAs($user)->delete('reports/'.$report->id,['forced' => false]);

            if($user->isModerator()){   //Moderator isn't forbidden to soft delete a report they don't own

                $response->assertRedirect('reports');   //Deletion successful, redirects to reports index

                //Attempt to find deleted report by id among trashed reports
                $soft_deleted_report = Report::onlyTrashed()->find($report->id);

                $this->assertNull(Report::find($report->id));   //Report isn't found among non-deleted reports
                $this->assertNotNull($soft_deleted_report);     //Report is found among trashed reports

                //Restore report from soft deletion for further testing
                $report = $soft_deleted_report;
                $report->restore();
            }
            else{

                //Other users can't soft delete the report because of their role
                $response->assertForbidden();
            }
        }


        //SOFT DELETE MODERATOR'S OWN REPORT

        //Set moderator from users
        $moderator_report_owner = $users['moderator'];

        //Set new report's reporter to the moderator and save
        $report->reporter_id = $moderator_report_owner->id;
        $report->save();

        //Moderator attempts to delete a report they own
        $response = $this->actingAs($moderator_report_owner)->delete('reports/'.$report->id,['forced' => false]);

        //A moderator isn't allowed to hide their own report from index
        $response->assertForbidden();
    }

    /**
     * Tests that the 'reports/{report}/restore' get route:
     *  - uses authentication middleware and only authenticated users can access it;
     *  - will restore a deleted report if the user's role is admin.
     *
     * @test
     * @group unit_route
     * @group resource_restore
     * @group acting_as_user
     * @return void
     */
    public function reports_restore_test()
    {

        //Seed everything since reports are seeded last
        $this->artisan('migrate:fresh --seed');


        //ROUTE USES AUTHENTICATION MIDDLEWARE

        $report = Report::factory()->create(['deleted_at' => time()]);  //Create a soft deleted report

        //Trying to restore a report as a guest user
        $response = $this->get('reports/'.$report->id.'/restore');    //Send the GET request

        $this->assertGuest();

        $response->assertRedirect('login');   //User is redirected to login page before accessing route


        //ROUTE RESPONSE DATA DEPENDING ON ROLE AND OWNERSHIP

        $users = $this->get_user_of_each_role();

        //Create a user that owns created report
        $report_owner = User::factory()->create();
        $report->reporter_id = $report_owner->id;
        $report->save();

        $users['report_owner'] = $report_owner; //Add report owner to array of users

        foreach ($users as $user){

            //Send request to restore the report
            $response = $this->actingAs($user)->get('reports/'.$report->id.'/restore');

            if($user->isAdmin()){    //Only admins can restore soft deleted reports

                $response->assertRedirect('reports');   //Restoration successful, redirects to reports index

                $this->assertNotNull(Report::find($report->id));   //Report with that id is found among non-deleted reports
                $this->assertNull(Report::onlyTrashed()->find($report->id));   //Report can not be found among trashed reports

                //Soft delete report for further testing (try catch block in case deletion fails)
                try {
                    $report->delete();
                } catch (\Exception $e) {
                }
            }
            else{

                //Other users can't restore the deleted the report because of their role
                $response->assertForbidden();
            }

        }

    }

    /**
     * Gets items by key from response data.
     *
     * @param $response
     * @param $key
     * @return mixed
     */
    private function getResponseData($response, $key){

        $content = $response->getOriginalContent();

        $content = $content->getData();

        return $content[$key]->all();

    }

    /**
     * Gets total item count from
     * Illuminate\Pagination\LengthAwarePaginator object.
     *
     * @param $response
     * @param $key
     * @return int
     */
    private function getResponseDataTotalItemCount($response, $key){

        $content = $response->getOriginalContent();

        $content = $content->getData();

        return $content[$key]->total();

    }

    /**
     * Creates users for:
     *  - standard
     *  - moderator
     *  - admin
     * roles for role dependant authorization tests.
     * Includes the creation of a user without a role.
     *
     * @return array
     */

    private function get_user_of_each_role(){
        $standard_role = Role::where('name','Standard')->first();
        $moderator_role = Role::where('name','Moderator')->first();
        $admin_role = Role::where('name','Admin')->first();

        $roleless_user = User::factory()->create();

        $standard_user = User::factory()->create();
        $standard_user->roles()->attach($standard_role);

        $moderator_user = User::factory()->create();
        $moderator_user->roles()->attach($moderator_role);

        $admin_user = User::factory()->create();
        $admin_user->roles()->attach($admin_role);

        //Array of users to iterate through
        return [
            'roleless' => $roleless_user,
            'standard' => $standard_user,
            'moderator' => $moderator_user,
            'admin' => $admin_user
        ];
    }

}
