<?php

namespace Tests\Feature;

use App\Models\Report;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\Services\ResponseDataService;
use Tests\Services\UserFactoryService;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;
    private $user_factory;

    public function setUp() : void
    {
        parent::setUp();

        //Models required for creating user images
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');
        $this->artisan('db:seed --class=ReportTypeSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
        $this->user_factory = new UserFactoryService();
    }

    /**
     * Tests that user show route is available for guest users
     * and returns a view with a user that matches the user specified in parameter
     *
     * @test
     * @group unit_route
     * @group resource_show
     * @return void
     */
    public function users_show_test()
    {

        //USER DOESN'T EXIST

        DB::table('users')->truncate();

        //Send request for user.show view for user when there are no users
        $response = $this->get('users/1');

        $this->assertGuest();

        //Route is accessible to guests but no users are found
        $response->assertNotFound();


        //A USER EXISTS

        //Create a new user
        $expected_user = $this->user_factory->get_user();

        $response = $this->get('users/'.$expected_user->id);    //Send request

        //Response status code is 200 and the view matches expected view
        $response->assertOk();
        $response->assertViewIs('user.show');

        //Get the user object in response and compare it's id with expected user id
        $user_in_response = collect($this->response_data_service->get_response_data($response,'user'));
        $this->assertEquals($expected_user->id,$user_in_response->first()->id);
        $this->assertCount(1,$user_in_response);    //There is only one user in the response
    }

    /**
     * Tests that user delete route:
     *  - requires an authenticated user;
     *  - deletes a user and it's relations if
     *  the authenticated user is admin
     *  or the user is the owner of the user specified in parameter
     *  or the user specified in parameter isn't an admin;
     *
     * @test
     * @group unit_route
     * @group resource_show
     * @return void
     */
    public function users_destroy_test()
    {

        //ROUTE USES AUTHENTICATION MIDDLEWARE

        //Trying to delete a user as a guest user
        $response = $this->delete('users/1');

        $this->assertGuest();

        //User is redirected to login page because route uses authentication middleware
        $response->assertRedirect('login');


        //USER DELETION REQUEST AUTHORIZATION

        //Create a user without roles that users will attempt to delete
        $user_to_delete = $this->get_user_with_relations();

        //Getting user deletion response while authenticated
        $users = $this->user_factory->get_users_with_roles();
        $users[] = $user_to_delete; //Add user to delete to array of users that will request the deletion

        foreach ($users as $user){

            //Send the request of a user's deletion
            $response = $this->actingAs($user)->delete('users/'.$user_to_delete->id);
            $this->assertAuthenticated();

            if($user->isAdmin() || $user == $user_to_delete){   //Only admins or user owner themselves can delete the user

                //Assert that there are no users with the user id in parameter (model Users doesn't implement soft deleting)
                $this->assertNull(
                    User::all()->find($user_to_delete->id)
                );

                //Assert that there are no related models either
                $this->assertEmpty(UserImage::where('user_id',$user_to_delete->id)->get());
                $this->assertEmpty(Report::where('reporter_id',$user_to_delete->id)->get());


                if($user->isAdmin())    //Admins get redirected to 'home' route by the controller
                    $response->assertRedirect('home');
                else {
                    //User owners get logged out and redirected to 'login' route
                    $this->assertGuest();
                    $response->assertRedirect('login');
                }

                //Create a new user in the deleted ones place to continue tests
                $user_to_delete = $this->get_user_with_relations();
            }
            else{

                //Assert that users with other roles are forbidden to delete the user
                $response->assertForbidden();
            }
        }


        //ADMIN DELETION REQUEST

        //Create users of each roles that other users will attempt to delete
        $users_to_delete = $this->user_factory->get_users_with_roles();

        //Getting users that will send the deletion requests
        $request_sending_users = $this->user_factory->get_users_with_roles();

        foreach ($request_sending_users as $request_sending_user){

            foreach ($users_to_delete as $user_role => $user_to_delete){
                //Send the request of a user's deletion
                $response = $this->actingAs($request_sending_user)->delete('users/'.$user_to_delete->id);
                $this->assertAuthenticated();
                if($request_sending_user->isAdmin()){   //Users with the role admin can't be deleted using this route

                    if($user_to_delete->isAdmin()){

                        //Admins are forbidden to delete other admins
                        $response->assertForbidden();
                    }
                    else{

                        //User to delete is not an admin and gets deleted

                        //Assert that there are no users with the user id in parameter (model Users doesn't implement soft deleting)
                        $this->assertNull(
                            User::all()->find($user_to_delete->id)
                        );

                        //Assert that there are no related models either
                        $this->assertEmpty(UserImage::where('user_id',$user_to_delete->id)->get());
                        $this->assertEmpty(Report::where('reporter_id',$user_to_delete->id)->get());

                        $response->assertRedirect('home');

                        //Create a new user in the deleted ones place to continue tests
                        $users_to_delete[$user_role] = $this->user_factory->get_user([ucfirst($user_role)]);
                    }
                }
                else {

                    //User isn't an admin and there are no users that are requesting their own deletion, only admins can delete
                    $response->assertForbidden();
                }

            }

        }

    }

    /**
     * Creates a user that has a user image and a report
     *
     * @return mixed
     */
    private function get_user_with_relations(){

        $new_user = $this->user_factory->get_user([]);

        //Create related models for the new user
        UserImage::factory()->create(['user_id'=>$new_user->id]);
        $this->assertNotEmpty(UserImage::where('user_id',$new_user->id)->get());   //There is at least one user image for the new user
        Report::factory()->create(['reporter_id'=>$new_user->id]);
        $this->assertNotEmpty(Report::where('reporter_id',$new_user->id)->get());   //There is at least one report for the new user

        return $new_user;
    }
}
