<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Services\ResponseDataService;
use Tests\Services\UserFactoryService;
use Tests\Services\UserImageFactoryService;
use Tests\TestCase;

class UserImagesSearchTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;
    private $user_factory;
    private $user_image_factory;

    public function setUp() : void
    {
        parent::setUp();

        //Models required for creating user images
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
        $this->user_factory = new UserFactoryService();
        $this->user_image_factory = new UserImageFactoryService();
    }


    /**
     * Tests that specifying a user image's name in search terms query:
     *  - returns a user image with matching name;
     *  - returns a user image with a similar name;
     *  - returns nothing otherwise.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function search_by_user_image_name_test(){

        //SEARCH BY USER IMAGE NAME

        //Set query string and expected user image models in response
        $user_images_for_query = [
            'search_terms=A' => [$this->user_image_factory->get_named_user_image('A')],   //Only one with matching name
            'search_terms=B' => [
                $this->user_image_factory->get_named_user_image('B'),    //One with matching name
                $this->user_image_factory->get_named_user_image('B C')    //One with similar name
            ],
            'search_terms=D' => [$this->user_image_factory->get_named_user_image('C D')],   //Only one with similar name
            'search_terms=E' => []   //None with matching or similar name
        ];

        //Send get request with queries and compare response with expected user images
        $this->assert_expected_user_images($user_images_for_query);
    }

    /**
     * Tests that specifying a user image's tag name in search terms query:
     *  - returns a user image that has a tag with matching name;
     *  - returns a user image that has a tag with a similar name;
     *  - returns nothing otherwise.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function search_by_user_image_tags_test(){

        //SEARCH BY USER IMAGE TAGS

        //Set query string and expected user image models in response
        $user_images_for_query = [
            'search_terms=A' => [$this->user_image_factory->get_user_image_with_tag('A')],   //Only one with matching tag name
            'search_terms=B' => [
                $this->user_image_factory->get_user_image_with_tag('B'),   //One with matching tag name
                $this->user_image_factory->get_user_image_with_tag('B C')  //One with similar tag name
            ],
            'search_terms=D' => [$this->user_image_factory->get_user_image_with_tag('C D')],   //Only one with similar tag name
            'search_terms=E' => []   //None with matching or similar tag name
        ];

        //Send get request with queries and compare response with expected user images
        $this->assert_expected_user_images($user_images_for_query);
    }

    /**
     * Tests that specifying a user image's user's name in search terms query:
     *  - returns a user image where the user image's user has a matching name;
     *  - returns a user image where the user image's user has a tag with a similar name;
     *  - returns nothing otherwise.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function search_by_user_image_user_test(){

        //SEARCH BY USER IMAGE USER'S NAME

        //Set query string and expected user image models in response
        $user_images_for_query = [
            'search_terms=A' => [$this->user_image_factory->get_user_image_for_named_user('A')],   //Only one with matching user's name
            'search_terms=B' => [
                $this->user_image_factory->get_user_image_for_named_user('B'),     //One with matching user's name
                $this->user_image_factory->get_user_image_for_named_user('B C')    //One with similar user's name
            ],
            'search_terms=D' => [$this->user_image_factory->get_user_image_for_named_user('C D')],   //Only one with similar user's name
            'search_terms=E' => []   //None with matching or similar name
        ];

        //Send get request with queries and compare response with expected user images
        $this->assert_expected_user_images($user_images_for_query);
    }

    /**
     * Tests how user image index changes when changing the search terms query:
     *  - returns the user images that can be found with all three search terms;
     *  - returns user images as if the search terms field was missing if no search terms are specified;
     *  - returns soft deleted user images if only trashed query is added (only for authorized users).
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function search_user_images_test(){

        //GET SEARCHABLE AND UNSEARCHABLE USER IMAGE

        //Search term and name of user image
        $name = time();
        //Create a user image that will be searchable at first but not after
        $searchable_user_image = $this->user_image_factory->get_named_user_image($name);

        $response = $this->get('user_images?search_terms='.$name);
        //The searchable user image can be found with the search term
        $this->assertContains($searchable_user_image->id, collect($this->response_data_service->get_response_data($response, 'user_images'))->pluck('id'));

        //Make the searchable user image unsearchable
        $unsearchable_user_image = $searchable_user_image;
        $unsearchable_user_image->unsearchable();

        $response = $this->get('user_images?search_terms='.$name);
        //There are no user images found with the same search term
        $this->assertEmpty($this->response_data_service->get_response_data($response, 'user_images'));


        //SEARCH BY ALL THREE INDEXABLE FIELDS (WITHOUT TRASHED)

        //Search terms for each of the model's searchable indexable fields
        $search_terms = [
            'user_image_name' => time()+1,
            'tag_name' => time()+2,
            'user_name' => time()+3,
        ];

        //Create array of user images that are to be expected in response (one by name, one by tag name, one by user's name)
        $searchable_user_images = [
            $this->user_image_factory->get_named_user_image($search_terms['user_image_name']),
            $this->user_image_factory->get_user_image_with_tag($search_terms['tag_name']),
            $this->user_image_factory->get_user_image_for_named_user($search_terms['user_name'])
        ];

        //Create array for both searchable and unsearchable user images to test when search_terms query is filled
        $all_user_images = $searchable_user_images;
        $all_user_images[] = $unsearchable_user_image;

        //Set query string and expected user image models in response
        $user_images_for_query = [
            'search_terms='.implode(' ',$search_terms) => $searchable_user_images, //Only the three user images with search term attributes
            'search_terms=' => $all_user_images,    //Request query string is not filled and acts like index without a query
            '' => $all_user_images,                 //The same as 'search_terms='
        ];

        //Send get request with queries and compare response with expected user images
        $this->assert_expected_user_images($user_images_for_query);


        //SEARCH BY ALL THREE INDEXABLE FIELDS (ONLY TRASHED)

        //Iterate through every role to assert which users are forbidden to request trashed user images
        $users = $this->user_factory->get_users_with_roles();

        foreach ($users as $user){

            $response = $this->actingAs($user)->get('user_images?search_terms='.implode(' ',$search_terms).'&only_trashed=true');

            if($user->isModerator() || $user->isAdmin()){

                $response->assertOk();  //Only moderators and admins get the status code 200

                //But there are not soft deleted user images
                $actual_user_images = $this->response_data_service->get_response_data($response, 'user_images');
                $this->assertCount(0, $actual_user_images);
            }
            else $response->assertForbidden();  //Other users are forbidden to request trashed images and get a different status code

        }

        //Set the array of users who are authorized to request trashed user images
        $authorized_users = [
            'moderator' => $users['moderator'],
            'admin' => $users['admin'],
        ];

        //Soft delete the three user images that would show up with the search terms filled
        foreach ($searchable_user_images as $user_image_by_search_terms) $user_image_by_search_terms->delete();

        $unsearchable_user_image->delete(); //Soft delete the unsearchable user image to test that both queries work

        foreach ($authorized_users as $authorized_user){

            //Attempt to get soft deleted user images with the search terms
            $response = $this
                ->actingAs($authorized_user)
                ->get('user_images?search_terms='.implode(' ',$search_terms).'&only_trashed=true');

            $response_user_images = $this->response_data_service->get_response_data($response, 'user_images');

            //Assert that response user image only contains the searchable user images
            foreach ($searchable_user_images as $user_image_by_search_terms) {
                $this->assertTrue(
                    collect($response_user_images)->pluck('id')
                        ->contains($user_image_by_search_terms->id)
                );
            }

            //There are no additional user images, only the searchable user images
            $this->assertSameSize($response_user_images, $searchable_user_images);

        }

        //Force delete the three user images that would show up with the search terms filled
        foreach ($searchable_user_images as $user_image_by_search_terms) $user_image_by_search_terms->forceDelete();

        foreach ($authorized_users as $authorized_user){

            //Attempt to get force deleted user images with the search terms
            $response = $this
                ->actingAs($authorized_user)
                ->get('user_images?search_terms='.implode(' ',$search_terms).'&only_trashed=true');

            $response_user_images = $this->response_data_service->get_response_data($response, 'user_images');

            //The unsearchable user image isn't in the response
            //because the search term query worked
            $this->assertEmpty($response_user_images);
        }

    }


    /**
     * Sends a 'user_images' get request for each query in array
     * where key-value pair's key is a query and value is an array of expected user images.
     * Compares the expected user images with actual user images in response
     * by user image id existence and sizes of arrays.
     *
     * @param array $user_images_for_query
     * @param bool $dump
     * @return void
     */
    private function assert_expected_user_images(array $user_images_for_query, bool $dump = false){

        // Iterate through the pairs (Query => Array of expected user images)
        foreach ($user_images_for_query as $query => $expected_user_images) {

            //Get response for given query
            $response = $this->get('user_images?'.$query);

            //Retrieve actual user images in response
            $actual_user_images = $this->response_data_service->get_response_data($response, 'user_images');

            if($dump) {
                dump([
                    'query' => $query,
                    'expected user images' => collect($expected_user_images)->pluck('id'),
                    'actual user images' => collect($actual_user_images)->pluck('id')
                ]);
            }

            $user_images[$query] = $actual_user_images;

            //Assert that actual user images have expected user images
            foreach ($expected_user_images as $expected_user_image)
                $this->assertTrue(
                    collect($actual_user_images)->pluck('id')
                        ->contains($expected_user_image->id)
                );
            //All expected user images are in actual user images

            //Expected user image count matches actual user image count
            $this->assertSameSize($expected_user_images, $actual_user_images);

            //There are no additional user images (search actual results match expected results)
        }

    }

}
