<?php

namespace Tests\Feature;

use App\Models\Report;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\Services\ResponseDataService;
use Tests\Services\UserFactoryService;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;
    private $user_factory;

    public function setUp() : void
    {
        parent::setUp();
        //Models required for creating user images
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
        $this->user_factory = new UserFactoryService();
    }

    /**
     * Tests that route 'home' redirects guest users to 'login'.
     *
     * @test
     * @group acting_as_guest
     * @return void
     */
    public function uses_auth_middleware_test()
    {

        //Attempt to get dashboard view as a guest
        $response = $this->get('home');

        $this->assertGuest();

        //User is redirected to login page because route uses authentication middleware
        $response->assertRedirect('login');
    }

    /**
     * If user isn't a standard, moderator or admin then user is redirected to '/'
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function redirect_users_without_roles_test()
    {

        //Create user without a role
        $user = $this->user_factory->get_user();

        //User doesn't have any of the three roles
        $this->assertFalse($user->isStandard() || $user->isModerator() || $user->isAdmin());

        //Attempt to get dashboard view authenticated as a user without a role
        //Send request from a specific location to assert that user is redirected to '/' not their previous location
        $response = $this->actingAs($user)->from('user_images')->get('home');

        $this->assertAuthenticated();

        //User is redirected to a route that is available for guest because
        //user doesn't have a role that the controller can provide a dashboard for
        $response->assertRedirect('/');
    }

    /**
     * Tests a user's dashboard response data for a user with the role 'Standard'
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function standard_user_dashboard_test()
    {

        //Create user with the role 'standard'
        $user = $this->user_factory->get_user(['Standard']);

        //Create a user image that is not supposed to be in the responses
        UserImage::factory()->create(['user_id'=> 2]);


        //USER HAS NO USER IMAGES THEY OWN AND NO FAVORITE USER IMAGES

        //Get dashboard for standard user if user has no favorites and no user images they own
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user image collections from response
        $my_user_images_in_response = $this->response_data_service->get_response_data($response,'my_user_images');
        $favorites_in_response = $this->response_data_service->get_response_data($response,'favorites');

        //The two collections in response have no user images
        $this->assertCount(0,$my_user_images_in_response);
        $this->assertCount(0,$favorites_in_response);


        //USER HAS USER IMAGES THEY OWN AND FAVORITE USER IMAGES

        //Create 2 user images where the owner is the created user
        $expected_my_user_images = UserImage::factory()->count(2)->create(['user_id' => $user->id]);
        //Create 3 user images and attach them to created user's favorites
        $expected_favorites = UserImage::factory()->count(3)->create([
            'user_id'=>2    //To prevent user from favoring their own user image and increasing my_user_images count
        ]);
        $user->favorites()->attach($expected_favorites->pluck('id'));

        //Get dashboard for standard user if user has favorites and user images they own
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user image collections from response
        $my_user_images_in_response = collect($this->response_data_service->get_response_data($response,'my_user_images'));
        $favorites_in_response = collect($this->response_data_service->get_response_data($response,'favorites'));

        //Assert that my_user_images in response contains all expected my_user_images
        foreach ($expected_my_user_images as $expected_my_user_image){
            $this->assertTrue(
                $my_user_images_in_response->pluck('id')
                    ->contains($expected_my_user_image->id)
            );
        }
        //There are no additional user images and the response collection matches expected collection of my_user_images
        $this->assertCount($expected_my_user_images->count(),$my_user_images_in_response);

        //Assert that favorites in response contains all expected favorites
        foreach ($expected_favorites as $expected_favorite){
            $this->assertTrue(
                $favorites_in_response->pluck('id')
                    ->contains($expected_favorite->id)
            );
        }
        //There are no additional user images and the response collection matches expected collection of favorites
        $this->assertCount($expected_favorites->count(),$favorites_in_response);
    }

    /**
     * Tests a user's dashboard response data for a user with the role 'Standard' and 'Moderator'
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function standard_moderator_user_dashboard_test()
    {
        //Models required for creating user images
        $this->artisan('db:seed --class=ReportTypeSeeder');

        //Create user with the role 'standard' and 'moderator'
        $user = $this->user_factory->get_user(['Standard', 'Moderator']);

        //Create a user image that is not supposed to be in the responses
        UserImage::factory()->create(['user_id'=> 2]);


        //NO USER IMAGES OR REPORTS IN RESPONSE DATA

        //Get dashboard for user with standard and moderator roles
        //if user has no favorites, user images they own and there are no reports
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user image collections and reports from response
        $my_user_images_in_response = $this->response_data_service->get_response_data($response,'my_user_images');
        $favorites_in_response = $this->response_data_service->get_response_data($response,'favorites');
        $reports_in_response = $this->response_data_service->get_response_data($response,'reports');

        //The collections in response have no user images or reports
        $this->assertCount(0,$my_user_images_in_response);
        $this->assertCount(0,$favorites_in_response);
        $this->assertCount(0,$reports_in_response);


        //RESPONSE DATA CONTAINS EXPECTED USER IMAGES AND REPORTS

        //Create 2 user images where the owner is the created user
        $expected_my_user_images = UserImage::factory()->count(2)->create(['user_id' => $user->id]);
        //Create 3 user images and attach them to created user's favorites
        $expected_favorites = UserImage::factory()->count(3)->create([
            'user_id'=>2    //To prevent user from favoring their own user image and increasing my_user_images count
        ]);
        $user->favorites()->attach($expected_favorites->pluck('id'));
        //Create 5 reports
        $expected_reports = Report::factory()->count(5)->create();

        //Get dashboard for user with standard and moderator roles if user has favorites, user images they own and there are reports
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user image collections from response
        $my_user_images_in_response = collect($this->response_data_service->get_response_data($response,'my_user_images'));
        $favorites_in_response = collect($this->response_data_service->get_response_data($response,'favorites'));
        $reports_in_response = collect($this->response_data_service->get_response_data($response,'reports'));

        //Assert that my_user_images in response contains all expected my_user_images
        foreach ($expected_my_user_images as $expected_my_user_image){
            $this->assertTrue(
                $my_user_images_in_response->pluck('id')
                    ->contains($expected_my_user_image->id)
            );
        }
        //There are no additional user images and the response collection matches expected collection of my_user_images
        $this->assertCount($expected_my_user_images->count(),$my_user_images_in_response);

        //Assert that favorites in response contains all expected favorites
        foreach ($expected_favorites as $expected_favorite){
            $this->assertTrue(
                $favorites_in_response->pluck('id')
                    ->contains($expected_favorite->id)
            );
        }
        //There are no additional user images and the response collection matches expected collection of favorites
        $this->assertCount($expected_favorites->count(),$favorites_in_response);

        //Assert that favorites in response contains all expected favorites
        foreach ($expected_reports as $expected_report){
            $this->assertTrue(
                $reports_in_response->pluck('id')
                    ->contains($expected_report->id)
            );
        }
        //There are no additional reports and the response collection matches expected collection of reports
        $this->assertCount($expected_reports->count(),$reports_in_response);
    }

    /**
     * Tests a user's dashboard response data for a user with the role 'Moderator'
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function moderator_user_dashboard_test()
    {

        //Models required for creating user images
        $this->artisan('db:seed --class=ReportTypeSeeder');

        //Create user with the role 'moderator'
        $user = $this->user_factory->get_user(['Moderator']);

        //Create a user image that is not supposed to be in the responses
        UserImage::factory()->create();


        //NO USER IMAGES OR REPORTS IN RESPONSE DATA

        //Get dashboard for moderator if user has no favorites and there are no reports
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user image collections and reports from response
        $favorites_in_response = $this->response_data_service->get_response_data($response,'favorites');
        $reports_in_response = $this->response_data_service->get_response_data($response,'reports');

        //The collections in response have no user images or reports
        $this->assertCount(0,$favorites_in_response);
        $this->assertCount(0,$reports_in_response);


        //RESPONSE DATA CONTAINS EXPECTED USER IMAGES AND REPORTS

        //Create 3 user images and attach them to created user's favorites
        $expected_favorites = UserImage::factory()->count(3)->create();
        $user->favorites()->attach($expected_favorites->pluck('id'));
        //Create 5 reports
        $expected_reports = Report::factory()->count(5)->create();

        //Get dashboard for moderator if user has favorites and there are reports
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user image collections from response
        $favorites_in_response = collect($this->response_data_service->get_response_data($response,'favorites'));
        $reports_in_response = collect($this->response_data_service->get_response_data($response,'reports'));

        //Assert that favorites in response contains all expected favorites
        foreach ($expected_favorites as $expected_favorite){
            $this->assertTrue(
                $favorites_in_response->pluck('id')
                    ->contains($expected_favorite->id)
            );
        }
        //There are no additional user images and the response collection matches expected collection of favorites
        $this->assertCount($expected_favorites->count(),$favorites_in_response);

        //Assert that favorites in response contains all expected favorites
        foreach ($expected_reports as $expected_report){
            $this->assertTrue(
                $reports_in_response->pluck('id')
                    ->contains($expected_report->id)
            );
        }
        //There are no additional reports and the response collection matches expected collection of reports
        $this->assertCount($expected_reports->count(),$reports_in_response);
    }

    /**
     * Tests a user's dashboard response data for a user with the role 'Admin'
     *
     * @test
     * @group acting_as_user
     * @return void
     */
    public function admin_user_dashboard_test()
    {

        //Models required for creating reports
        $this->artisan('db:seed --class=ReportTypeSeeder');

        //Delete all seeded users for expected user counts
        DB::table('users')->truncate();

        //Create user with the role 'admin'
        $user = $this->user_factory->get_user(['Admin']);

        //Create a user image that is not supposed to be in the responses
        $standard_user = $this->user_factory->get_user(['Standard']);
        $standard_user->user_images()->save(UserImage::factory()->create());

        //Create a report that is not supposed to be in the responses
        Report::factory()->create();


        //NO USERS, HIDDEN USER IMAGES OR HIDDEN REPORTS IN RESPONSE DATA

        //Get dashboard for admin if user there are no other users, hidden user images or hidden reports
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user image collections and reports from response
        $users_in_response = collect($this->response_data_service->get_response_data($response,'users'));
        $hidden_user_images_in_response = $this->response_data_service->get_response_data($response,'hidden_user_images');
        $hidden_reports_in_response = $this->response_data_service->get_response_data($response,'hidden_reports');

        //There are only two users in dashboard and it's the admin and the unexpected user image owner
        $this->assertCount(2,$users_in_response);
        $this->assertContains($user->id, $users_in_response->pluck('id'));
        $this->assertContains($standard_user->id, $users_in_response->pluck('id'));
        //The collections in response have no hidden user images or hidden reports
        $this->assertCount(0,$hidden_user_images_in_response);
        $this->assertCount(0,$hidden_reports_in_response);


        //RESPONSE DATA CONTAINS EXPECTED USERS, HIDDEN USER IMAGES AND HIDDEN REPORTS

        //Create 2 users
        $expected_users = User::factory()->count(2)->create();
        $expected_users[] = $user; //Add created admin to array of expected users
        $expected_users[] = $standard_user; //Add created standard user to array of expected users
        //Create 3 user images and soft delete them
        $expected_hidden_user_images = UserImage::factory()->count(3)->create(['deleted_at'=>time()]);
        //Create 5 reports and soft delete them
        $expected_hidden_reports = Report::factory()->count(5)->create(['deleted_at'=>time()]);

        //Get dashboard for admin if database has users, hidden user images and hidden reports
        $response = $this->actingAs($user)->get('home');

        //Request is valid and the right view is returned in response
        $response->assertOk();
        $response->assertViewIs('user.dashboard');

        //Get user, hidden user image and hidden report collections from response
        $users_in_response = collect($this->response_data_service->get_response_data($response,'users'));
        $hidden_user_images_in_response = collect($this->response_data_service->get_response_data($response,'hidden_user_images'));
        $hidden_reports_in_response = collect($this->response_data_service->get_response_data($response,'hidden_reports'));

        //Assert that users in response contains all expected users
        foreach ($expected_users as $expected_user){
            $this->assertTrue(
                $users_in_response->pluck('id')
                    ->contains($expected_user->id)
            );
        }
        //There are no additional users and the response collection matches expected collection of users
        $this->assertCount($expected_users->count(),$users_in_response);

        //Assert that hidden user images in response contains all expected hidden user images
        foreach ($expected_hidden_user_images as $expected_hidden_user_image){
            $this->assertTrue(
                $hidden_user_images_in_response->pluck('id')
                    ->contains($expected_hidden_user_image->id)
            );
        }
        //There are no additional hidden user images and the response collection matches expected collection of hidden user images
        $this->assertCount($expected_hidden_user_images->count(),$hidden_user_images_in_response);

        //Assert that hidden reports in response contains all expected hidden reports
        foreach ($expected_hidden_reports as $expected_hidden_report){
            $this->assertTrue(
                $hidden_reports_in_response->pluck('id')
                    ->contains($expected_hidden_report->id)
            );
        }
        //There are no additional hidden reports and the response collection matches expected collection of hidden reports
        $this->assertCount($expected_hidden_reports->count(),$hidden_reports_in_response);
    }
}
