<?php

namespace Tests\Services;

use App\Models\Role;
use App\Models\User;

class UserFactoryService
{

    /**
     * Creates users for:
     *  - standard
     *  - moderator
     *  - admin
     * roles for role dependant authorization tests.
     * Includes the creation of a user without a role.
     *
     * @return array
     */

    public function get_users_with_roles(){
        $standard_role = Role::where('name','Standard')->firstOrFail();
        $moderator_role = Role::where('name','Moderator')->firstOrFail();
        $admin_role = Role::where('name','Admin')->firstOrFail();

        $roleless_user = User::factory()->create();

        $standard_user = User::factory()->create();
        $standard_user->roles()->attach($standard_role);

        $moderator_user = User::factory()->create();
        $moderator_user->roles()->attach($moderator_role);

        $admin_user = User::factory()->create();
        $admin_user->roles()->attach($admin_role);

        //Array of users to iterate through
        return [
            'roleless' => $roleless_user,
            'standard' => $standard_user,
            'moderator' => $moderator_user,
            'admin' => $admin_user
        ];
    }

    /**
     * Creates a user with roles specified by role name.
     *
     * @param array $roles
     * @return mixed
     */
    public function get_user(array $roles = []){

        $user = User::factory()->create();
        $user->roles()->detach();

        //Add each of the roles in array
        foreach ($roles as $role){
            $user->roles()->attach(
                Role::where('name',$role)->first()
            );
        }

        return $user;
    }

}
