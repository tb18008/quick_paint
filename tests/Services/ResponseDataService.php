<?php

namespace Tests\Services;


class ResponseDataService
{

    /**
     * Gets items by key from response data.
     *
     * @param $response
     * @param $key
     * @return mixed
     */
    public function get_response_data($response, $key){

        $content = $response->getOriginalContent();

        $content = $content->getData();

        return $content[$key]->all();

    }

    /**
     * Gets total item count from
     * Illuminate\Pagination\LengthAwarePaginator object.
     *
     * @param $response
     * @param $key
     * @return int
     */
    public function get_total_item_count($response, $key){

        $content = $response->getOriginalContent();

        $content = $content->getData();

        return $content[$key]->total();

    }

}
