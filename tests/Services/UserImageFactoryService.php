<?php

namespace Tests\Services;

use App\Models\Tag;
use App\Models\User;
use App\Models\UserImage;

class UserImageFactoryService
{
    public function get_named_user_image(string $user_image_name){

        //Use user image factory to create a new user image with the specified name
        $user_image = UserImage::factory()->create(['name'=>$user_image_name]);

        $user_image->tags()->detach();  //Remove all factory made tags to avoid unexpected results in actual results

        return $user_image;
    }

    public function get_user_image_with_tag(string $tag_name){

        //Use user image factory to create a new user image
        $user_image = UserImage::factory()->create();

        $user_image->tags()->detach();  //Remove all factory made tags to avoid unexpected results in actual results
        $new_tag = Tag::factory()->create(['name'=>$tag_name]); //Use tag factory to create a new tag with the specified name
        $user_image->tags()->attach($new_tag);  //Attach newly created tag to created user image

        $user_image->searchable();  //User image has records of newly created user image's name, user but not tags

        return $user_image;
    }

    public function get_user_image_with_tags(array $tag_ids){

        //Use user image factory to create a new user image
        $user_image = UserImage::factory()->create();

        $user_image->tags()->detach();  //Remove all factory made tags to avoid unexpected results in actual results
        foreach ($tag_ids as $tag_id){
            $new_tag = Tag::factory()->create(['id'=>$tag_id]); //Use tag factory to create a new tag with the id
            $user_image->tags()->attach($new_tag);  //Attach newly created tag to created user image
        }

        return $user_image;
    }


    public function get_user_images_for_user($user_id, $count){

        $user = User::factory()->create(['id'=>$user_id]);

        //Use user image factory to create a new user image

        $user_images = [];

        for($i = 0; $i < $count; $i++){
            $user_image = UserImage::factory()->create(['user_id'=>$user->id]);
            $user_images[] = $user_image;
        }

        return $user_images;
    }


    public function get_user_image_for_named_user(string $user_name){

        $user = User::factory()->create(['name'=>$user_name]);

        //Use user image factory to create a new user image
        $user_image = UserImage::factory()->create(['user_id'=>$user->id]);

        $user_image->tags()->detach();  //Remove all factory made tags to avoid unexpected results in actual results

        return $user_image;
    }

}
