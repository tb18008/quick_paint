<?php

namespace Tests\Unit;

use App\Models\Tag;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Services\ResponseDataService;

class UserImagesBrowseTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;

    public function setUp() : void
    {
        parent::setUp();

        //Models required for creating user images
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
    }

    /**
     * Tests that the user image browse route doesn't require authentication
     * and returns all tags and users that have user images
     *
     * @test
     * @group unit_route
     * @return void
     */
    public function user_images_browse_test()
    {


        //GET EMPTY USER IMAGE BROWSE VIEW

        $response = $this->get('browse');    //Send request for user image browse view

        $this->assertGuest();   //Guest users can access this route, no authentication required

        //Request was valid and response view matches expected view
        $response->assertOk();
        $response->assertViewIs('user_image.browse');

        //Get tag and user collections in response
        $tags_in_response = $this->response_data_service->get_response_data($response,'tags');
        $users_in_response = $this->response_data_service->get_response_data($response,'users');

        //Assert that there are no tags or users because none have been created
        $this->assertEmpty($tags_in_response);
        $this->assertEmpty($users_in_response);


        //GET USER IMAGE BROWSE VIEW WITH TAGS AND USERS

        Tag::factory()->create();   //Tag that doesn't have user images and is not expected in response
        User::factory()->create();   //User that doesn't have user images and is not expected in response

        //Create expected users and user images that they own
        $expected_users = User::factory()->count(3)->create();
        foreach ($expected_users as $expected_user) UserImage::factory()->create(['user_id'=>$expected_user->id]);

        //Create expected tags and user images that they belong to
        $expected_tags = Tag::factory()->count(5)->create();
        foreach ($expected_tags as $expected_tag) {
            $user_image = UserImage::factory()->create(['user_id'=>0]); //Without user to not increase the expected users count
            $user_image->tags()->attach($expected_tag);
        }

        //Send request for user image browse view
        $response = $this->get('browse');

        //Get tag and user collections in response
        $tags_in_response = collect($this->response_data_service->get_response_data($response,'tags'));
        $users_in_response = collect($this->response_data_service->get_response_data($response,'users'));

        //Assert that all expected tags are in the response tags
        foreach($expected_tags as $expected_tag){
            $this->assertContains($expected_tag->id, $tags_in_response->pluck('id'));
        }
        //The size of expected tags and tags in response are the same, there are no unexpected tags
        $this->assertSameSize($expected_tags,$tags_in_response);

        //Assert that all expected users are in the response users
        foreach($expected_users as $expected_user){
            $this->assertContains($expected_user->id, $users_in_response->pluck('id'));
        }
        //The size of expected users and users in response are the same, there are no unexpected users
        $this->assertSameSize($expected_users,$users_in_response);

    }
}
