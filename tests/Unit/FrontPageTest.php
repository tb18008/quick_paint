<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Services\ResponseDataService;
use Tests\TestCase;

class FrontPageTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;

    public function setUp() : void
    {
        parent::setUp();

        //Models required for creating user images
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
    }

    /**
     * Tests that the front page route doesn't require authentication
     * and returns newest user images and favored user images in the right order
     *
     * @test
     * @group unit_route
     * @return void
     */
    public function front_page_test()
    {

        //GET EMPTY FRONT PAGE

        $response = $this->get('/');    //Send request for front page view

        $this->assertGuest();   //Guest users can access this route, no authentication required

        //Request was valid and response view matches expected view
        $response->assertOk();
        $response->assertViewIs('front_page');

        //Get user image collections in response
        $newest_user_images_in_response = $this->response_data_service->get_response_data($response,'newest_user_images');
        $favored_user_images_in_response = $this->response_data_service->get_response_data($response,'favored_user_images');

        //Assert that there are no user images in response because no there are no user images
        $this->assertEmpty($newest_user_images_in_response);
        $this->assertEmpty($favored_user_images_in_response);


        //GET FRONT PAGE WITH USER IMAGES IN THE RIGHT ORDER

        $time = time();

        //Newest user image, without favorites
        $user_image = UserImage::factory()->create(['created_at'=>$time + 5]);
        $user_images[] = $user_image;

        //Second user image (created at before first but updated after), one favorite
        $user_image = UserImage::factory()->create(['created_at'=>$time, 'updated_at'=>$time + 2]);
        $user_image->favorites()->attach(User::factory()->create());
        $user_images[] = $user_image;

        //First user image, most favorites
        $user_image = UserImage::factory()->create(['created_at'=>$time + 1]);
        $user_image->favorites()->attach(User::factory()->count(3)->create());
        $user_images[] = $user_image;

        $expected_user_images = [
            'newest_user_images' => collect([
                $user_images[0],    //Newest user image created
                $user_images[1],    //Second user image (created before the first but updated after)
                $user_images[2],    //First user image created
            ]),
            'favored_user_images' => collect([
                $user_images[2],    //Most favorites
                $user_images[1],    //One favorite
                $user_images[0],    //Without favorites
            ]),
        ];

        //Send request for front page view with newest user images and favored user images
        $response = $this->get('/');

        //Get user image collections in response
        $newest_user_images_in_response = collect($this->response_data_service->get_response_data($response,'newest_user_images'));
        $favored_user_images_in_response = collect($this->response_data_service->get_response_data($response,'favored_user_images'));

        //Assert that the user image collections in response are the expected sizes
        $this->assertSameSize($expected_user_images['newest_user_images'],$newest_user_images_in_response);
        $this->assertSameSize($expected_user_images['favored_user_images'],$favored_user_images_in_response);

        //Assert that the user image ids in the response are in the expected order
        for($i = 0; $i<$expected_user_images['newest_user_images']->count(); $i++){
            $this->assertEquals($expected_user_images['newest_user_images'][$i]->id, $newest_user_images_in_response[$i]->id);
        }

        for($i = 0; $i<$expected_user_images['favored_user_images']->count(); $i++){
            $this->assertEquals($expected_user_images['favored_user_images'][$i]->id, $favored_user_images_in_response[$i]->id);
        }
    }
}
