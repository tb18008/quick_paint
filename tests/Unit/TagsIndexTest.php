<?php

namespace Tests\Unit;

use App\Models\Tag;
use App\Models\UserImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Services\ResponseDataService;
use Tests\TestCase;

class TagsIndexTest extends TestCase
{
    use RefreshDatabase;

    private $response_data_service;

    public function setUp() : void
    {
        parent::setUp();
        //Models required for creating user images which are needed for creating tags
        $this->artisan('db:seed --class=RoleSeeder');
        $this->artisan('db:seed --class=UserSeeder');

        //Services for methods that are used in other test cases
        $this->response_data_service = new ResponseDataService();
    }

    /**
     * Tests that 'tags' returns a view with tags that belong to at least one user image
     *
     * @test
     * @group resource_index
     * @group acting_as_guest
     * @return void
     */
    public function tags_index_test()
    {

        //GET TAG INDEX WITHOUT TAGS
        $response = $this->get('tags'); //Send request

        $this->assertGuest();   //Guests are allowed to access route

        $response->assertOk();  //Request is valid
        $response->assertViewIs('tag.browse');  //View matches the expected view

        //View doesn't contain any tags
        $this->assertEmpty($this->response_data_service->get_response_data($response, 'tags'));


        //GET TAG INDEX WITH TAGS THAT DON'T HAVE USER IMAGES

        //Create tags
        $expected_tags = Tag::factory()->count(5)->create();

        $response = $this->get('tags');

        //View doesn't contain any tags because none of them belong to a user image
        $this->assertEmpty($this->response_data_service->get_response_data($response, 'tags'));


        //GET TAG INDEX WITH TAGS THAT HAVE USER IMAGES

        $user_image = UserImage::factory()->create();   //Create a user image
        $user_image->tags()->detach();  //Remove tags created by user images factory
        $user_image->tags()->attach($expected_tags->pluck('id'));   //Attach the expected tags to user image

        //Send request and get collection of tags in response
        $response = $this->get('tags');
        $tags_in_response = collect($this->response_data_service->get_response_data($response, 'tags'));

        //Assert that tags in response contains all expected tags
        foreach ($expected_tags as $expected_tag){
            $this->assertTrue(
                $tags_in_response->pluck('id')
                    ->contains($expected_tag->id)
            );
        }
        //There are no additional tags and the response collection matches expected collection of tags
        $this->assertCount($expected_tags->count(),$tags_in_response);

    }
}
