<?php

namespace Database\Factories;

use App\Models\Report;
use App\Models\ReportType;
use App\Models\Role;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

class ReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Report::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'report_type_id' => ReportType::all()->random(),
            'reporter_id' => User::whereHas('roles', function($query) {
                $query->where('name', 'Standard')->orWhere('name', 'Moderator');
            })->get()->random(),   //Random user who is either a standard user or a moderator
            'user_image_id' => UserImage::all()->random(),
            'subject' => $this->faker->sentence,
            'report_text' => $this->faker->text,
        ];
    }
}
