<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\UserImage;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::whereHas('roles', function($query) {
                $query->where('name', 'Standard');
            })->get()->random(),   //Select a random existing standard user
            //All factory made user images use the same blank image
            'url' => 'image0000000000.png',
            'thumbnail_url' => 'image0000000000.png',
            'name' => $this->faker->word,
        ];
    }
}
