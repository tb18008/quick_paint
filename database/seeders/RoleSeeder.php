<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Populate roles table
        DB::table('roles')->insert([
            'name' => 'Standard',
        ]);
        DB::table('roles')->insert([
            'name' => 'Moderator',
        ]);
        DB::table('roles')->insert([
            'name' => 'Admin',
        ]);
    }
}
