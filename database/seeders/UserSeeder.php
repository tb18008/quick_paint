<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Get ids of created roles
        $standard_user_id = Role::where('name', 'Standard')->first()->id;
        $moderator_user_id = Role::where('name', 'Moderator')->first()->id;
        $admin_user_id = Role::where('name', 'Admin')->first()->id;

        //Make 3 users for each role and attach the role
        $standard_user = new User([
            'name' => 'Standard User',
            'email' => 'user@gmail.com',
            'password' => Hash::make('password'),
        ]);
        $standard_user->save();
        $standard_user->roles()->attach($standard_user_id);

        $moderator_user = new User([
            'name' => 'Moderator User',
            'email' => 'moderator@gmail.com',
            'password' => Hash::make('password'),
        ]);
        $moderator_user->save();
        $moderator_user->roles()->attach($moderator_user_id);

        $admin_user = new User([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
        ]);
        $admin_user->save();
        $admin_user->roles()->attach($admin_user_id);

        //Make random users with user factory and attach random roles to them
        User::factory()->count(6)->create()->each(function ($user) {
            $user->roles()->attach(Role::all()->random());
        });
    }
}
