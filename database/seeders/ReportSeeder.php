<?php

namespace Database\Seeders;

use App\Models\Report;
use Illuminate\Database\Seeder;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Report::factory()->count(22)->create(); //1 more than the maximum reports shown in one page of index
        Report::factory()->count(11)->create(['deleted_at' => time()]); //1 more than the maximum reports shown in one page of split
    }
}
