<?php

namespace Database\Seeders;

use App\Models\Tag;
use App\Models\User;
use App\Models\UserImage;
use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;
use Intervention\Image\ImageManager;

class UserImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filesystem = new Filesystem;
        $filesystem->cleanDirectory(storage_path('app/public/user_images/'));
        $filesystem->cleanDirectory(storage_path('app/public/thumbnails/'));

        $image_manager = new ImageManager();
        $img = $image_manager->canvas(128, 128, '#fff');
        $img->save(storage_path('app/public/user_images/image0000000000.png'));
        $img->resize(100,100)->save(storage_path('app/public/thumbnails/image0000000000.png'));

        UserImage::factory()->count(52)->create()  //Create 52 user images with unique standard users
            ->each(function ($user_image) {
            $user_image->tags()->attach(Tag::all()->random(5)); //Attach 5 random tags to the newly created user image
            $user_image->favorites()->attach(User::all()->random(5)); //Newly created user image is a favorite to 5 random users
        });

        UserImage::factory()->count(11)->create(['deleted_at' => time()])
        ->each(function ($user_image) {
            $user_image->tags()->attach(Tag::all()->random(5)); //Attach 5 random tags to the newly created user image
            $user_image->favorites()->attach(User::all()->random(5)); //Newly created user image is a favorite to 5 random users
        });
    }
}
