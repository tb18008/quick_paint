<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReportTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('report_types')->insert([
            'name' => 'Violation of Terms of Service',
        ]);
        DB::table('report_types')->insert([
            'name' => 'Bug report',
        ]);
    }
}
