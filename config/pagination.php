<?php

return [
    'split_items_per_page' => 9,
    'index_items_per_page' => 20,
    'cloud_items_per_page' => 50,
    'browse_items_per_page' => 10
];
