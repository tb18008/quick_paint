@extends('layouts.width-100')

@push('scripts')
    <script src="{{ asset('js/weight_cloud_color.js') }}"></script>
@endpush

@section('content')

    <div class="card">
        <div class="card-header">
            <b>
                Browse tags
            </b>
        </div>

        <div class="card-body">

            <div class="d-flex flex-wrap align-content-center justify-content-center weight-cloud">
                @foreach($tags->sortBy('name') as $tag)
                    <a class="badge-pill m-1 py-0 text-left search-term user-select-none text-center"
                         id="{{strtolower(class_basename($tag))}}-{{$tag->id}}"
                         data-weight="{{$tag->weight}}"
                        href="{{url('user_images?tags[]='.$tag->id)}}">
                        <span class="badge badge-light">{{$tag->weight}}</span>
                        <span class="text-break">{{$tag->name}}</span>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            <div class="col-12 d-flex justify-content-center">
                {{$tags->links('pagination::bootstrap-4')}}
            </div>
        </div>
    </div>

@endsection
