<!-- Store User Image Modal -->
<div class="modal fade" id="store_user_image_modal" tabindex="-1" role="dialog" aria-labelledby="store_user_image_modal_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="store_user_image_modal_label">Save image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ url('user_images/') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group row text-right">
                        <label for="name" class="col col-form-label">Image title</label>
                        <div class="col">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="col"></div>
                        <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                        <input type="hidden" id="image_data" name="image_data" value="">
                    </div>
                    <div class="form-group row text-right">
                        <label for="tag_input" class="col col-form-label">Image tags</label>
                        <div class="col">
                            <textarea class="form-control" id="tag_input" placeholder="Image tags (separated by commas)" rows="3"></textarea>
                            <input type="hidden" id="tag_string" name="tag_string" value="">
                        </div>
                        <div class="col"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Store image</button>
                </div>
            </form>
        </div>
    </div>
</div>
