<!-- New User Image Modal -->
<div class="modal fade" id="new_user_image_modal" tabindex="-1" role="dialog" aria-labelledby="new_user_image_modal_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="new_canvas_modal_label">Select canvas size</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row text-right">
                        <label for="canvas_width_number" class="col col-form-label">Width</label>
                        <div class="col">
                            <input type="number" class="form-control" id="canvas_width_number" name="canvas_width_number" min="16" max="512" value="128">
                        </div>
                        <div class="col">
                            <input class="custom-range w-100" type="range" id="canvas_width_range" name="canvas_width_range" min="16" max="512" value="128" step="1">
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <label for="canvas_height_number" class="col col-form-label">Height</label>
                        <div class="col">
                            <input type="number" class="form-control" id="canvas_height_number" name="canvas_height_number" min="16" max="512" value="128">
                        </div>
                        <div class="col">
                            <input class="custom-range w-100" type="range" id="canvas_height_range" name="canvas_height_range" min="16" max="512" value="128" step="1">
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <label for="canvas_background_rgba" class="col col-form-label">Background color</label>
                        <div class="col">
                            <input type="text" id="canvas_background_rgba" class="form-control minicolors-input" data-format="rgb" data-opacity="1.0" value="#ffffff">
                        </div>
                        <div class="col"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="new_user_image_submit" data-dismiss="modal" class="btn btn-primary">Create canvas</button>
            </div>
        </div>
    </div>
</div>
