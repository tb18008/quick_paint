<div class="col full-height">
    <div class="row full-height">
        <div class="col-3 noselect border border-right-0 border-dark bg-secondary rounded-left p-2">
            <form class="form-horizontal">
                <div class="form-group row">
                    <label class="col-5 col-form-label text-md-right" for="color_picker">Color</label>
                    <div class="col-7">
                        <input type="text" id="color_picker" class="form-control minicolors-input" data-format="rgb" data-opacity="1.0" value="#000000">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-5 col-form-label text-md-right" for="brush_size_range">Brush size</label>
                    <div class="col-7">
                        <input class="form-control" type="number" id="brush_size_number" name="brush_size_number" min="1" max="100" value="1">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <input class="custom-range w-100" type="range" id="brush_size_range" name="brush_size_range" min="1" max="100" value="1" step="1">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="form-check">
                            <input class="form-check-input custom-checkbox" type="checkbox" value="" id="pan_input">
                            <label class="form-check-label" for="pan_input">
                                Pan
                            </label>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row buttons d-flex justify-content-center pt-2">
                <button id="clear" class="btn btn-light" type="button">Clear</button>
                <span class="mr-3"></span>
                <button id="save" class="btn btn-light" type="button">Save</button>
            </div>
        </div>
        <div id="drawing_area" class="col-8 border border-dark rounded-right d-flex justify-content-center align-items-center">
            <canvas id="paint_canvas" height="128" width="128"></canvas>
            <canvas id="top_layer" height="128" width="128"></canvas>
            <div class="row" id="scale_overlay">
                <span class="badge badge-light" id="scale_label">100 %</span>
            </div>
            <div id="brush_cursor" hidden></div>
        </div>
        <div class="col-1 noselect border border-left-0 border-dark bg-secondary rounded-right p-2">
            <div class="row buttons d-flex justify-content-center pt-2">
                <button class="badge badge-light ml-1" id="zoom_reset">Reset</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <!-- Widget scripts -->
    <script src="{{ asset('js/painting_widget.js') }}"></script>
    <!-- MiniColors -->
    <script src="{{ asset('js/jquery.minicolors.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#canvas_background_rgba').minicolors({
                control:'hue',
                theme: 'bootstrap',
                format: 'hex',
                opacity:true,
            });
            $('#color_picker').minicolors({
                control:'hue',
                theme: 'bootstrap',
                format: 'hex',
                opacity:true,
            });

            $('#brush_cursor').hidden = false;  //Show hidden brush cursor when client can point it
        });
    </script>
@endpush
@push('styles')
    <!-- MiniColors -->
    <link rel="stylesheet" href="{{ asset('css/jquery.minicolors.css') }}">
    <!-- Painting Widget -->
    <link type="text/css" href="{{ asset('css/painting_widget.css') }}" rel="stylesheet" >
    <!-- Full height style -->
    <link type="text/css" href="{{ asset('css/full_height.css') }}" rel="stylesheet" >
@endpush
