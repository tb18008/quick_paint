@extends('layouts.width-100')

@push('styles')
    <!-- Full height style to fill page with more drawing area-->
    <link type="text/css" href="{{ asset('css/full_height.css') }}" rel="stylesheet" >
@endpush

@section('content')

    <div class="card full-height">

        <div class="card-header">
            <b>
                Create new image
            </b>
        </div>

        <div class="card-body full-height">
            @include('painting_widget.painting_widget')
            @include('painting_widget.new_user_image_modal')
            @include('painting_widget.store_user_image_modal')
            @push('scripts')
                <script>

                    // Window loaded

                    $(document).ready(function(){   //Preventing the user from dismissing the modal
                        $("#new_user_image_modal").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    });

                    window.onload = function () {
                        canvas.hidden = true;  //Hide canvas with default settings
                        $('#new_user_image_modal').modal('show');
                    };

                    let canvas_width_number = $(document).find("#canvas_width_number");
                    let canvas_height_number = $(document).find("#canvas_height_number");
                    let canvas_width_range = $(document).find("#canvas_width_range");
                    let canvas_height_range = $(document).find("#canvas_height_range");

                    //Canvas dimension input and change events

                    let max_size = 512;
                    let min_size = 16;
                    let default_size = 128;

                    canvas_width_number.on('change', function() {
                        if(between(this.value , min_size, max_size)){
                            canvas_width_range.val(this.value);
                        }
                        else{
                            canvas_width_range.val(default_size);
                            canvas_width_number.val(default_size);
                        }
                    });
                    canvas_height_number.on('change', function() {
                        if(between(this.value , min_size, max_size)){
                            canvas_height_range.val(this.value);
                        }
                        else{
                            canvas_height_range.val(default_size);
                            canvas_height_number.val(default_size);
                        }
                    });
                    canvas_width_range.on('input', function() {canvas_width_number.val(this.value);});
                    canvas_height_range.on('input', function() {canvas_height_number.val(this.value);});

                    // Clicking Create canvas button

                    $(document).find( "#new_user_image_submit" ).on('click', function(){    //Set up canvas with defined width and height and background color

                            let canvas_width = $(document).find("#canvas_width_number").val();
                            let canvas_height = $(document).find("#canvas_height_number").val();

                            canvas_width = between(canvas_width , min_size, max_size) ? canvas_width : default_size;
                            canvas_height = between(canvas_height , min_size, max_size) ? canvas_height : default_size;

                            canvas.setAttribute("width", canvas_width);
                            canvas.setAttribute("height", canvas_height);

                            canvas_context.fillStyle = $(document).find("#canvas_background_rgba").minicolors('rgbaString');
                            canvas_context.fillRect(0, 0, canvas_width,  canvas_height);

                            top_layer.width = canvas.width;
                            top_layer.height = canvas.height;
                            top_layer_context = top_layer.getContext("2d");

                            zoomToFit();    //Scale canvas to fill the drawing area
                            canvas.hidden = false;  //Show canvas with user's settings
                            recalculateBounds(); //After hiding canvas, it's bounds had been changed

                        }
                    );

                    function between(value, min, max) {
                        return value >= min && value <= max;
                    }

                    // Clicking Clear button

                    $(document).find("#clear").click(function() {

                        canvas_context.fillStyle = $(document).find("#canvas_background_rgba").minicolors('rgbaString');
                        canvas_context.globalAlpha = 1;
                        canvas_context.fillRect(0, 0, canvas_width,  canvas_height);
                        canvas_context.globalAlpha = $(document).find( "#color_picker" ).minicolors('opacity');

                    });

                    // Clicking Save button

                    $(document).find("#save").click(function() {

                        $("#image_data").val(canvas.toDataURL());
                        $('#store_user_image_modal').modal('show');

                    });
                </script>
            @endpush
        </div>

    </div>

@endsection
