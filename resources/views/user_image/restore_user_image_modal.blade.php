<!-- Restore User Image Modal -->
<div class="modal fade" id="{{$modal_id}}" tabindex="-1" role="dialog" aria-labelledby="{{$modal_id}}_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="{{$modal_id}}_label">
                        Are you sure you want to restore @if(isset($user_image->name)) {{$user_image->name}}? @else user image? @endif</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img src="{{url(Storage::url('thumbnails/'.$user_image->thumbnail_url))}}" alt="{{url(Storage::url('thumbnails/'.$user_image->thumbnail_url))}}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <a href="{{ url('user_images/'.$user_image->id. '/restore')}}" class="btn btn-warning">Restore</a>
                </div>
        </div>
    </div>
</div>
