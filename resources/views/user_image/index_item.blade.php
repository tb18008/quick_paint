<li class="list-group-item">

    <div class="d-flex justify-content-between">
        <div class="d-flex justify-content-start align-items-center">
            <a href="{{url('user_images/'.$user_image->id)}}">
                <img src="{{url(Storage::url('thumbnails/'.$user_image->thumbnail_url))}}" alt="{{url(Storage::url('thumbnails/'.$user_image->thumbnail_url))}}">
            </a>
            <div class="col">
                <div class="row">
                    &nbsp;
                </div>
                <div class="row">
                    <div class="col">
                        <a href="{{url('user_images/'.$user_image->id)}}">{{$user_image->name}}</a>
                        <span> by </span>
                        <a href="{{url('users/'.$user_image->user->id)}}">{{$user_image->user->name}}</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <span class="text-secondary">{{$user_image->updated_at}}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-end align-items-center">

            <div class="badge badge-pill badge-background">
                <div class="row align-items-center">
                    <div class="col justify-content-end">
                        <span class="text-white">{{$user_image->favorites()->count()}}</span>
                    </div>
                    <div class="col pl-0">
                        @auth
                            {{--Favorite toggle--}}
                            @if(Auth::user()->isStandard() || Auth::user()->isModerator())
                                <div class="mr-2">
                                    <a href="#" class="favorite-toggle" data-toggle-type="attach" data-user-image="{{$user_image->id}}" id="favorite_attach_{{$user_image->id}}" {{ Auth::user()->favorites->contains($user_image) ? "hidden" : "" }}>
                                        <img src="{{url("/img/star-inactive.svg")}}" alt="Add to favorites">
                                    </a>
                                    <a href="#" class="favorite-toggle" data-toggle-type="detach" data-user-image="{{$user_image->id}}" id="favorite_detach_{{$user_image->id}}" {{ Auth::user()->favorites->contains($user_image) ? "" : "hidden" }}>
                                        <img src="{{url("/img/star-active.svg")}}" alt="Remove from favorites">
                                    </a>
                                </div>
                            @else
                                <img class="disabled" src="{{url("/img/star-inactive.svg")}}" alt="Favorited by">
                            @endif
                        @else
                            <img class="disabled" src="{{url("/img/star-inactive.svg")}}" alt="Favorited by">
                        @endauth
                    </div>
                </div>
            </div>

            {{--User image dropdown--}}
            @auth
                <div class="dropdown ml-2">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_menu_{{$user_image->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_menu_{{$user_image->id}}">

                        {{--Edit button--}}
                        @if($user_image->user->id == Auth::user()->id)  {{--Only the owner can edit--}}
                        <a class="dropdown-item text-secondary" href="{{url('/user_images/'.$user_image->id.'/edit')}}" role="button">Edit</a>
                        @endif

                        {{--Delete button--}}
                        @if($user_image->user->id == Auth::user()->id || Auth::user()->isAdmin())   {{--Owner or admin force deletes--}}

                        <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#forced_delete_modal_{{$user_image->id}}">
                            Delete
                        </button>

                        {{--Safe delete buttons and modal--}}
                        @elseif(Auth::user()->isModerator() && !(Auth::user()->id == $user_image->user->id))

                            @if(!$user_image->trashed())
                                <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#safe_delete_modal_{{$user_image->id}}">
                                    Hide from index
                                </button>
                            @else
                                <button type="submit" class="dropdown-item text-secondary" data-toggle="modal" data-target="#restore_modal_{{$user_image->id}}">
                                    Restore
                                </button>
                            @endif

                        @endif

                        {{--Report button--}}
                        @if(Auth::user()->isStandard() || Auth::user()->isModerator())
                            <a class="dropdown-item" href="{{url('/reports/create?user_image_id='.$user_image->id)}}" role="button">Report</a>
                        @endif

                    </div>
                </div>

                {{--Modals--}}

                @if($user_image->user->id == Auth::user()->id || Auth::user()->isAdmin())   {{--Owner or admin force deletes--}}

                @include('user_image.delete_user_image_modal', [
                    'modal_id' => 'forced_delete_modal_'.$user_image->id,
                    'user_image' => $user_image,
                    'forced' => 1,
                    'button_text' => 'Delete'
                ])

                @elseif(Auth::user()->isModerator() && !(Auth::user()->id == $user_image->user->id))
                    @if(!$user_image->trashed())
                        @include('user_image.delete_user_image_modal', [
                            'modal_id' => 'safe_delete_modal_'.$user_image->id,
                            'user_image' => $user_image,
                            'forced' => 0,
                            'button_text' => 'Hide from index'
                        ])
                    @else
                        @include('user_image.restore_user_image_modal', [
                            'modal_id' => 'restore_modal_'.$user_image->id,
                            'user_image' => $user_image,
                        ])
                    @endif
                @endif

            @endauth

        </div>
    </div>

    @if($user_image->tags->count())
        <div class="row">
            <div class="card mt-2 w-100 bg-background">
                <div class="card-body d-flex overflow-auto p-0">
                    @foreach($user_image->tags->sortBy('name') as $tag)
                        <a href="{{url('user_images?tags[]='.$tag->id)}}" class="badge badge-pill badge-secondary ml-1">{{$tag->name}}</a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
</li>
