@extends('layouts.split')

@push('scripts')
    <script src="{{ asset('js/user_image_browse.js') }}"></script>
    <script src="{{ asset('js/weight_cloud_color.js') }}"></script>
@endpush

@section('sidebar')
    <div class="card full-height">

        <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">

                {{--Header--}}
                <div class="flex-grow-1">
                    <span>{{ __('Browse images') }}</span>
                    <br>
                    <small class="text-muted">Select search terms</small>
                </div>

                {{--Tag search term sort toggle (shown by default)--}}
                <div class="btn-group btn-group-toggle sort_toggle" id="tag_sort_toggle" data-toggle="buttons">
                    <label class="btn btn-secondary p-1 active">
                        <input class="sort-option" data-target=".sort-tags" type="radio" name="options" autocomplete="off" checked>
                        <img src="{{url("/img/sort-name.svg")}}" alt="Sort by name">
                    </label>
                    <label class="btn btn-secondary p-1">
                        <input class="sort-option" data-target=".sort-tags" type="radio" name="options" autocomplete="off">
                        <img src="{{url("/img/sort-weight.svg")}}" alt="Sort tags by popularity">
                    </label>
                </div>

                {{--User search term sort toggle--}}
                <div class="btn-group btn-group-toggle sort_toggle d-none" id="user_sort_toggle" data-toggle="buttons">
                    <label class="btn btn-secondary p-1 active">
                        <input class="sort-option" data-target=".sort-users" type="radio" name="options" autocomplete="off" checked>
                        <img src="{{url("/img/sort-name.svg")}}" alt="Sort by name">
                    </label>
                    <label class="btn btn-secondary p-1">
                        <input class="sort-option" data-target=".sort-users" type="radio" name="options" autocomplete="off">
                        <img src="{{url("/img/sort-weight.svg")}}" alt="Sort tags by popularity">
                    </label>
                </div>
            </div>
        </div>

        <div class="card-body overflow-auto p-1">

            {{--Tabbed interface to navigate between search terms (tags, users) and adding a name--}}
            <div class="d-flex justify-content-center">

                {{--Nav links for switching the active navigation pane--}}
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a href="#" class="nav-link active" data-target-pane=".tags-pane" data-target-sort-toggle="#tag_sort_toggle">Tags</a>
                        <a href="#" class="nav-link" data-target-pane=".users-pane" data-target-sort-toggle="#user_sort_toggle">Users</a>
                        <a href="#" class="nav-link" data-target-pane=".name-pane">Name</a>
                    </div>
                </nav>
            </div>

            {{--Tab panes--}}
            <div class="col tab-joined bg-background-025 tab-content p-1" id="nav-content-panes">

                {{--Tag weight cloud pane--}}
                <div class="tab-pane tags-pane show active">

                    {{--Tag weight cloud sorted by name--}}
                    <div class="sort-tags sort-tags-name">
                        @include('partials.weight_cloud', ['search_terms'=>$tags, 'sort_type'=>'name'])
                    </div>

                    {{--Tag weight cloud sorted by weight--}}
                    <div class="sort-tags sort-tags-weight d-none">
                        @include('partials.weight_cloud', ['search_terms'=>$tags, 'sort_type'=>'weight'])
                    </div>
                </div>

                {{--User weight cloud pane--}}
                <div class="tab-pane users-pane">

                    {{--User weight cloud sorted by name--}}
                    <div class="sort-users sort-users-name">
                        @include('partials.weight_cloud', ['search_terms'=>$users,'sort_type'=>'name'])
                    </div>

                    {{--User weight cloud sorted by weight--}}
                    <div class="sort-users sort-users-weight d-none">
                        @include('partials.weight_cloud', ['search_terms'=>$users,'sort_type'=>'weight'])
                    </div>
                </div>

                {{--Name input pane--}}
                <div class="tab-pane name-pane" id="nav-name-content">

                    {{--Input and add button--}}
                    <div class="form-group">
                        <input type="text" class="form-control" id="name-input" aria-describedby="name-input-help">
                        <small id="name-input-help" class="form-text text-muted">Full user image name (not case sensitive)</small>
                    </div>
                    <div class="form-row justify-content-center">
                        <button class="btn btn-primary align-self-center" id="add-name-button">Add search term</button>
                    </div>

                    {{--Hidden search term element to clone--}}
                    <div class="badge-pill m-1 py-0 text-left search-term user-select-none text-center btn-primary-100 d-none"
                         id="name-search-term"
                         data-target=""
                         data-name=""
                         data-type="name"
                         data-listed="false">
                        <span class="text-break"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('content')

    {{--Search term bar and search button and search results--}}
    <div class="card">
        <div class="card-header">

            {{--Search term bar and search button--}}
            <div class="d-flex justify-content-center">
                <label for="search_term_bar" class="flex-fill text-nowrap align-self-center">Search terms</label>

                {{--Search term bar with rows for each type of search term--}}
                <div class="search-term-bar bg-background-025 container-fluid mx-3 text-light">

                    {{--Row for tag search terms list--}}
                    <div class="row px-2 d-none">
                        <div class="d-flex flex-wrap" id="tag-list">
                            <span class="align-self-center">Tags: </span>

                        </div>
                    </div>

                    {{--Row for user search terms--}}
                    <div class="row px-2 d-none">
                        <div class="d-flex flex-wrap" id="user-list">
                            <span class="align-self-center">Users: </span>

                        </div>
                    </div>

                    {{--Row for name search terms--}}
                    <div class="row px-2 d-none">
                        <div class="d-flex flex-wrap" id="name-list">
                            <span class="align-self-center">Names: </span>

                        </div>
                    </div>
                </div>

                {{--Submit search button with loading spinner animation while ajax responds--}}
                <button type="submit" class="btn btn-primary text-nowrap" id="search_button">
                    <span class="spinner-border spinner-border-sm loading-spinner d-none"></span>
                    <span class="loading-spinner d-none">Loading ...</span>
                    <span id="search_button_text">Search</span>
                </button>
            </div>
        </div>

        {{--Div for successful ajax response HTML--}}
        <div id="search_results">
        </div>
    </div>
@endsection
