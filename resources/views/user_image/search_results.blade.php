@if($user_images->count())  {{--At least one user image--}}
    <div class="card-body">
        <ul class="list-group mb-2">
            @each('user_image.index_item', $user_images, 'user_image')
        </ul>
    </div>

    @if($user_images->lastPage() > 1)
        <div class="card-footer">
            <ul class="d-flex justify-content-center">
                {{$user_images->links('pagination::bootstrap-4')}}
            </ul>
        </div>
    @endif

    @auth()
        @if(Auth::user()->isStandard() || Auth::user()->isModerator())

            <!-- Favorite attach detach script -->
            <script src="{{ asset('js/favorite_attach_detach.js') }}"></script>
        @endif
    @endauth

    <!-- Asynchronous pagination script -->
    <script src="{{ asset('js/pagination_ajax.js') }}"></script>
@else
    <div class="card-body d-flex justify-content-center">
        <p>No user images found</p>
    </div>
@endisset
