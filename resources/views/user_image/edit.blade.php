@extends('layouts.width-100')

@push('styles')
    <!-- Full height style to fill page with more drawing area-->
    <link type="text/css" href="{{ asset('css/full_height.css') }}" rel="stylesheet" >
@endpush

@section('content')

    <div class="card full-height">

        <div class="card-header">
            <b>
                Edit @if($user_image->name) {{$user_image->name}} @else image @endif
            </b>
        </div>

        <div class="card-body full-height">
            @include('painting_widget.painting_widget')
            @include('painting_widget.update_user_image_modal')
            @push('scripts')
                <script>

                    //Draw user image on canvas

                    let user_image = new Image();
                    user_image.src = "{{url(Storage::url('user_images/'.$user_image->url))}}";

                    user_image.onload = function() {    //Set up canvas with defined width and height and user image to edit

                        canvas_width = this.width;
                        canvas_height = this.height;

                        canvas.setAttribute("width",canvas_width);
                        canvas.setAttribute("height",canvas_height);

                        top_layer.width = canvas.width;
                        top_layer.height = canvas.height;
                        top_layer_context = top_layer.getContext("2d");

                        canvas_context.drawImage(user_image,0,0);

                        zoomToFit();

                    }

                    // Clicking Clear button

                    $(document).find("#clear").click(function() {

                        canvas_context.globalAlpha = 1;
                        canvas_context.drawImage(user_image,0,0);
                        canvas_context.globalAlpha = $(document).find( "#color_picker" ).minicolors('opacity');

                    });

                    // Clicking Save button

                    $(document).find("#save").click(function() {

                        $("#image_data").val(canvas.toDataURL());
                        $('#update_user_image_modal').modal('show');

                    });
                </script>
            @endpush
        </div>

    </div>

@endsection
