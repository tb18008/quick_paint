@extends('layouts.width-100')

@auth()
    @if(Auth::user()->isStandard() || Auth::user()->isModerator())
        @push('scripts')
            <!-- Favorite attach detach script -->
            <script src="{{ asset('js/favorite_attach_detach.js') }}"></script>
        @endpush
    @endif
@endauth

@section('content')

    <div class="card">

        <div class="card-header">
            <b>
                User image index
            </b>
        </div>

        <div class="card-body">
            <ul class="list-group mb-2">
                @each('user_image.index_item', $user_images, 'user_image')
            </ul>
        </div>
        <div class="card-footer">
            <div class="col-12 d-flex justify-content-center">
                {{$user_images->links('pagination::bootstrap-4')}}
            </div>
        </div>

    </div>

@endsection
