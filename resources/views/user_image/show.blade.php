@extends('layouts.width-50')

@section('content')

    <div class="card text-center">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <div class="col"></div>
                <div class="col d-flex justify-content-center align-items-center">
                    <b>
                        {{$user_image->name}}
                    </b>
                </div>

                <div class="col d-flex justify-content-end align-items-center">

                    @auth()
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_menu_{{$user_image->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_menu_{{$user_image->id}}">

                                {{--Edit button--}}
                                @if($user_image->user->id == Auth::user()->id)  {{--Only the owner can edit--}}
                                <a class="dropdown-item text-secondary" href="{{url('/user_images/'.$user_image->id.'/edit')}}" role="button">Edit</a>
                                @endif

                                {{--Delete button--}}
                                @if($user_image->user->id == Auth::user()->id || Auth::user()->isAdmin())   {{--Owner or admin force deletes--}}

                                <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#forced_delete_modal_{{$user_image->id}}">
                                    Delete
                                </button>

                                {{--Safe delete buttons and modal--}}
                                @elseif(Auth::user()->isModerator() && !(Auth::user()->id == $user_image->user->id))

                                    @if(!$user_image->trashed())
                                        <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#safe_delete_modal_{{$user_image->id}}">
                                            Hide from index
                                        </button>
                                    @else
                                        <button type="submit" class="dropdown-item text-secondary" data-toggle="modal" data-target="#restore_modal_{{$user_image->id}}">
                                            Restore
                                        </button>
                                    @endif

                                @endif

                                {{--Report button--}}
                                @if(Auth::user()->onlyStandard())
                                    <a class="dropdown-item" href="{{url('/reports/create?user_image_id='.$user_image->id)}}" role="button">Report</a>
                                @endif

                            </div>
                        </div>

                        {{--Modals--}}

                        @if($user_image->user->id == Auth::user()->id || Auth::user()->isAdmin())   {{--Owner or admin force deletes--}}

                        @include('user_image.delete_user_image_modal', [
                            'modal_id' => 'forced_delete_modal_'.$user_image->id,
                            'user_image' => $user_image,
                            'forced' => 1,
                            'button_text' => 'Delete'
                        ])

                        @elseif(Auth::user()->isModerator() && !(Auth::user()->id == $user_image->user->id))
                            @if(!$user_image->trashed())
                                @include('user_image.delete_user_image_modal', [
                                    'modal_id' => 'safe_delete_modal_'.$user_image->id,
                                    'user_image' => $user_image,
                                    'forced' => 0,
                                    'button_text' => 'Hide from index'
                                ])
                            @else
                                @include('user_image.restore_user_image_modal', [
                                    'modal_id' => 'restore_modal_'.$user_image->id,
                                    'user_image' => $user_image,
                                ])
                            @endif
                        @endif
                    @endauth

                </div>
            </div>
        </div>
        <div class="card-body">
            <img class="img-fluid" src="{{url(Storage::url('user_images/'.$user_image->url))}}" alt="{{url(Storage::url('user_images/'.$user_image->url))}}">
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <b>
                        <span>Author:</span>
                        <a href="{{url('users/'.$user_image->user->id)}}">{{$user_image->user->name}}</a>
                    </b>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <span>Tags:</span>
                    @foreach($user_image->tags->sortBy('name') as $tag)
                        <a href="{{url('user_images?tags[]='.$tag->id)}}" class="badge badge-primary">{{$tag->name}}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
