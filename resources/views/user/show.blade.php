@extends('layouts.width-50')

@section('content')

    <div class="card">
        <div class="card-header">
            <div class="row align-middle">
                <div class="col">
                    <div class="row">
                        <div class="col d-flex justify-content-start align-items-center">
                                <span class="mr-3">
                                    <b>
                                        {{$user->name}}
                                    </b>
                                </span>
                            @foreach($user->roles as $role)
                                <span class="badge badge-primary mr-1">{{$role->name}}</span>
                            @endforeach
                        </div>

                        @auth()
                            @if(!$user->isAdmin() && (Auth::user()->isAdmin() || $user->id == Auth::user()->id))
                                {{--Admins can't be affected by role elevation or delete
                                the user needs to be an admin or theirselves--}}

                                <div class="col d-flex justify-content-end align-items-center">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_menu">

                                            {{--Delete button--}}

                                            <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#delete_user_modal">
                                                Delete user
                                            </button>

                                            {{--User role elevate--}}
                                            @if(Auth::user()->isAdmin())    {{--Only for admin to see--}}
                                            @if($user->onlyStandard())
                                                <form method="POST" action="{{ url('add_mod/'.$user->id) }}">
                                                    @csrf
                                                    @method('patch')
                                                    <input type="hidden" name="promote" value="1">
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                    <button type="submit" class="dropdown-item btn-warning">Promote to moderator</button>
                                                </form>
                                            @elseif($user->isModerator())
                                                <form method="POST" action="{{ url('remove_mod/'.$user->id) }}">
                                                    @csrf
                                                    @method('patch')
                                                    <input type="hidden" name="demote" value="1">
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                    <button type="submit" class="dropdown-item btn-warning">Demote to standard</button>
                                                </form>
                                            @endif
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                {{--Modal--}}

                                @include('user.delete_user_modal', ['user' => $user])

                            @endif
                        @endauth


                    </div>
                </div>
            </div>
        </div>
        @if($user->isStandard())
            <div class="card-body">
                <a href="{{url('user_images/?user='.$user->id)}}">
                    {{$user->name}} images
                </a>
            </div>
        @endif
    </div>

@endsection
