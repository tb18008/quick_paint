<li class="list-group-item">
    <div class="d-flex justify-content-between">
        <div class="d-flex justify-content-start align-items-center">

            <a href="{{url('users/'.$user->id)}}">
                {{$user->name}}
            </a>

            <div class="col">
                @foreach($user->roles as $role)
                    <span class="badge badge-primary mr-1">{{$role->name}}</span>
                @endforeach
            </div>
        </div>

        @if(!$user->isAdmin())
            <div class="d-flex justify-content-end align-items-center">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_menu">

                        {{--Delete button--}}
                        <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#delete_user_modal">
                            Delete user
                        </button>

                        {{--User role elevate--}}
                        @if($user->onlyStandard())
                            <form method="POST" action="{{ url('add_mod/'.$user->id) }}">
                                @csrf
                                @method('patch')
                                <input type="hidden" name="promote" value="1">
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <button type="submit" class="dropdown-item btn-warning">Promote to moderator</button>
                            </form>
                        @elseif($user->isModerator())
                            <form method="POST" action="{{ url('remove_mod/'.$user->id) }}">
                                @csrf
                                @method('patch')
                                <input type="hidden" name="demote" value="1">
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <button type="submit" class="dropdown-item btn-warning">Demote to standard</button>
                            </form>
                        @endif

                    </div>
                </div>
            </div>

            {{--Modal--}}

            @include('user.delete_user_modal', ['user' => $user])
        @endif

    </div>
</li>
