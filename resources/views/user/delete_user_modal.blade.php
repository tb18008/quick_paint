<!-- Delete User Modal -->
<div class="modal fade" id="delete_user_modal" tabindex="-1" role="dialog" aria-labelledby="delete_user_modal_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ url('users/'.$user->id) }}">
                @method('DELETE')
                @csrf

                <div class="modal-header">
                    <h5 class="modal-title" id="delete_user_modal_label">
                        Are you sure you want to delete {{$user->name}}?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    @foreach($user->roles as $role)
                        <span class="badge badge-primary mr-1">{{$role->name}}</span>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete user</button>
                </div>
            </form>
        </div>
    </div>
</div>
