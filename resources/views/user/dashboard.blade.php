@extends('layouts.split')

@push('navbar-item')
    @include('partials.search_bar')
@endpush

@section('sidebar')
    <div class="card full-height">
        <div class="card-header">{{ __('Dashboard') }}</div>

        <div class="card-body">

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            {{--Authenticated user specific nav-links--}}

            @auth()
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @if(Auth::user()->isStandard())
                            <a class="nav-link @if($active === 'my_user_images') active @endif" id="v-pills-my-user-images-tab" data-toggle="pill" href="#v-pills-my-user-images" role="tab" aria-controls="v-pills-my-user-images" aria-selected="@if($active === 'my_user_images') true @else false @endif">My images</a>
                        @endif
                        @if(Auth::user()->isStandard() || Auth::user()->isModerator())
                            <a class="nav-link @if($active === 'favorites') active @endif" id="v-pills-favorites-tab" data-toggle="pill" href="#v-pills-favorites" role="tab" aria-controls="v-pills-favorites" aria-selected="@if($active === 'favorites') true @else false @endif">Favorites</a>
                        @endif
                        @if(Auth::user()->isAdmin())
                            <a class="nav-link @if($active === 'hidden_user_images') active @endif" id="v-pills-hidden-user-images-tab" data-toggle="pill" href="#v-pills-hidden-user-images" role="tab" aria-controls="v-pills-hidden-user-images" aria-selected="@if($active === 'hidden_user_images') true @else false @endif">Hidden user images</a>
                        @endif
                        @if(Auth::user()->isModerator())
                            <a class="nav-link @if($active === 'reports') active @endif" id="v-pills-reports-tab" data-toggle="pill" href="#v-pills-reports" role="tab" aria-controls="v-pills-reports" aria-selected="@if($active === 'reports') true @else false @endif">Reports</a>
                        @endif
                        @if(Auth::user()->isAdmin())
                            <a class="nav-link @if($active === 'hidden_reports') active @endif" id="v-pills-hidden-reports-tab" data-toggle="pill" href="#v-pills-hidden-reports" role="tab" aria-controls="v-pills-hidden-reports" aria-selected="@if($active === 'hidden_reports') true @else false @endif">Hidden reports</a>
                            <a class="nav-link @if($active === 'users') active @endif" id="v-pills-users-tab" data-toggle="pill" href="#v-pills-users" role="tab" aria-controls="v-pills-users" aria-selected="@if($active === 'users') true @else false @endif">Users</a>
                        @endif
                    </div>
                    <div class="dropdown-divider"></div>
            @endauth

            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link" href="{{url('/')}}">Front page</a>
                <a class="nav-link" href="{{url('/browse')}}">Browse images</a>
                @auth()
                    <a class="nav-link" href="{{url('users/'.Auth::user()->id)}}">My profile</a>
                    @if(Auth::user()->isStandard())
                        <a class="nav-link" href="{{url('user_images/create')}}">Create a new image</a>
                    @endif
                @endauth
            </div>
        </div>
    </div>
@endsection

@section('content')

    @if(Auth::user()->isStandard())
        <div class="tab-pane fade @if($active === 'my_user_images') show active @endif" id="v-pills-my-user-images" role="tabpanel" aria-labelledby="v-pills-my-user-images-tab">
            <ul class="list-group mb-2">
                @each('user_image.index_item', $my_user_images, 'user_image')
            </ul>
            <div class="d-flex justify-content-center">
                {{$my_user_images->appends([
                        'favorites'=>$favorites->currentPage(),
                        'active'=>'my_user_images'
                        ])->links('pagination::bootstrap-4')}}
            </div>
        </div>
        <div class="tab-pane fade @if($active === 'favorites') show active @endif" id="v-pills-favorites" role="tabpanel" aria-labelledby="v-pills-favorites-tab">
            <ul class="list-group mb-2">
                @each('user_image.index_item', $favorites, 'user_image')
            </ul>
            <div class="col-12 d-flex justify-content-center">
                {{$favorites->appends([
                        'my_user_images'=>$my_user_images->currentPage(),
                        'active'=>'favorites'
                        ])->links('pagination::bootstrap-4')}}
            </div>
        </div>
    @endif
    @if(Auth::user()->isModerator())
        <div class="tab-pane fade @if($active === 'favorites') show active @endif" id="v-pills-favorites" role="tabpanel" aria-labelledby="v-pills-favorites-tab">
            <ul class="list-group mb-2">
                @each('user_image.index_item', $favorites, 'user_image')
            </ul>
            <div class="col-12 d-flex justify-content-center">
                {{$favorites->appends([
                        'reports'=>$reports->currentPage(),
                        'active'=>'favorites'
                        ])->links('pagination::bootstrap-4')}}
            </div>
        </div>
        <div class="tab-pane fade  @if($active === 'reports') show active @endif" id="v-pills-reports" role="tabpanel" aria-labelledby="v-pills-reports-tab">
            <ul class="list-group mb-2">
                @each('report.index_item', $reports, 'report')
            </ul>
            <div class="col-12 d-flex justify-content-center">
                {{$reports->appends([
                        'favorites'=>$favorites->currentPage(),
                        'active'=>'reports'
                        ])->links('pagination::bootstrap-4')}}
            </div>
        </div>
    @endif
    @if(Auth::user()->isAdmin())
        <div class="tab-pane fade @if($active === 'hidden_user_images') show active @endif" id="v-pills-hidden-user-images" role="tabpanel" aria-labelledby="v-pills-hidden-user-images-tab">
            <ul class="list-group mb-2">
                @each('user_image.index_item', $hidden_user_images, 'user_image')
            </ul>
            <div class="col-12 d-flex justify-content-center">
                {{$hidden_user_images->appends([
                        'hidden_reports'=>$hidden_reports->currentPage(),
                        'users'=>$users->currentPage(),
                        'active'=>'hidden_user_images'
                        ])->links('pagination::bootstrap-4')}}
            </div>
        </div>
        <div class="tab-pane fade @if($active === 'hidden_reports') show active @endif" id="v-pills-hidden-reports" role="tabpanel" aria-labelledby="v-pills-hidden-reports-tab">
            <ul class="list-group mb-2">
                @each('report.index_item', $hidden_reports, 'report')
            </ul>
            <div class="col-12 d-flex justify-content-center">
                {{$hidden_reports->appends([
                        'hidden_user_images'=>$hidden_user_images->currentPage(),
                        'users'=>$users->currentPage(),
                        'active'=>'hidden_reports'
                        ])->links('pagination::bootstrap-4')}}
            </div>
        </div>
        <div class="tab-pane fade  @if($active === 'users') show active @endif" id="v-pills-users" role="tabpanel" aria-labelledby="v-pills-users-tab">
            <ul class="list-group mb-2">
                @each('user.index_item', $users, 'user')
            </ul>
            <div class="col-12 d-flex justify-content-center">
                {{$users->appends([
                        'hidden_reports'=>$hidden_reports->currentPage(),
                        'hidden_reports'=>$hidden_reports->currentPage(),
                        'active'=>'users'
                        ])->links('pagination::bootstrap-4')}}
            </div>
        </div>
    @endif

@endsection

@push('scripts')
    <!-- Favorite attach detach script -->
    <script src="{{ asset('js/favorite_attach_detach.js') }}"></script>
@endpush
