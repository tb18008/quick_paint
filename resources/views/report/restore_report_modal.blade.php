<!-- Restore Report Modal -->
<div class="modal fade" id="{{$modal_id}}" tabindex="-1" role="dialog" aria-labelledby="{{$modal_id}}_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="{{$modal_id}}_label">
                        Are you sure you want to restore @if(isset($report->subject)) "{{$report->subject}}"? @else report? @endif</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <a href="{{ url('reports/'.$report->id. '/restore')}}" class="btn btn-warning">Restore</a>
                </div>
        </div>
    </div>
</div>
