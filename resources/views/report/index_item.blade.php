<li class="list-group-item">
    <div class="d-flex justify-content-between">
        <div class="d-flex justify-content-start align-items-center">

            @isset($report->user_image)
                <a href="{{url('user_images/'.$report->user_image_id)}}">
                    <img src="{{url(Storage::url('thumbnails/'.$report->user_image->thumbnail_url))}}" alt="{{url(Storage::url('thumbnails/'.$report->user_image->thumbnail_url))}}">
                </a>
            @else
                <span>Missing user image (User Image ID = {{$report->user_image_id}})</span>
            @endisset

            <div class="col">
                <a href="{{url('reports/'.$report->id)}}">{{$report->subject}}</a>
                <span> by </span>
                <a href="{{url('users/'.$report->reporter->id)}}">{{$report->reporter->name}}</a>
            </div>
        </div>
        <div class="d-flex justify-content-end align-items-center">

            @if(!Auth::user()->isAdmin() || $report->trashed()) {{--Hides if no actions available--}}

                <div class="dropdown">

                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_menu">

                        {{--Owner's actions--}}
                        @if($report->reporter_id == Auth::user()->id)

                            <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#forced_delete_modal_{{$report->id}}">
                                Delete
                            </button>

                        {{--Moderator's actions--}}
                        @elseif(Auth::user()->isModerator() && !(Auth::user()->id == $report->reporter_id))

                            <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#safe_delete_modal_{{$report->id}}">
                                Unlist
                            </button>

                        {{--Admin's actions--}}
                        @elseif(Auth::user()->isAdmin() && $report->trashed())

                            <button type="submit" class="dropdown-item text-danger" data-toggle="modal" data-target="#forced_delete_modal_{{$report->id}}">
                                Delete
                            </button>

                            <button type="submit" class="dropdown-item text-secondary" data-toggle="modal" data-target="#restore_modal_{{$report->id}}">
                                Restore
                            </button>

                        @endif

                    </div>
                </div>

                {{--Modals--}}

                @if($report->reporter_id == Auth::user()->id || Auth::user()->isAdmin())   {{--Owner or admin force deletes--}}

                    @include('report.delete_report_modal', [
                        'modal_id' => 'forced_delete_modal_'.$report->id,
                        'report' => $report,
                        'forced' => 1,
                        'button_text' => 'Delete'
                    ])

                    @if($report->trashed())
                        @include('report.restore_report_modal', [
                            'modal_id' => 'restore_modal_'.$report->id,
                            'report' => $report,
                        ])
                    @endif

                @elseif(Auth::user()->isModerator() && !(Auth::user()->id == $report->reporter_id))

                    @include('report.delete_report_modal', [
                            'modal_id' => 'safe_delete_modal_'.$report->id,
                            'report' => $report,
                            'forced' => 0,
                            'button_text' => 'Unlist'
                        ])

                @endif

            @endif
        </div>
    </div>
</li>
