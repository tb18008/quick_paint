@extends('layouts.width-100')

@section('content')

    <div class="card">
        <div class="card-header">
            <b>Report index</b>
        </div>

        <div class="card-body">
            <ul class="list-group mb-2">
                @each('report.index_item', $reports, 'report')
            </ul>
        </div>
        <div class="card-footer">
            <div class="col-12 d-flex justify-content-center">
                {{$reports->links()}}
            </div>
        </div>

    </div>

@endsection
