@extends('layouts.width-50')

@section('content')

    <div class="card">
        <div class="card-header">
            <b>Report @if(isset($user_image->name)) "{{$user_image->name}}" @else user image @endif by {{$user_image->user->name}}</b>
        </div>

        <form class="m-2" method="POST" action="{{ url('reports/') }}">

            @csrf

            <div class="form-group row text-right">
                <label for="report_type_id" class="col-3 col-form-label">Report type</label>
                <div class="col">
                    <select class="custom-select @error('report_type_id') is-invalid @enderror" name="report_type_id" id="report_type_id" required>
                        <option value="" selected disabled>Select report type</option>
                        @foreach($report_types as $report_type)
                            <option value="{{$report_type->id}}">{{$report_type->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3">
                    @error('report_type_id')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row text-right">
                <label for="subject" class="col-3 col-form-label">Subject</label>
                <div class="col">
                    <input type="text" class="form-control @error('subject') is-invalid @enderror" name ="subject" id="subject" required>
                </div>
                <div class="col-3">
                    @error('subject')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row text-right">
                <label for="report_text" class="col-3 col-form-label">Report text</label>
                <div class="col">
                    <textarea class="form-control @error('report_text') is-invalid @enderror" name ="report_text" id="report_text" rows="3" required></textarea>
                </div>
                <div class="col-3">
                    @error('report_text')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>
            </div>

            {{--Hidden inputs--}}
            <input type="hidden" name="reporter_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="user_image_id" value="{{$user_image->id}}">

            <div class="form-group d-flex flex-row-reverse">
                <div class="col-4 offset-2">
                    <button type="submit" class="btn btn-primary">
                        Submit Report
                    </button>
                </div>
            </div>
        </form>
    </div>

@endsection
