@include('head')
    <div id="app">
        <div class="d-flex" id="navigation-header">

            @stack('sidebar-toggler')

            <div class="flex-grow-1">
                <nav class="navbar navbar-expand navbar-light bg-foreground">
                    <a class="navbar-brand mx-5" id="app_name" @auth() href="{{ url('/home') }}" @else href="{{ url('/') }}" @endauth>
                        {{ config('app.name', 'Laravel') }}
                    </a>

                    <div class="container d-flex">

                        <div class="flex-grow-1">   <!-- Left Side Of Navbar -->
                            @stack('navbar-item')
                        </div>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>

                    </div>
                </nav>
            </div>
        </div>

        <main>
            @yield('layout')
        </main>
    </div>
@include('footer')
