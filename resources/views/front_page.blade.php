@extends('layouts.split')

@push('navbar-item')
    @include('partials.search_bar')
@endpush

@section('sidebar')
    <div class="card full-height">
        <div class="card-header">{{ __('Front page') }}</div>

        <div class="card-body">

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            {{--Newest and favored user images nav-link--}}
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link @if($active === 'newest') active @endif" id="v-pills-newest_user_images-tab" data-toggle="pill" href="#v-pills-newest_user_images" role="tab" aria-controls="v-pills-newest_user_images" aria-selected=" @if($active === 'newest') true @else false @endif">Newest user images</a>
                <a class="nav-link @if($active === 'favored') active @endif" id="v-pills-favored_user_images-tab" data-toggle="pill" href="#v-pills-favored_user_images" role="tab" aria-controls="v-pills-favored_user_images" aria-selected=" @if($active === 'favored') true @else false @endif">Favored user images</a>
            </div>

            <div class="dropdown-divider"></div>

            {{--Guest nav links--}}
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link" href="{{url('/browse')}}">Browse images</a>
                <a class="nav-link" href="{{url('/tags')}}">Browse tags</a>
            </div>
        </div>
    </div>
@endsection

@section('content')

    {{--Newest user images pane--}}
    <div class="tab-pane fade @if($active === 'newest') show active @endif" id="v-pills-newest_user_images" role="tabpanel" aria-labelledby="v-pills-newest_user_images-tab">
        <ul class="list-group mb-2">
            @each('user_image.index_item', $newest_user_images, 'user_image')
        </ul>
        <div class="d-flex justify-content-center">
            {{$newest_user_images->appends(['favored'=>$favored_user_images->currentPage(), 'active'=>'newest'])->links('pagination::bootstrap-4')}}
        </div>
    </div>

    {{--Favored user images pane--}}
    <div class="tab-pane fade @if($active === 'favored') show active @endif" id="v-pills-favored_user_images" role="tabpanel" aria-labelledby="v-pills-favored_user_images-tab">
        <ul class="list-group mb-2">
            @each('user_image.index_item', $favored_user_images, 'user_image')
        </ul>
        <div class="d-flex justify-content-center">
            {{$favored_user_images->appends(['newest'=>$newest_user_images->currentPage(), 'active'=>'favored'])->links('pagination::bootstrap-4')}}
        </div>
    </div>

@endsection
