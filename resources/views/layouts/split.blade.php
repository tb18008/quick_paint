@extends('app')

@push('sidebar-toggler')
    @include('partials.sidebar_toggler')
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $("#toggle-sidebar").click(function () {
                $("#sidebar").toggleClass("collapsed");
                $("#content").toggleClass("col-12 col-9");

                return false;
            });
        });
    </script>
@endpush

@push('styles')
    <!-- Full height style -->
    <link type="text/css" href="{{ asset('css/full_height.css') }}" rel="stylesheet" >

    <style>
        .collapsed {    /* Hide elements with class this class */
            display: none;
        }
    </style>
@endpush

@section('layout')
    <div class="container-fluid full-height">
        <div class="row full-height">
            <div class="col-3 full-height bg-background-025 py-2" id="sidebar">
                @yield('sidebar')
            </div>
            <div class="col-9 full-height bg-background overflow-auto" id="content">

                @include('partials.messages')

                <div class="tab-content mt-2" id="v-pills-tabContent">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
@endsection
