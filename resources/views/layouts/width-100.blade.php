@extends('app')

@push('navbar-item')
    @include('partials.create_button')
@endpush

@section('layout')
    <div class="col-12 mx-auto p-3 full-height">

        @include('partials.messages')

        @yield('content')

    </div>
@endsection
