@extends('app')

@push('navbar-item')
    @include('partials.create_button')
@endpush

@section('layout')
    <div class="col-6 mx-auto p-3">

        @include('partials.messages')

        @yield('content')

    </div>
@endsection
