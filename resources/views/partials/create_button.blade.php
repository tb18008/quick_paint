@auth()
    @if(Auth::user()->isStandard())
        <div class="col px-5">
            <a href="{{url('/user_images/create')}}" role="button" class="btn btn-outline-primary btn-lg btn-block">
                Create a new image
            </a>
        </div>
    @endif
@endauth
