<form method="GET" action="{{ url('user_images/') }}">
    <div class="row align-items-center">
        <div class="col text-right">
            <label for="search_terms" class="col col-form-label">Search Image</label>
        </div>
        <div class="col">
            <input type="text" class="form-control" id="search_terms" name="search_terms" title="Search terms are separated with spaces" value="{{ old('search_terms') }}">
        </div>
        <div class="col align-self-end">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </div>
</form>
