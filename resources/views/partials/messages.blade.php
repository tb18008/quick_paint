@if(session()->has('message.success'))
    <div class="m-1 alert alert-success">
        {{ session()->get('message.success') }}
    </div>
@endif
@if (session()->has('message.fail'))
    <div class="m-1 alert alert-danger">
        {{ session()->get('message.fail') }}
    </div>
@endif
@if (session()->has('message.warning'))
    <div class="m-1 alert alert-warning">
        {{ session()->get('message.warning') }}
    </div>
@endif
