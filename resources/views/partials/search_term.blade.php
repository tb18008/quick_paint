<div class="badge-pill m-1 py-0 text-left search-term user-select-none text-center {{strtolower(class_basename($search_term))}}-{{$search_term->id}}"
     id="{{strtolower(class_basename($search_term))}}-{{$search_term->id}}"
     data-target="{{strtolower(class_basename($search_term))}}-{{$search_term->id}}"
     data-id="{{$search_term->id}}"
     data-type="{{strtolower(class_basename($search_term))}}"
     data-listed="false"
     data-weight="{{$search_term->weight}}">
    <span class="badge badge-light align-text-top">{{$search_term->weight}}</span>
    <span class="text-break">{{$search_term->name}}</span>
</div>
