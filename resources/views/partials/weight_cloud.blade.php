<div class="weight-cloud d-flex flex-wrap align-content-center justify-content-center">
    @if($sort_type == 'name')
        @each('partials.search_term', $search_terms->sortBy('name'), 'search_term')
    @elseif($sort_type == 'weight')
        @each('partials.search_term', $search_terms->sortByDesc('weight'), 'search_term')
    @endif
</div>
