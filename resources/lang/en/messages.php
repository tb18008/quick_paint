<?php

return [
    'stored' => ':Object_type ":Object_name" successfully created!',
    'updated' => ':Object_type ":Object_name" successfully updated!',
    'destroyed' => ':Object_type ":Object_name" successfully deleted!',
    'hidden' => ':Object_type ":Object_name" successfully hidden from index!',
    'restored' => ':Object_type ":Object_name" successfully restored!',
    'not_found' => ':Object_type was not found!',

    'role_update' => ':User_name was :update_type to :new_role!',

    'report_submit' => 'Your report has been submitted for approval!'
];
