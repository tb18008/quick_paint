<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Routes accessible only for authenticated users
Route::middleware('auth')->group(function () {

    //Dashboard
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'dashboard']);

    //Users
    Route::delete('users/{user}', [App\Http\Controllers\UserController::class, 'destroy']);

    //UserImages
    $routes = ['except' => ['show', 'index']];
    Route::resource('user_images',App\Http\Controllers\UserImageController::class, $routes);
    Route::get('user_images/{user_image_id}/restore', [App\Http\Controllers\UserImageController::class, 'restore']);

    //Roles
    Route::patch('role_elevate/', [App\Http\Controllers\RoleController::class, 'update']);

    //Reports
    Route::get('reports/', [App\Http\Controllers\ReportController::class, 'index']);
    Route::get('reports/create', [App\Http\Controllers\ReportController::class, 'create']);
    Route::post('reports/', [App\Http\Controllers\ReportController::class, 'store']);
    Route::get('reports/{report}', [App\Http\Controllers\ReportController::class, 'show']);
    Route::delete('reports/{report}', [App\Http\Controllers\ReportController::class, 'destroy']);
    Route::get('reports/{report_id}/restore', [App\Http\Controllers\ReportController::class, 'restore']);

    //Favorites
    Route::get('favorites/', [App\Http\Controllers\FavoriteController::class, 'user_favorites']);
    Route::get('favorites_attach/{user_image_id}', [App\Http\Controllers\FavoriteController::class, 'attach']);
    Route::get('favorites_detach/{user_image_id}', [App\Http\Controllers\FavoriteController::class, 'detach']);
});

//Routes for authenticated and guest users

Auth::routes();

//Front page
Route::get('/', [App\Http\Controllers\HomeController::class, 'front_page']);

//Users
Route::get('users/{user}', [App\Http\Controllers\UserController::class, 'show']);

//UserImages
Route::get('/browse', [App\Http\Controllers\UserImageController::class, 'browse']);
$routes = ['only' => ['index','show']];
Route::resource('user_images',App\Http\Controllers\UserImageController::class, $routes);

//Tags
Route::get('tags/', [App\Http\Controllers\TagController::class, 'index']);


