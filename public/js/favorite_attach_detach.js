/*  Favorites attach detach script
*   Contains AJAX requests and favorite-toggle hyperlink toggling with jQuery, show/hide
*/


    //Instead of each <a> element calling functions, jQuery click function implemented on class selector
    $( ".favorite-toggle" ).click(function(event) {
        event.preventDefault(); //Prevent from moving up to top of page when clicking link

        let user_image = $(this).attr("data-user-image");

        if($(this).attr("data-toggle-type") === "attach") favoriteAttach(user_image)
        else favoriteDetach(user_image)
    });


    //AJAX request for attaching a favorite
    function favoriteAttach(user_image_id){
        $.ajax({
            url: '/favorites_attach/'+user_image_id,
            dataType: 'json',
            success: function () {
                switchFavoriteButtons(user_image_id);
            },
            error: function (data) {
                if(!alert(data.responseText)){window.location.reload();}    //Reload page after alert to fix update data in HTML
            }
        });
    }

    //AJAX request for detaching a favorite
    function favoriteDetach(user_image_id){
        $.ajax({
            url: '/favorites_detach/'+user_image_id,
            dataType: 'json',
            success: function () {
                switchFavoriteButtons(user_image_id);
            },
            error: function (data) {
                if(!alert(data.responseText)){window.location.reload();}    //Reload page after alert to fix update data in HTML
            }
        });
    }

    //Showing/Hiding <a> elements (one always stays hidden and the other visible)
    function switchFavoriteButtons(user_image_id){

        let star = $("#favorite_attach_"+user_image_id);
        let blank = $("#favorite_detach_"+user_image_id);
        if(star.is(":hidden")){    //Mark as favorite
            star.removeAttr('hidden');
            blank.attr('hidden',true);
        }
        else{   //Remove favorite mark
            star.attr('hidden',true);
            blank.removeAttr('hidden');
        }

    }
