/*  Weight Cloud Color script
*   Change color of each weight cloud's children based on the attribute data-weight.
*   Highest weight value is assigned the style btn-primary-100, lowest - btn-primary-10
*/

let weights = [];
let max_weight = 0;

//For each weight cloud in document
$('.weight-cloud').each(function() {

    //Accrue weights of child elements
    weights = [];   //Array for finding the highest weight among the weight cloud's children

    //For each child element in this weight cloud
    $(this).children().each(function() {

        weights.push($(this).attr("data-weight"));  //Add child's weight value to array

    });

    max_weight = Math.max.apply(null, weights); //Calculate highest weight value

    //Iterate through all the children again
    $(this).children().each(function() {

        //Apply the class for style based on the child elements weight proportional to the highest value
        $(this).addClass('btn-primary-' + (Math.ceil($(this).attr("data-weight")/max_weight * 10) * 10));

    });
});
