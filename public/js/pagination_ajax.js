// Overrides default pagination link event and uses AJAX to get a specific page
$(".pagination a").click(function(event){
    event.preventDefault();
    let search_terms_with_page = window.search_terms;

    //Add new key value pair to search terms by splitting the url and getting page number from clicked link element
    search_terms_with_page['page'] = $(this).attr('href').split('page=')[1];

    $.get(
        '/user_images/',
        search_terms_with_page,
        function (data) {
            $("#search_results").html(data);    //Fill search results div with response
        }
    );
});
