/*  User Image Browse script
*   Contains:
*       Interactions with user input
*       Setting selected search terms
*       Search term submission and search result retrieval with AJAX
*/

var search_terms = {};  //Global variable for when user submits search terms and pagination requests a different page

$(document).ready(function(){

    //Variables for set search terms that (updated on search term changes, read on search submission)
    let tag_list = [];
    let user = null;
    let name = null;

    //Handle changing current active pane by clicking navigation tab
    $( ".nav-link" ).click(function() { //Moves show and active classes from current pane and link to target pane and link, shows the active pane's sort toggle

        if($(this).hasClass("active")) return; //An already active tab was clicked, nothing changes

        //An inactive tab was clicked, switching panes

        let current_pane = $("#nav-content-panes > .active");    //Find the pane that is currently active
        let target_pane = $($(this).attr("data-target-pane"));    //Find the pane that needs to be activated (.nav-link points to pane with data attribute)

        current_pane.removeClass('show');   //Deactivate current pane
        current_pane.removeClass('active');

        target_pane.addClass('show');   //Activate target pane
        target_pane.addClass('active');

        $("#nav-tab > .active").removeClass('active');  //Activate clicked nav-link
        $(this).addClass('active');

        $(".sort_toggle").addClass('d-none');   //Hide all sort toggles
        $($(this).attr("data-target-sort-toggle")).removeClass('d-none');   //Show the sort toggle that needs to be activated (.nav-link points to sort toggle with data attribute)

    });

    //Handle search term adding and removing from search term bar by clicking the search term badge
    $(".search-term").click(function() {

        //Clicked search term badge is in a pane and is not in the list of search terms
        if($(this).attr("data-listed") === "false"){    //Clicked search term is unlisted

            $("."+$(this).attr("data-target")).addClass('d-none'); //Hide clicked element in both sort divs

            //Tag search term specific logic (adding to array of tag search terms)
            if($(this).attr("data-type") === "tag") tag_list.push($(this).attr("data-id")); //If clicked on a tag type add associated model's id to tags array

            //User search term specific logic (switching out current user for clicked user)
            else {

                user = $(this).attr("data-id"); //Assign associated user model's id to search term variable

                let current_user_search_term = $("#user-list .search-term");    //Look for search term bar's user list elements

                if(current_user_search_term !== undefined){ //If an existing user search term is found

                    //Show the hidden search term badges that current user search term points to
                    $("#"+current_user_search_term.attr("data-target")).removeClass('d-none');

                    //Remove the elements inside search term bar user's list
                    current_user_search_term.remove();
                }
            }

            //Cloning clicked search term badge into the search term bar
            let term_div_clone = $(this).clone(true);   //Clone clicked search term badge
            term_div_clone.children("span:first-child").text($(this).attr("data-name"));    //Set text of badge
            term_div_clone.attr("data-listed",true);    //Mark as listed
            term_div_clone.removeAttr("id");
            term_div_clone.removeClass('d-none');

            $("#"+$(this).attr("data-type")+"-list").append(term_div_clone);  //Add clone to search term bar

        }

        //Clicked search term badge is in the search term bar and is in the list of search terms
        else{

            let target_search_term_badge =  $("."+$(this).attr("data-target"));

            switch($(this).attr("data-type")) { //Remove listed item from search query array according to it's type
                case "tag":

                    target_search_term_badge.removeClass('d-none');   //Show the element that search term points to

                    const index = tag_list.indexOf($(this).attr("data-id"));  //Find position of search term in tag list array
                    if (index > -1) tag_list.splice(index, 1);   //If found, then remove from array

                    break;
                case "user":

                    target_search_term_badge.removeClass('d-none');   //Show the element that search term points to

                    user = null;    //Unset selected user

                    break;
                case "name":

                    //Allow the user to use the name input
                    $("#add-name-button").removeClass('disabled');
                    $("#name-input").removeClass('disabled');

                    name = null;    //Unset selected name

                    break;
            }

            $(this).remove();   //Remove clicked search term badge from list

        }

        toggleTermLists($(this).attr("data-type")); //Hide or show respective list

    });

    //Handle adding name to search terms on button click
    $("#add-name-button").click(function() {

        if($(this).hasClass("disabled")) return;    //User is not allowed to add more than one name to search terms

        let name_input = $("#name-input");
        let new_name = name_input.val();   //User's input

        name_input.val("");    //Clear text input

        //Check for duplicate name search terms
        if($("#name-"+ new_name).length) return;

        let new_name_term_div = $("#name-search-term").clone(true);   //Clone hidden search term badge

        new_name_term_div.attr("id",'name-'+ new_name); //Set id of new element
        new_name_term_div.attr("data-id",new_name);

        let new_name_term_div_span = new_name_term_div.children("span:first-child");  //Find span inside div
        new_name_term_div_span.text(new_name);    //Set text of span
        new_name_term_div.attr("data-listed",true);    //Set search term as listed

        $("#name-list").append(new_name_term_div);  //Add to search term bar

        //Prevent user from adding more names as search terms
        $(this).addClass('disabled');
        name_input.addClass('disabled');

        name_input.val(""); //Prevent user from adding the same name without writing it out

        name = new_name;   //Set name to search term variable

        toggleTermLists("name"); //Hide or show respective list
    });

    //Handle submitting selected search terms and requesting search results on button click
    $("#search_button").click(function (){

        window.search_terms = {tags:tag_list,user:user,name:name};  //Set the global variable to an associative array with the selected search terms

        $.ajax({
            type: 'GET',
            url: '/user_images/',
            dataType: 'html',
            data: window.search_terms,
            beforeSend: function () {   //Show loading spinner animation while waiting for response
                $(".loading-spinner").removeClass("d-none");
                $("#search_button_text").addClass("d-none");
                $("#search_button").addClass("disabled");
            },
            success: function (data) {  //Fill the search result area with returned view from back-end
                $("#search_results").html(data);
            },
            complete: function () {   //Hide loading spinner animation and allow user to submit more searches
                $(".loading-spinner").addClass("d-none");
                $("#search_button_text").removeClass("d-none");
                $("#search_button").removeClass("disabled");
            },
        });

        $("#content").scrollTop = 0;    //Scroll to the top
    })

    //Handles toggling the display class of what sort option targets
    $(".sort-option").click(function(){
        $($(this).attr("data-target")).toggleClass("d-none");
    });

    //Changes visibility of lists as the search term badges get added or removed
    function toggleTermLists(search_term_type){

        let list = $("#"+search_term_type+"-list"); //Specified list with search term elements
        let list_parent = list.parent();    //List's parent with the display class

        if(list.children("div").length > 0 && list_parent.hasClass("d-none")){   //At least one element and is hidden
            list_parent.removeClass('d-none');  //Shows div
        }
        else if(list.children("div").length === 0 && !list_parent.hasClass("d-none")){   //Zero elements and isn't hidden
            list_parent.addClass('d-none'); //Hides div
        }
    }

});
