/*  Painting Widget script
*   Contains user input logic and HTML canvas style alterations
*/

/*
VARIABLE INITIALIZATION
*/

    //Variables referring to HTML elements

        //Drawing area and canvases
        let drawing_area = document.getElementById("drawing_area"); //Space where user can use drawing tools
        let canvas = document.getElementById("paint_canvas");   //Resulting image of user's drawing
        let top_layer = document.getElementById("top_layer");   //Canvas above the resulting image where composite operations take place (unified stroke opacity)
        let brush_cursor = document.getElementById("brush_cursor");
        let scale_overlay = document.getElementById("scale_overlay");
        let zoom_reset = document.getElementById("zoom_reset");

        //Input variables
        let color_picker = $(document).find("#color_picker");
        let brush_size_range = $(document).find("#brush_size_range");
        let brush_size_number = $(document).find("#brush_size_number");

    //Contexts
    let canvas_context = canvas.getContext("2d");
    let top_layer_context = top_layer.getContext("2d");

    //Scale variables
    let canvas_width = 128;
    let canvas_height = 128;
    let canvas_bounds = canvas.getBoundingClientRect();
    let initial_scale = calculateInitialScale();
    let scale = 1;

    //Position variable for canvas transform origin changes
    let position_offset = {
        x:0,
        y:0
    };

    //Brush variables
    let brush_size = 1;
    let brush_color = 'black';

    //Mouse variables
    let mouseX = 0;
    let mouseY = 0;
    let dragging = false;
    let drawing = false;
    let panning = false;
    let over_overlay = false;

    function calculateInitialScale(){   //Returns the amount the canvas needs to be scaled for it to fill the drawing area

        let drawing_area_bounds = drawing_area.getBoundingClientRect();
        let canvas_bounds = canvas.getBoundingClientRect();
        let margin;

        let drawing_area_aspect_ratio = drawing_area_bounds.height / drawing_area_bounds.width;
        let canvas_aspect_ratio = canvas_bounds.height / canvas_bounds.width;

        if(drawing_area_aspect_ratio > canvas_aspect_ratio){
            margin = drawing_area_bounds.width / 20;
            return (drawing_area_bounds.width - margin) / canvas_bounds.width;
        }
        else{
            margin = drawing_area_bounds.height / 20;
            return (drawing_area_bounds.height - margin) / canvas_bounds.height;
        }

    }

/*
USER INPUT JQUERY EVENTS
*/

    //Brush color change event

    color_picker.change(function() {

        brush_color = color_picker.minicolors('rgbString') || 'black';
        top_layer_context.strokeStyle = brush_color;
        top_layer_context.fillStyle = brush_color;

        canvas_context.globalAlpha = color_picker.minicolors('opacity'); //Setting opacity of top layer when it is transferred to canvas

    });



    //Brush size change event

    brush_size_range.on('input', function() {
        changeBrushSize(this.value);
        brush_size_number.val(this.value);
    });

    brush_size_number.on('input', function() {
        changeBrushSize(this.value);
        brush_size_range.val(this.value);
    });

    function changeBrushSize(value){
        brush_size = value || 1;
        top_layer_context.lineWidth = brush_size;

        scaleCursor();
    }



    // Tag input

    $("#tag_input").change(function () {    //When textarea is changed, hidden input's value is
        $(document).find('#tag_string').val(this.value.replace(/\s/g, '').toLowerCase());    //value of text area but without white space characters and lowercase
    });



    // Panning

    $("#pan_input").change(function () {
        if(this.checked){
            brush_cursor.hidden = true;
            drawing_area.style.cursor = "all-scroll";
            top_layer.style.cursor = "all-scroll";
            panning = true;
        }
        else{
            brush_cursor.hidden = false;
            drawing_area.style.cursor = "none";
            top_layer.style.cursor = "none";
            panning = false;

            brush_cursor.hidden = true;
        }
    });


/*
METHODS
*/

    // View transform methods (zoom, panning)

        function change_origin(wheel_event){    // Move point of transform-origin without moving the canvas

            let canvas_rect = canvas.getBoundingClientRect();   // Information about canvas element position and size

            let canvas_old_position = { //Set canvas position before canvas transform-origin change
                x: canvas_rect.x,
                y: canvas_rect.y,
            }

            //Cursor position relative to canvas size
            let cursor_position_on_canvas = {
                x: ((wheel_event.clientX - canvas_rect.x) / canvas_rect.width * 100) + '%',
                y: ((wheel_event.clientY - canvas_rect.y) / canvas_rect.height * 100) + '%'
            }

            //CSS transform-origin property modification
            canvas.style.transformOrigin = cursor_position_on_canvas.x + ' ' + cursor_position_on_canvas.y;
            top_layer.style.transformOrigin = cursor_position_on_canvas.x + ' ' + cursor_position_on_canvas.y;

            canvas_rect = canvas.getBoundingClientRect();   //Current position of canvas after transform-origin moved the element

            let canvas_position_shift = {
                x: canvas_rect.x - canvas_old_position.x,
                y: canvas_rect.y - canvas_old_position.y
            }

            //Position offset value gets incremented by the shift because translate parameters are absolute
            position_offset.x -= canvas_position_shift.x;
            position_offset.y -= canvas_position_shift.y;

            //CSS translate property modification
            canvas.style.transform = `translate(${position_offset.x}px ,${position_offset.y}px)`;
            top_layer.style.transform = `translate(${position_offset.x}px ,${position_offset.y}px)`;

        }

        // Zooming in increments with mouse wheel

        function zoom(event) {

            const maximum_zoom = 4;
            const zoom_increment = .00025;
            const minimum_zoom = 0.03125;

            scale += event.deltaY * (-zoom_increment);

            // Restrict scale
            scale = Math.min(Math.max(minimum_zoom, scale), maximum_zoom);

            // Apply scale transform
            canvas.style.transform += `scale(${initial_scale * scale})`;
            top_layer.style.transform += `scale(${initial_scale * scale})`;

            // Change cursor scale
            scaleCursor();
            moveCursor(event);

            // Change label text
            document.getElementById("scale_label").innerText = Math.round(scale * 100) + " %";

            recalculateBounds();

        }

        // Zooming on window load to fit drawing area

        function zoomToFit(){
            canvas.style.transform = `scale(${initial_scale})`;
            top_layer.style.transform = `scale(${initial_scale})`;
            document.getElementById("scale_label").innerText = "100 %";
            recalculateBounds();
        }

        //Sets the bounds of the canvas after scaling or panning

        function recalculateBounds() {

            canvas_bounds = canvas.getBoundingClientRect();
            let width_unscaled = canvas_bounds.right - canvas_bounds.left;
            let height_unscaled = canvas_bounds.bottom - canvas_bounds.top;
            canvas_bounds = new DOMRect(canvas_bounds.x, canvas_bounds.y, width_unscaled * initial_scale * scale, height_unscaled * initial_scale * scale);

        }

        //Scale brush cursor after zooming

        function scaleCursor(){

            brush_cursor.style.width = brush_size * initial_scale * scale + "px";
            brush_cursor.style.height = brush_size * initial_scale * scale + "px";
        }

        //Move brush cursor when mouse is moved while in drawing area

        function moveCursor(mouse_event){
            let drawing_area_bounds = drawing_area.getBoundingClientRect();
            let cursor_bounds = brush_cursor.getBoundingClientRect();
            brush_cursor.style.left = mouse_event.clientX - drawing_area_bounds.left - (cursor_bounds.width / 2) + "px";
            brush_cursor.style.top = mouse_event.clientY - drawing_area_bounds.top - (cursor_bounds.height / 2) + "px";
        }


    // Drawing methods

        function putPoint (){
            //Puts a point at the position of cursor

            top_layer_context.arc(mouseX, mouseY, brush_size / 2, 0, Math.PI * 2);
            top_layer_context.fill();
        }

        function startStroke (mouse_event) {
            //Sets starting position of stroke

            setMouseCoordinates(mouse_event);
            top_layer_context.beginPath();
        }

        function lengthenStroke (mouse_event) {
            //Setting start point of line
            lineSettings();
            top_layer_context.beginPath();
            top_layer_context.moveTo(mouseX, mouseY);

            //Setting end point of line
            setMouseCoordinates(mouse_event);
            top_layer_context.lineTo(mouseX, mouseY)

            //Drawing along the line
            top_layer_context.closePath();
            top_layer_context.stroke();
        }

        function finishStroke () {
            //Transfers top layer to canvas and clears top layer for next stroke

            canvas_context.drawImage(top_layer,0,0);
            top_layer_context.clearRect(0, 0, top_layer.width, top_layer.height);

            drawing = false;
        }

        function setMouseCoordinates(event) {
            //Sets the current position of cursor factoring in scale

            recalculateBounds();
            mouseX = (event.clientX - canvas_bounds.left) / (initial_scale * scale);
            mouseY = (event.clientY - canvas_bounds.top) /(initial_scale * scale);
        }

        function lineSettings(){
            top_layer_context.lineCap = "round";
            top_layer_context.lineJoin = "round";
        }



/*
MOUSE EVENT LISTENERS
*/

    //Drawing area mouse event listeners

    drawing_area.addEventListener('wheel', function(wheel_event){
        wheel_event.preventDefault(); //Prevents from scrolling the page vertically

        change_origin(wheel_event); //Setting point relative to which the canvas is scaled
        zoom(wheel_event);  //Scaling the canvas
    });

    drawing_area.addEventListener('mousedown', function(mouse_event){
        brush_cursor.hidden = true;
        if(!panning) {
            drawing = true;
            startStroke(mouse_event);
            putPoint();
        }
    });

    drawing_area.addEventListener('mouseup', function(mouse_event){
        dragging = false;

        if(!panning){
            drawing = false;
            finishStroke(mouse_event);
            brush_cursor.hidden = false;
        }
    });

    drawing_area.addEventListener('mouseleave', function(mouse_event){
        dragging = false;

        brush_cursor.hidden = true;

        if(!panning) finishStroke(mouse_event);
    });

    drawing_area.addEventListener('mouseenter', function(mouse_event){
        if(!panning) brush_cursor.hidden = false;
    });

    drawing_area.addEventListener('mousemove', function(mouse_event){
        if(!panning && !dragging) moveCursor(mouse_event);
        else if(panning && dragging){
            canvas.style.top = (canvas.offsetTop + mouse_event.movementY) + "px";
            canvas.style.left = (canvas.offsetLeft + mouse_event.movementX) + "px";
            top_layer.style.top = (top_layer.offsetTop + mouse_event.movementY) + "px";
            top_layer.style.left = (top_layer.offsetLeft + mouse_event.movementX) + "px";

            recalculateBounds();
        }
    });



    //Top layer mouse event listeners

    top_layer.addEventListener('mousemove', function(mouse_event){
        if(!panning && drawing) {
            dragging = true;
            lengthenStroke(mouse_event);
        }
    });

    top_layer.addEventListener('mouseenter', function(mouse_event){
        if(dragging && !panning) startStroke(mouse_event);
    });



    //Zoom reset mouse event listeners

    zoom_reset.addEventListener('click', function(){
        //Position offset value gets incremented by the shift because translate parameters are absolute
        position_offset.x = position_offset.y = 0;

        // Apply scale transform
        canvas.style.transform = `translate(${position_offset.x}px, ${position_offset.y}px) scale(${initial_scale})`;
        top_layer.style.transform = `translate(${position_offset.x}px, ${position_offset.y}px) scale(${initial_scale})`;

        //CSS transform-origin property modification
        canvas.style.transformOrigin = '50% 50%';
        top_layer.style.transformOrigin = '50% 50%';

        //Reset style changed by panning
        canvas.style.left = null;
        canvas.style.top = null;
        top_layer.style.left = null;
        top_layer.style.top = null;

        // Change label text
        document.getElementById("scale_label").innerText = "100 %";

        scale = 1;

        // Scale cursor size
        scaleCursor();

        recalculateBounds();
    });

    scale_overlay.addEventListener('mouseenter', function(){
        over_overlay = true;
        if(!panning && !dragging) brush_cursor.hidden = true;
    });

    scale_overlay.addEventListener('mouseleave', function(){
        over_overlay = false;
        if(!panning && !dragging) brush_cursor.hidden = false;
    });
